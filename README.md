# README #

This repository is now deprecated! All future improvements to the algorithm will be posted to:

https://github.com/openPfizer/vpop-gen

I will keep this repository on bitbucket for as long as it is allowed since it contains all the project history.

### Who do I talk to? ###

* Ted Rieger: ted.rieger@pfizer.com