function [DYDT,FinalPts,Pfinal,Sfinal,Jobs]=get_and_distribute_VPs(numofbatches)

delete(gcp('nocreate'))
%evalin('base',['clear ','VPmodel'])
Pfinal=[];
Sfinal=[];
str1='Results/My_Model_VPs';
for i=1:numofbatches
    dataload=[str1 num2str(i) '.mat'];
    load(dataload);
    Pfinal=[Pfinal  pOut];
    Sfinal=[Sfinal,Score_V];
end
% Remove non-converged solutions

zeroScores=Sfinal==0;

Pfinal=Pfinal(:,zeroScores);

%% Split Pfinal into 8 jobs
NumVPs=numel(Pfinal(1,:));

NJobs=8;
split(2:(NJobs+1))=round(NumVPs*((1:NJobs)/NJobs));
split(1)=0;

%% Work out where to make the split

for i=1:NJobs
    
    tag_string=strcat('_',num2str(i));
    
    %%Slight overlap
    pSplit=Pfinal(:,split(i)+1:split(i+1));
    
    Jobs(i)=batch('SimulateVPs',0,{VPmodel,pSplit,tag_string,1});
    
end

for i=1:NJobs
    wait(Jobs(i));
end

%% Put everything together and clean up
DYDT = [];
FinalPts=[];

for i=1:NJobs
    load_string=strcat('Results_',num2str(i),'.mat');
    load(load_string);
    FinalPts=[FinalPts; OutPts];
    DYDT=[DYDT; OutDYDT];
    delete(load_string);
end

save('Results_All.mat','FinalPts','DYDT','Pfinal','Sfinal');
end
