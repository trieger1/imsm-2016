function [p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag)

p_bestfit=ConstantParameterValues;


%%% Edit for specific model


% Range calculated by following through  analysis and calculations from
% van de pas 2012
% with mean +/- 3*sigma (from the experimental data).


%% edited van de pas (2012) model has 8 states and 22 parameters,

nStates=numel(names);
nParameters=numel(ConstantParameterValues);

% initialize arrays

SS_bestfit=-1*ones(nStates,1);
SS_lower=SS_bestfit;
SS_upper=SS_bestfit;
p_lower=-1*ones(nParameters,1);
p_upper=-1*ones(nParameters,1);

%%%%%%%%%%% EDIT THIS FOR DIFFERENT MODELS %%%%%%%%%%%%%%%%%%%%%%%

% steady-states

%vdists
v_intestine=VPmodel.compartments(1).capacity;
v_liver=VPmodel.compartments(2).capacity;
v_plasma=VPmodel.compartments(3).capacity;
v_periphery=VPmodel.compartments(4).capacity;

% intestine free cholesterol
SS_bestfit(1)= 1.2736; SS_lower(1)=0; SS_upper(1)=4.8*v_intestine;


% intestesine cholesterol ester
SS_bestfit(2)= 0.16; SS_lower(2)=0;  SS_upper(2)=0.88*v_intestine;


% liver free cholesterol
SS_bestfit(3)= 14.4; SS_lower(3)=6.8*v_liver; SS_upper(3)=9.2*v_liver;

% liver cholesterol ester
SS_bestfit(4)= 9.54; SS_lower(4)=3.5*v_liver;  SS_upper(4)=7.1*v_liver;


% Choose:
%%%  HDL_fc + HDL_ce < upperHDL = max(NHANES) = 4.0415{ to ensure
%%%  coverage of range}
% van de Pas claim 1:3 fc:ce ratio from Groener, et al. (1998) Atherosclerosis 137, 311-319
% assume this is accurate... then 4*HDL_fc <upperHDL & 1.33*HDL_ce
%%%

% plasma HDL free cholesterol
SS_bestfit(5)= 0.837; SS_lower(5)=0.3627/4; SS_upper(5)=v_plasma*4.0415/4;
% plasma HDL cholesterol ester
SS_bestfit(6)= 2.4831; SS_lower(6)=0.3627/1.333;  SS_upper(6)=v_plasma*4.0415/1.333;


% plasma non-HDL cholesterol
SS_bestfit(7)= 11.2437; SS_lower(7)=0.5959*v_plasma; SS_upper(7)=10.18*v_plasma;  %% NHAHNES


% peripherary cholesterol
SS_bestfit(8)= 77.76; SS_lower(8)=0.34*v_periphery;  SS_upper(8)=1.74*v_periphery;


% LDL
SS_bestfit(9)= SS_bestfit(7)*0.85; SS_lower(9)=0.232*v_plasma;  SS_upper(9)=8.571*v_plasma; %% NHAHNES



%%%%%%%%%%%


%%%%%%%%%%%

% parameters


% paper/simbiology reaction name: "Hepatic Cholesterol Synthesis"
p_lower(1)=0.0362; p_upper(1)=1.44; %p_bestfit(1)=0.44;

% paper/simbiology reaction name: "Peripheral Cholesterol Synthesis"
p_lower(2)=0; p_upper(2)=12.1; %p_bestfit(2)=3.79;

% paper/simbiology reaction name: "Intestinal Cholesterol Synthesis"
p_lower(3)=0.0692; p_upper(3)=0.4995; %p_bestfit(3)=0.18;

% paper/simbiology reaction name: "Dietary Cholesterol Intake"
p_lower(4)=0.01; p_upper(4)=2.19; %p_bestfit(4)=1.09;

% paper/simbiology reaction name: "HDL-associated cholesterol esterification"
p_lower(9)=0; p_upper(9)=17.58/SS_bestfit(5);    %p_bestfit(9)=9.39068;

% paper/simbiology reaction name: "biliary cholesterol excretion"
p_lower(14)=1.28/SS_bestfit(3); p_upper(14)=6.03/SS_bestfit(3); %p_bestfit(14)=0.2542;

% paper/simbiology reaction name: "fecal cholesterol excretion"
p_lower(15)=0/SS_bestfit(1); p_upper(15)=3.71/SS_bestfit(1);%%% range corrected for best fit steady state %p_bestfit(15)=1.4526;

% paper/simbiology reaction name: "intestinal cholesterol transport to HDL"
%p_lower(16)=0.0314; p_upper(16)=1.83/SS_bestfit(1);%p_bestfit(16)=0.0314;
%% hard to estimate this one

% paper/simbiology reaction name: "hepatic cholesterol catabolism"
p_lower(18)=0/SS_bestfit(3); p_upper(18)=2.23/SS_bestfit(3);  %p_bestfit(18)=0.0715278;



% paper/simbiology reaction name: "CE transfer from HDL to LDL"
p_lower(21)=3.46*v_plasma/(SS_bestfit(6)*SS_bestfit(7)); p_upper(21)=6.64*v_plasma/(SS_bestfit(6)*SS_bestfit(7)); %p_bestfit(21)=0.49266;

% ratio of LDL to total non-HDL cholesterol (by definitiion between 0 and 1).
p_bestfit(22)=0.85; p_lower(22)=0; p_upper(22)=1;



%%% use this range for any parameters we don't know about
p_2_scale=p_lower==-1 & p_upper ==-1;

p_lower(p_2_scale)=p_bestfit(p_2_scale)/search_mag;
p_upper(p_2_scale)=p_bestfit(p_2_scale)*search_mag;


end

