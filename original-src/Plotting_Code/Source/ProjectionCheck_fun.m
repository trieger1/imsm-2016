function ProjectionCheck_fun(varargin)

%% Script to check the pairwise projections of vectors. Worst results are skewed to the right, better to the left.
% Used to think about how independent large parameter sets are. In
% particular, for virtual populations we would prefer orthogonal parameter
% sets if possible for hypothesis exploration

for j=1:numel(varargin)

sm=varargin{j};

P_scaled=sm.P_scaled;
method_name=sm.method_name;
P_scaled=P_scaled';

%% To expensive to do every pair-wise comparison. 
%% Assume that if this number is large enough we get a good sample of the true distribution
Nchecks=1e4;

%% Going to pick two parameter vectors at random and calculate the normalized dot product between them.
Projection_All=zeros(Nchecks,1);
Projection_random=zeros(Nchecks,1);
Projection_correlated=zeros(Nchecks,1);
Projection_mixed=zeros(Nchecks,1);

CenterData=0.5*ones(size(P_scaled));

%% Might want to look at the PP vs VP correlation. Just interested in the final population for now.
NumVPs=numel(P_scaled(1,:));

%% Some Test Cases
P_random=rand(size(P_scaled));
P_correlated=0.1+0.5*rand(size(P_scaled));
split=floor(NumVPs/2);
P_mixed=[P_scaled(:,1:split) P_correlated(:,(split+1):end)];


%% Sample the selection subset, obviously pointless for the test cases but keep everything same dimensions.
PP_Parameters=P_scaled-CenterData;
VP_Parameters=P_scaled-CenterData;
P_random_selected=P_random-CenterData;
P_correlated_selected=P_correlated-CenterData;
P_mixed_selected=P_mixed-CenterData;


%% Loop through and select two vectors at random and store the normalized dot product
for i=1:Nchecks

        if mod(i,Nchecks/100)==0
       
        end
        
        
            index1=ceil(rand*numel(VP_Parameters(1,:)));
            index2=ceil(rand*numel(VP_Parameters(1,:)));
            
        if index1~=index2
            
            Projection_All(i)=dot(VP_Parameters(:,index1),VP_Parameters(:,index2))/(norm(VP_Parameters(:,index1)*norm(VP_Parameters(:,index2))));
           if j==1
            Projection_random(i)=dot(P_random_selected(:,index1),P_random_selected(:,index2))/(norm(P_random_selected(:,index1)*norm(P_random_selected(:,index2))));
            Projection_correlated(i)=dot(P_correlated_selected(:,index1),P_correlated_selected(:,index2))/(norm(P_correlated_selected(:,index1)*norm(P_correlated_selected(:,index2))));
            Projection_mixed(i)=dot(P_mixed_selected(:,index1),P_mixed_selected(:,index2))/(norm(P_mixed_selected(:,index1)*norm(P_mixed_selected(:,index2))));
           end
        else
            Projection_All(i)=nan;
            if j==1
            Projection_random(i)=nan;
            Projection_correlated(i)=nan;
            Projection_mixed(i)=nan;
            end
        end
        
   
end

%%Get rid of any self-comparisons.
Projection_All=Projection_All(~(isnan(Projection_All)));
Projection_random=Projection_random(~(isnan(Projection_random)));
Projection_correlated=Projection_correlated(~(isnan(Projection_correlated)));
Projection_mixed=Projection_mixed(~(isnan(Projection_mixed)));

%% plot the results


figure(90)
h_VPs=histogram(Projection_All,'Normalization','cdf');
BinEdges1=h_VPs.BinEdges;
BinCenters1=0.5*(BinEdges1(2:end)+BinEdges1(1:end-1));
figure(80)
h_random=histogram(Projection_random,'Normalization','cdf');
BinEdges2=h_random.BinEdges;
BinCenters2=0.5*(BinEdges2(2:end)+BinEdges2(1:end-1));

%{
figure
h_correlated=histogram(Projection_correlated,'Normalization','cdf');
BinEdges3=h_correlated.BinEdges;
BinCenters3=0.5*(BinEdges3(2:end)+BinEdges3(1:end-1));

figure
h_mixed=histogram(Projection_mixed,'Normalization','cdf');
BinEdges4=h_mixed.BinEdges;
BinCenters4=0.5*(BinEdges4(2:end)+BinEdges4(1:end-1));
%}

if j==1
h=figure('Name','Orthogonality of Parameters, CDF');
stairs(BinCenters2,h_random.Values,'DisplayName', 'Random Parameters Uniformly Sampled [0,1]');
hold on
else
figure(h)
end

stairs(BinCenters1,h_VPs.Values,'DisplayName',strcat(method_name, ' scaled by bounds to [-0.5, 0.5]'));


%stairs(BinCenters3,h_correlated.Values,'DisplayName', 'Parameters Uniformly Sampled [0.1,0.6]');
%stairs(BinCenters4,h_mixed.Values,'DisplayName', 'Parameters Uniformly Sampled from [0.1,0.6] and [0,1], (50% mix each)');
legend('show')

close 90 80
end
end