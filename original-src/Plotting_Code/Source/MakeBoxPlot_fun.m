function smOut=MakeBoxPlot_fun(sm)


VPmodel   = sm.VPmodel;
Pfinal    = sm.Pfinal;
selection = sm.selection;

%% Get p_upper and p_lower
[tb,xb,names] = sbiosimulate(VPmodel);

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

%% Make plots
NumVPs=numel(Pfinal(1,:));
PriorP=rand(NumVPs,1);

for i=1:22
    ind=num2str(i);
    Pname=strcat('k_{',ind,'}');
    ParameterNames{i}=Pname;
end

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);


P_scaled=(Pfinal-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
P_scaled1=P_scaled;
%ParameterNames=[ConstantParameters];
%ParameterNames{23}='k22';

figure('Name','Violin Plot')
subplot(2,2,1)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)
ylim([0 1])
alpha(0.3)
C1=corrcoef(P_scaled);

NumVPs=numel(Pfinal(1,selection));

PriorP=rand(NumVPs,1);

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);

P_scaled=(Pfinal(:,selection)-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
C2=corrcoef(P_scaled);
subplot(2,2,3)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)

ylim([0 1])
alpha(0.3)

Cmax=max(max(max(C1-eye(size(C1)))),max(max(C2-eye(size(C2)))));
Cmin=min(min(min(C1-eye(size(C1)))),min(min(C2-eye(size(C2)))));

subplot(2,2,2)
imagesc(C1,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square
subplot(2,2,4)
imagesc(C2,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square

smOut=sm;
smOut.P_scaled=P_scaled;

end