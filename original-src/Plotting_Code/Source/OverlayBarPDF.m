function  OverlayBarPDF(Data,mu,sigma,Nbins,Xlabel)

%figure

hold on

[N,X]=hist(Data,Nbins);

area2=sum(N.*(X(2)-X(1)));

stairs((X-(X(2)-X(1))/2),N/area2,'r','LineWidth',3)
xlim([min(X) max(X)])

%% Plot the PDF

Xp=min(X):0.01:max(X);

Y = normpdf(Xp,mu,sigma);
%ylim([0,1.2*max(N)/area2])
plot(Xp,Y,'--k','LineWidth',5)

xlabel(Xlabel);
ylabel('Probability/Normalized Density');
ylim([0,1.2*max(Y)])
end

