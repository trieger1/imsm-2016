function Bar_Time_Summary(varargin)

y=zeros(numel(varargin),2);


for i=1:numel(varargin)
  
    sm=varargin{i};
   
    y(i,1)=sm.time_per_pp;
    y(i,2)=sm.time_per_vp;
    Labels(i)= {sm.method_name};
end
    figure('Name','Computational Time Summary')

    
    bar(y,'grouped')
   
   set(gca, 'XTick', 1:numel(varargin), 'XTickLabel', Labels);
   ylabel('Time (seconds)')
    legend('Time per PP','Time per VP')
end


