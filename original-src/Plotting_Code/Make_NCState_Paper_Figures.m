clear all
close all
%% load a structure here if necessary
load('SavedRuns/Nested_Short_Test')

%% Add Source Directory to Path

addpath(genpath('Source'))
%just use one workspace structure repeatedly for testing
sm_original=sm;
sm_nested=sm;
sm_MH=sm;
sm_GA=sm;

%% Structure Definition of 'sm' For Input
%How well does your histograms match the data? (lower the better)
%sm.gof = gof;
%How many plausible patients do you need to make 1 VP? (lower the better)
%sm.selection_efficiency = numel(selection)/sum(selection);
%How long to make 1 plausible patient (shorter the better)
%sm.time_per_pp = (PPs_time)/N_PPs;
%How long to make 1 VP? (shorter the better)
%sm.time_per_vp = sm.time_per_pp*sm.selection_efficiency;
%How long is the cpu time for generating pps?
%sm.cputime_pp = PPs_time;
%How long is the cpu time for all this procedure?
%sm.cputime_total = total_cpu_time;
%How many virtual patients you generated in the end?
%sm.n_vps = sum(selection);
%Record the number of PPs generated.
%sm.n_pps = N_PPs;
%Store the generated characteristics for selection
%sm.VPChar      = VPChar ;
%Store the selected population
%sm.selection   = selection;
%Store the probability of inclusion
%sm.ProbInclude = p_include;
%Store the final parameters for the plausible population
%sm.Pfinal      = pOut;
%Store the scale-factore to multiply p_include
%sm.betaS       = sf;
%Mean of MVN distribution for fitting
%sm.mu          = mu;
%Covariance matrix of MVN distribution for fitting
%sm.sigma       = sigma;
%Simbiology model
%sm.VPmodel     = VPmodel;
%Method Name
%sm.method_name = method_name;


%% Make figures like the original paper
getVPsPlotFits_fun(sm_original)
getVPsPlotFits_fun(sm_nested)
getVPsPlotFits_fun(sm_MH)
getVPsPlotFits_fun(sm_GA)
%% Make Violin Plot
smOut_orignal=MakeBoxPlot_fun(sm_original);
smOut_nested=MakeBoxPlot_fun(sm);
smOut_MH=MakeBoxPlot_fun(sm);
smOut_GA=MakeBoxPlot_fun(sm);

%% Orthogonality of parameters (not this analyzes scaled parameters amended to the structure by MakeBoxPlot_fun
ProjectionCheck_fun(smOut_orignal,smOut_nested,smOut_MH,smOut_GA);

%% Plot a Bar Graph Summarizing Time to make a PP and VP
Bar_Time_Summary(sm,sm,sm,sm)