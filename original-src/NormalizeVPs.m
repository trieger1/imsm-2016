function [VPs] = NormalizeVPs(VPs,mu,sigma)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

NumOfVPs=numel(VPs(:,1));

% set bounds for VPs

dim=numel(mu);

mu1=repmat(mu,NumOfVPs,1);

%% generate test data
%for real data will need individual data, or infer distribution using known
% distr. and cross-correlations. Assumining multi-variate normal for now.

issymmetric(sigma);  %check symmetry
[~,posdef] = chol(sigma); % check positive definite

sigmaVPs=repmat(diag(sigma)',NumOfVPs,1);

VPs=(VPs-mu1)./sigmaVPs.^0.5;
end

