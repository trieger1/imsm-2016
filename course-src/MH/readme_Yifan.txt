test_select.m : test the pecentage selected from PP to VP.

visualization.m : visualization of real data, plausible population, virtual population and rejected population in 3D plot.

MCmultinormalci.m : calculate c_i in stratified sampling.

findpi.m : calculate p_i in stratified sampling.

metro.m: metropolis hasting algorithm.
