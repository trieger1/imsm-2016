addpath('Results');
addpath('Source');
addpath('Source/NHANES');
thdl = xptread('HDL_G.XPT');
ttc = xptread('TCHOL_G.XPT');
ttrig = xptread('TRIGLY_G.XPT');

% Merge based on the key SEQN:
c1 = join(thdl,ttc,'Keys','SEQN');
c1 = join(ttrig,c1,'Keys','SEQN');

load('Results/tempWorkspace');
% Log-transform the relevant columns:
lnHDL = log(c1.LBDHDD);
lnLDL = log(c1.LBDLDL);
lnTC = log(c1.LBXTC);


% real data


rdata=[lnHDL,lnLDL,lnTC];

[realdata] = NormalizeVPs(rdata,mu,sigma);

realdata(sum(isnan(realdata),2)>=1,:)=[];

for i=1:3
 [~,pVal(i),ksstat(i),cv(i)] = kstest(realdata(:,i));
end

sum(ksstat)