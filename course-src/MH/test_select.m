% First, load saved data
addpath('Results');
addpath('Source');
addpath('Source/NHANES');
load('Results/tempWorkspace');
% most fit percentage
tic;
[selection,~,~,~,ProbInclude,betaS]= prevalence_select_func(mu,sigma,log(VPChar));
[mm,nn]=size(selection);
p1=sum(selection)/mm;

VPs=log(VPChar);
% one hundred trials percentage
num_vps = numel(VPs(:,1)); % number of plausible patients
dim     = numel(mu); % number of distributions to match (1d histograms)
mu1     = repmat(mu,num_vps,1); % for use in scaling VPs
sigmaVPs    = repmat(diag(sigma)',num_vps,1);
VPs         = (VPs-mu1)./sigmaVPs.^0.5; % Normalize VPs
data_pdf     = mvnpdf(VPs.*sigmaVPs.^0.5+mu1,mu,sigma); 
num_pts     = 5; % Nearest neighbor points for density calculation
vp_tree     = KDTreeSearcher(VPs,'BucketSize',1);
[~,DVPs]    = knnsearch(vp_tree,VPs,'K',num_pts);  % Assumes non-identical VPs
BallVolume2 = nsphereVolume(dim,max(DVPs'));
vp_density = num_pts./BallVolume2; % VP density based on num_pts nearest neighbors
p_include = (data_pdf)./vp_density'; % Calculate the probability of inclusion
runs    = 10; % Number of times to try the fitting
fopt    = @(p)OptimizeVPgeneration(p,p_include,VPs,runs);
sf_max  = 1/max(p_include); % Maximum scaling factor
sf_lower = 0; % lower bound for the scaling factor on probability of inclusion
sf_upper = log10(1000*sf_max); % upper bound for the scaling factor on the probability of inclusion

options_sa = saoptimset('TolFun',1e-15,'ReannealInterval',50000,...
    'InitialTemperature',0.5,'MaxIter',1000,'Display','Iter',...
    'TemperatureFcn',@temperatureboltz,'AnnealingFcn',@annealingboltz,...
    'AcceptanceFcn',@acceptancesa);
%   options=saoptimset('TolFun',1e-15,'ReannealInterval',50000,'InitialTemperature',1,'MaxIter',1000,'TemperatureFcn',@temperatureboltz,'AnnealingFcn', @annealingboltz,'AcceptanceFcn',@acceptancesa);
k   = simulannealbnd(fopt,log10(sf_max),sf_lower,sf_upper,options_sa); % optimize the log10(scaling factor) value
sf  = 10.^k(1); % scaling factor transformed back from log10
num_tests = 100;
hist_score_temp = zeros(num_tests,1);
selected_temp   = zeros(num_vps,num_tests);
for i=1:num_tests
    [hist_score_temp(i),selected_temp(:,i)]= OptimizeVPgeneration(k,p_include,VPs,1);
end
[mm,nn]=size(selected_temp);
p2=sum(selected_temp,1)/mm;

% generate new selection, percentage
[gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,log(VPChar),1);


[mm,nn]=size(newselection);
p3=sum(newselection)/mm;

toc;


% compare p1, p2 ,p3