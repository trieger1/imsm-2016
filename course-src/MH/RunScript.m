%% RunScript.m - generates figures from published paper
%   Generates a plausible patient cohort from the van de Pas Model, fits
%   the cohort against the NHANES data (with some transformation of model
%   outputs) and outputs key figures from the paper.
%
%   Step 1.  Generate Plausible Patients
%   Call GenerateVPs.m function which executes the generation of
%   'Plausible Patients'
%
%   Step 2. Analyze Plausible Patients
%
%   Extract the information to constrain against NHANES data set
%
%   Step 3. Select the virtual population based on the NHANES data distribution for LDLC, HDLC and TC
%   Note, the file will display iterations indicating convergence of the
%   method. The f(x) value printed is the average K-S test score between
%   univariate comparison with data. Anything around 0.1 is typically a
%   very good fit.
%
%   Increase these for anything other than a test run. Total number of
%   plausible patients will be simsperbatch*numofbatches. Aim for at least >
%   10,000 for this problem.

%% Setup
clear all; %#ok<CLSCR>
addpath('Results');
addpath('Source');
addpath('Source/NHANES');

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0);
mu = m.mu;
sigma = m.Sigma;

% Set the number of plausible patients to generate:
simsperbatch = 2000;
numofbatches = 8;

%% Step 1. Generate Plausible Patients
% By default, uses parallel toolbox
p = parpool;
parfor i=1:numofbatches
    batchname=strcat('My_Model_VPs',num2str(i));
    GenerateVPs(batchname,simsperbatch);
end
delete(p);
%% Step 2. Analyze Plausible Patients
% Model-specific steps:
% Simulate plausible patients to steady state:
[DYDT,FinalPts,Pfinal,Jobs]=get_and_distribute_VPs(numofbatches);
% Gather model outputs into outputs amenable for comparison to NHANES:
VPChar(:,1)=FinalPts(:,5)+FinalPts(:,6); %HDL
VPChar(:,2)=FinalPts(:,9); % LDL
VPChar(:,3)=FinalPts(:,7)+FinalPts(:,5)+FinalPts(:,6); %TC

% Unit conversions to match NHANES:
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;

%% Step 3. Select the virtual population based on the NHANES data distribution
[selection,~,~,~,ProbInclude,betaS]= prevalence_select_func(mu,sigma,log(VPChar));

% Example: generate new selection, and evaluate how good this selection is
[gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,log(VPChar),1);

% Save key variables:
matfile = fullfile('Results', 'tempWorkspace.mat');
save(matfile,'DYDT','FinalPts','Pfinal','Jobs',...
    'VPChar','mu','sigma','selection','ProbInclude','betaS',...
    'gof','newselection');

%% Step 4. plot Paper Data
% Note: clears work space and closes figures. If you want to plot the 
% latest data generated edit getVPsPlotFit.m file to load 'tempWorkspace'.
% Otherwise load paper data 'Results_All_Paper.mat'
getVPsPlotFits;
