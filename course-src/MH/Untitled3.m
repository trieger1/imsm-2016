tic;
[selection,~,~,~,ProbInclude,betaS]= prevalence_select_func3(mu,sigma,log(VPChar));

% Example: generate new selection, and evaluate how good this selection is
[gof,newselection]= OptimizeVPgeneration(betaS,ProbInclude,log(VPChar),1);
toc;

[mm,nn]=size(newselection);
sum(newselection)/mm

[mm,nn]=size(selection);
sum(selection)/mm


[mm,nn]=size(selected);
sum(selected)/mm


[mm,nn]=size(selected_temp);
sum(selected_temp,1)/mm