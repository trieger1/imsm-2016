load('metro4000')
addpath('Results');
addpath('Source');
addpath('Source/NHANES');
load('Results/tempWorkspace.mat')
[selection,gof,~,~,ProbInclude,betaS]=prevalence_select_func(mu,sigma,vpfinal');