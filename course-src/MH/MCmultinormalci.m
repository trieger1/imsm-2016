function [vector]=MCmultinormalci(mu,sigma,n)
% c_i^2
NN=1000000;
R=mvnrnd(mu,sigma,NN);
mu1=repmat(mu,NN,1);
temp = (R-mu1)*inv(sigma);
distance2=zeros(NN,1);
for i=1:NN
   distance2(i)=temp(i,:)*(R(i,:)-mu1(i,:))';
end
q=[1:(n-1)]/n;
vector=quantile(distance2,q);
