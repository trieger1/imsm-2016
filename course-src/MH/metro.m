tic;
t=cputime;
addpath('../Results');
addpath('../Source');
addpath('../Source/NHANES');

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0);
mu = m.mu;
sigma = m.Sigma;

% Set the number of plausible patients to generate:
%simsperbatch = 2000;
%numofbatches = 8;
batchname=strcat('My_Model_VPs',num2str(1));
My_model_save=batchname;
NumVPs=1000; % ORIGINAL: 20000
dirSave=strcat('Results/',My_model_save);
load('Source/My_Model.mat')

% Rename the model something sensible
VPmodel=van_de_pas_mod1;

% Reseed the random number generator:
rng('shuffle')

% Accelerate the model - need to setup mex compliler to do this:
sbioaccelerate(VPmodel);

% Simulate to generate baseline VP, grab names:
[tb,xb,names] = sbiosimulate(VPmodel);


disflag=ones(NumVPs,1);

%% Load free parameters
% Do not include 'parameters' redefined by SimBiology in repeated
% assignments, or parameters which are known with high accuracy.

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);



%%%%%%%%%%%%%%%%%%%%

m=numel(p_lower); % dimension of parameters
q=zeros(m,NumVPs); % sample
q(:,1)=p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
pSS=-1*ones(numel(p_lower),2);

pSS(:,1)=p_lower;
pSS(:,2)=p_upper;
%Midp=(pSS(:,1)+pSS(:,2))/2;



NewP3 = addvariant(VPmodel,'NewP3');
p=q(:,1);

for j=1:numel(p)
    addcontent(NewP3,{'parameter',ConstantParameters{j},'Value',p(j)})
end

try
    [t1,x1] = sbiosimulate(VPmodel,C,NewP3); % Simulated the new variant, usually to steady state
    
catch
    x1 = 1e30*ones(1,numel(names));
    t1=[1 2];
    disflag(1)=0;
end


pp=x1(end,:);
vpc1=zeros(1,3);
vpc1(:,1)=pp(:,5)+pp(:,6); %HDL
vpc1(:,2)=pp(:,9); % LDL
vpc1(:,3)=pp(:,7)+pp(:,5)+pp(:,6); %TC
delete(VPmodel.variant(numel(VPmodel.variant)))

if (sum(vpc1<0)>0)
    disp('try it again');
end


vpc1=vpc1/2.79;
vpc1=vpc1*38.66;
vpc1=log(vpc1);

vp(:,1)=vpc1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=2:NumVPs
    q(:,i)=q(:,i-1)+(pSS(:,2)-pSS(:,1))/10.*(2*rand(m,1)-1);
    
    % This step adds a "variant" to the simbiology model, that is an
    % alternative parameterization of the model, which is simulated below.
    NewP2 = addvariant(VPmodel,'NewP2');
    p=q(:,i);
    
    for j=1:numel(p)
        addcontent(NewP2,{'parameter',ConstantParameters{j},'Value',p(j)})
    end
    
    
    try
        [t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state
    catch
        x1 = 1e30*ones(1,numel(names));
        t1=[1 2];
        disflag(i)=0;
    end
    
    
    
    delete(VPmodel.variant(numel(VPmodel.variant)))
    qq=x1(end,:);
    vpc1=zeros(1,3);
    vpc1(:,1)=qq(:,5)+qq(:,6); %HDL
    vpc1(:,2)=qq(:,9); % LDL
    vpc1(:,3)=qq(:,7)+qq(:,5)+qq(:,6); %TC
    
    
    if (sum(vpc1<0)>0)
        disflag(i)=0;
    end
    
    
    vpc1=vpc1/2.79;
    vpc1=vpc1*38.66;
    vpc1=log(vpc1);
    
    if (disflag(i)==0)
        vpc1=vp(:,i-1)';
        q(:,i)=q(:,i-1);
        vp(:,i)=vp(:,i-1);
    end
    
    ratio=mvnpdf(vpc1,mu,sigma)/mvnpdf(vp(:,i-1)',mu,sigma);
    u=rand(1);
    
    if (u>ratio)
        q(:,i)=q(:,i-1);
        vp(:,i)=vp(:,i-1);
    else
        vp(:,i)=vpc1;
    end
    
end


vpfinal=vp(:,logical(disflag));




matfile = fullfile('metro.mat');
save(matfile,'vpfinal');

[selection,gof,~,~,ProbInclude,betaS]=prevalence_select_func(mu,sigma,vpfinal');
e=cputime-t
toc;