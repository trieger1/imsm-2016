%mu,sigma


NN=30000;
R=mvnrnd(mu,sigma,NN);
mu1=repmat(mu,NN,1);
distance=diag((R-mu1)*inv(sigma)*(R-mu1)');
distance2=sort(distance);
[quantile(distance2,0),quantile(distance2,0.2),quantile(distance2,0.4),quantile(distance2,0.6),quantile(distance2,0.8),quantile(distance2,1)]




NN=10000000;
R=mvnrnd(mu,sigma,NN);
mu1=repmat(mu,NN,1);
temp = (R-mu1)*inv(sigma);
distance2=zeros(NN,1);
for i=1:NN
   distance2(i)=temp(i,:)*(R(i,:)-mu1(i,:))';
end
[quantile(distance2,0),quantile(distance2,0.2),quantile(distance2,0.4),quantile(distance2,0.6),quantile(distance2,0.8),quantile(distance2,1)]
