tic;
[selection,~,~,~,ProbInclude,betaS]= prevalence_select_func2(mu,sigma,log(VPChar));

% Example: generate new selection, and evaluate how good this selection is
[gof,newselection]= OptimizeVPgeneration(betaS,ProbInclude,log(VPChar),1);
toc;