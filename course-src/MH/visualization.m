% figure 1: real data  
% figure 2: plausible and selected patients in one picture
% figure 3: rejected population
% figure 4: real data and selected population in one picture 
% figure 5: real data and rejected population in one picture 


% visual real data
addpath('Results');
addpath('Source');
addpath('Source/NHANES');
thdl = xptread('HDL_G.XPT');
ttc = xptread('TCHOL_G.XPT');
ttrig = xptread('TRIGLY_G.XPT');

% Merge based on the key SEQN:
c1 = join(thdl,ttc,'Keys','SEQN');
c1 = join(ttrig,c1,'Keys','SEQN');

load('Results/tempWorkspace');
% Log-transform the relevant columns:
lnHDL = log(c1.LBDHDD);
lnLDL = log(c1.LBDLDL);
lnTC = log(c1.LBXTC);


% real data


rdata=[lnHDL,lnLDL,lnTC];

[realdata] = NormalizeVPs(rdata,mu,sigma);
figure;
scatter3(realdata(:,1),realdata(:,2),realdata(:,3));
clear title xlabel ylabel
title ('real data');
%get plausible population

[normVPs] = NormalizeVPs(log(VPChar),mu,sigma);
% visualization for plausible population
xx=normVPs(:,1);
yy=normVPs(:,2);
zz=normVPs(:,3);
figure;
scatter3(xx,yy,zz);
hold on;

% visualization for selected population

[normVPs] = NormalizeVPs(log(VPChar),mu,sigma);
gofbest=1;
for i=1:100
    
    [gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,normVPs,1);
    
    if gof<gofbest
        gofbest=gof;
        selection=newselection;
    end
end


PSelect=normVPs(selection,:);
xx1=PSelect(:,1);
yy1=PSelect(:,2);
zz1=PSelect(:,3);
%figure;

scatter3(xx1,yy1,zz1);
clear title xlabel ylabel
title ('plausible and selected')
legend([,],'plausible', 'selected');
% visualization for rejected population

Pnoselect=normVPs;
Pnoselect(selection,:)=[];
xx2=Pnoselect(:,1);
yy2=Pnoselect(:,2);
zz2=Pnoselect(:,3);
figure;
scatter3(xx2,yy2,zz2);
clear title xlabel ylabel
title ('rejected')

% figure 4
figure;
scatter3(realdata(:,1),realdata(:,2),realdata(:,3));
hold on;
scatter3(xx1,yy1,zz1);
clear title xlabel ylabel
title ('real and selected')
legend([,],'real', 'selected');


% figure 5
figure;
scatter3(realdata(:,1),realdata(:,2),realdata(:,3));
hold on;
scatter3(xx2,yy2,zz2);
clear title xlabel ylabel
title ('real data and rejected')
legend([,],'real', 'rejected');



% figure 6

num_vps=size(vpfinal,2);
sigmaVPs    = repmat(diag(sigma)',num_vps,1);
mu1     = repmat(mu,num_vps,1); % for use in scaling VPs
VPs         = (vpfinal'-mu1)./sigmaVPs.^0.5; % Normalize VPs
figure;
scatter3(VPs (:,1),VPs (:,2),VPs (:,3));


vpfinal2=vpfinal(:,selection);
num_vps=size(vpfinal2,2);
sigmaVPs    = repmat(diag(sigma)',num_vps,1);
mu1     = repmat(mu,num_vps,1); % for use in scaling VPs
VPs         = (vpfinal2'-mu1)./sigmaVPs.^0.5; % Normalize VPs
figure;
scatter3(VPs (:,1),VPs (:,2),VPs (:,3));







