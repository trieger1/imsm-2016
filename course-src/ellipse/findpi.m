function [p]=findpi(c,max)
%% Use the solution for equation systems (10) in report to calculate the proportions pi's
% c is a vector of dimension number of regions -1; max is the boundary of
% the largest ellipsoid. The dimension of p is the number of regions. And
% the summation of each element of p is equal to 1.

n=size(c,2)+1;
p=zeros(n,1);
c=c.^1.5;
max=max.^1.5;
c=[c,max];
p(n)=1/n*max/(max-c(n-1));
for i=1:(n-2)
    j=n-i;
    sum1=0;
    for k=1:(n-j)
        sum1=sum1+p(n-k+1)/c(n-k+1);
    end
    temp=1/n*c(j)/(c(j)-c(j-1));
    p(j)=temp-c(j)*sum1;
end

p(1)=1/n-c(1)*sum(p(2:end)./c(2:end)');

end % function findpi