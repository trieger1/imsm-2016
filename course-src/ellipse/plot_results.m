function void = plot_results(method_id, yourname, fidelity)
addpath('Source/NHANES');
addpath('Source');
addpath('Results');
addpath('Source/distributionPlot');

global sigma mu
N_size = 10000;

%% Scatter plots for plasusible patients and data
result_name = sprintf('Results/SpeedTest_M%d_S10000_%s.mat', method_id, yourname);
load(result_name);
[~,~,~,~,ProbInclude,betaS]= prevalence_select_func(mu,sigma,log(VPChar));
%This script loads the data generated for the paper, or that generated from
%RunScript.m and makes figures similar to the main results of the paper.

% For calculating 95% error surfaces from 3D data, this script calls
% error_ellipse.m by AJ Johnson (2004). Available from the MATLAB File
% Exchange http://www.mathworks.com/matlabcentral/fileexchange/4705-error-ellipse
% see error_license.txt for the license for this file.


%load('Results/SpeedTest.mat');
FinalPts = OutPts;
Pfinal = pOut; %only 1 is transposed
% 
% % this step pre-saved
% %%%[DYDT,FinalPts,Pfinal,Jobs]=get_and_distribute_VPs();
% %close all
% VPChar(:,1)=FinalPts(:,5)+FinalPts(:,6); %HDL_c
% VPChar(:,2)=FinalPts(:,9); % LDL_c
% VPChar(:,3)=FinalPts(:,7)+FinalPts(:,5)+FinalPts(:,6); %TC
% VPChar=VPChar/2.79;
% VPChar=VPChar*38.66;

%fidelity=85;
BinN=85;
%[selection,~,~,~,ProbInclude,sf]= prevalence_select_func(mu,sigma,log(VPChar(:,:)));
%sf
NumVps=numel(VPChar(:,1));

figure
%%figure1 hist of log of include prob
ProbInclude2=ProbInclude*betaS;
bins=-300:0.1:0.5;
bins2=-300:0.1:0.5;
temp=ProbInclude2>1;
ProbInclude2(temp)=1;

[n,x]=hist(log10(ProbInclude2),bins);
%set(gca,'XScale','log');
bar1=bar(x,n,'b','LineWidth',1,'BarWidth',1);
set(bar1,'FaceColor','b')
xlim([-20  2])
xlabel('log(Probabilty VP in Vpop)')
ylabel('frequency')
hold on
[n,x]=hist(log10(ProbInclude2(selection)),bins2);
bar2=bar(x,n,'r','LineWidth',1,'BarWidth',1);
set(bar2,'FaceColor','r','EdgeColor','k')

axes('parent',gcf,'position',[0.2 0.5 0.4 0.4]);
bar3=bar(x,n,'r','LineWidth',1,'BarWidth',1,'FaceColor','none');
set(bar3,'FaceColor','r','EdgeColor','k')
xlim([-3  1])

figure_1_1 = sprintf('Results/figure1_M%d_S%d_%s.fig', method_id, N_size, yourname);
figure_1_2 = sprintf('Results/figure1_M%d_S%d_%s.png', method_id, N_size, yourname);
saveas(gcf, figure_1_1);
saveas(gcf, figure_1_2);



[normVPs] = NormalizeVPs(log(VPChar),mu,sigma);
gofbest=1;
for i=1:100
    
    [gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,normVPs,1);
    
    if gof<gofbest
        gofbest=gof;
        selection=newselection;
    end
end

%set(gca,'XScale','log');

PSelect=Pfinal(:,selection);
w = std(PSelect');
Wmat=repmat(w,sum(selection),1);
medP=repmat(median(PSelect'),sum(selection),1);
Pnorm=(PSelect'-medP)./Wmat;
idx=kmeans(Pnorm,2,'Replicates',100);


%figure
%fugure 2 (useless)
%silhouette(Pnorm,idx)
VP_final_all=VPChar(selection,:);
VP_final_cluster1=VP_final_all(idx==1,:);
VP_final_cluster2=VP_final_all(idx==2,:);






figure(7)
[hellipse]=error_ellipse(sigma,mu,'conf' ,0.95);

figure
subplot(2,3,1)
%HDL_c
OverlayBarPDF(log(VPChar(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')
figAlim=xlim;
subplot(2,3,2)
%LDL_c
OverlayBarPDF(log(VPChar(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')
figBlim=xlim;
subplot(2,3,3)
%TC
OverlayBarPDF(log(VPChar(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')
figClim=xlim;


xdatas = get(hellipse(3), 'XData');
ydatas = get(hellipse(3), 'YData');
zdatas = get(hellipse(3), 'ZData');
subplot(2,3,6)
%TC vs HDL_c
hold on

scatter(log(VPChar(1:fidelity:NumVps,3)),log(VPChar(1:fidelity:NumVps,1)),2,'r')
xlabel('log(TC)')
ylabel('log(HDL_c)')

plot(zdatas,xdatas,'k--','LineWidth',2)
xlim([figClim])
xdatas = get(hellipse(2), 'XData');
ydatas = get(hellipse(2), 'YData');
zdatas = get(hellipse(2), 'ZData');
subplot(2,3,5)
hold on
%TC vs LDL_c

scatter(log(VPChar(1:fidelity:NumVps,2)),log(VPChar(1:fidelity:NumVps,3)),2,'r')
xlabel('log(LDL_c)')
ylabel('log(TC)')
plot(ydatas,zdatas,'k--','LineWidth',2)
xlim([figBlim])
xdatas = get(hellipse(1), 'XData');
ydatas = get(hellipse(1), 'YData');
zdatas = get(hellipse(1), 'ZData');
subplot(2,3,4)
hold on
%HDL_c vs LDL_c

scatter(log(VPChar(1:fidelity:NumVps,1)),log(VPChar(1:fidelity:NumVps,2)),2,'r')

xlabel('log(HDL_c)')
ylabel('log(LDL_c)')

plot(xdatas,ydatas,'k--','LineWidth',2)
xlim([figAlim])

figure_2_1 = sprintf('Results/figure2_M%d_S%d_%s.fig', method_id, N_size, yourname);
figure_2_2 = sprintf('Results/figure2_M%d_S%d_%s.png', method_id, N_size, yourname);
saveas(gcf, figure_2_1);
saveas(gcf, figure_2_2);


figure
subplot(2,3,1)
%HDL_c
OverlayBarPDF(log(VP_final_all(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')
figAlim=xlim;
subplot(2,3,2)
%LDL_c
OverlayBarPDF(log(VP_final_all(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')
figBlim=xlim;
subplot(2,3,3)
%TC
OverlayBarPDF(log(VP_final_all(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')
figClim=xlim;


xdatas = get(hellipse(3), 'XData');
ydatas = get(hellipse(3), 'YData');
zdatas = get(hellipse(3), 'ZData');
subplot(2,3,6)
%TC vs HDL_c
hold on

scatter(log(VP_final_all(:,3)),log(VP_final_all(:,1)),6,'r')
plot(zdatas,xdatas,'k--','LineWidth',2)
xlim([figClim])
xlabel('log(TC)')
ylabel('log(HDL_c)')


xdatas = get(hellipse(2), 'XData');
ydatas = get(hellipse(2), 'YData');
zdatas = get(hellipse(2), 'ZData');
subplot(2,3,5)
hold on
%TC vs LDL_c

scatter(log(VP_final_all(:,2)),log(VP_final_all(:,3)),6,'r')
plot(ydatas,zdatas,'k--','LineWidth',2)
xlim([figBlim])
xlabel('log(LDL_c)')
ylabel('log(TC)')

xdatas = get(hellipse(1), 'XData');
ydatas = get(hellipse(1), 'YData');
zdatas = get(hellipse(1), 'ZData');
subplot(2,3,4)
hold on
%HDL_c vs LDL_c

scatter(log(VP_final_all(:,1)),log(VP_final_all(:,2)),6,'r')
plot(xdatas,ydatas,'k--','LineWidth',2)
xlim([figAlim])
xlabel('log(HDL_c)')
ylabel('log(LDL_c)')

figure_3_1 = sprintf('Results/figure3_M%d_S%d_%s.fig', method_id, N_size, yourname);
figure_3_2 = sprintf('Results/figure3_M%d_S%d_%s.png', method_id, N_size, yourname);
saveas(gcf, figure_3_1);
saveas(gcf, figure_3_2);

% figure
% subplot(2,3,1)
% Overlay2BarPDF(log(VP_final_cluster1(:,1)),log(VP_final_cluster2(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')
% 
% subplot(2,3,2)
% Overlay2BarPDF(log(VP_final_cluster1(:,2)),log(VP_final_cluster2(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')
% 
% subplot(2,3,3)
% Overlay2BarPDF(log(VP_final_cluster1(:,3)),log(VP_final_cluster2(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')
% 
% 
% xdatas = get(hellipse(3), 'XData');
% ydatas = get(hellipse(3), 'YData');
% zdatas = get(hellipse(3), 'ZData');
% subplot(2,3,6)
% %TC vs HDL_c
% hold on
% 
% scatter(log(VP_final_cluster1(:,3)),log(VP_final_cluster1(:,1)),6,'b')
% scatter(log(VP_final_cluster2(:,3)),log(VP_final_cluster2(:,1)),6,'r')
% plot(zdatas,xdatas,'k--','LineWidth',2)
% xlabel('log(TC)')
% ylabel('log(HDL_c)')
% xlim([figClim])
% 
% xdatas = get(hellipse(2), 'XData');
% ydatas = get(hellipse(2), 'YData');
% zdatas = get(hellipse(2), 'ZData');
% subplot(2,3,5)
% hold on
% %TC vs LDL_c
% 
% scatter(log(VP_final_cluster1(:,2)),log(VP_final_cluster1(:,3)),6,'b')
% scatter(log(VP_final_cluster2(:,2)),log(VP_final_cluster2(:,3)),6,'r')
% plot(ydatas,zdatas,'k--','LineWidth',2)
% xlabel('log(LDL_c)')
% ylabel('log(TC)')
% xlim([figBlim])
% 
% xdatas = get(hellipse(1), 'XData');
% ydatas = get(hellipse(1), 'YData');
% zdatas = get(hellipse(1), 'ZData');
% subplot(2,3,4)
% hold on
% %HDL_c vs LDL_c
% 
% scatter(log(VP_final_cluster1(:,1)),log(VP_final_cluster1(:,2)),6,'b')
% scatter(log(VP_final_cluster2(:,1)),log(VP_final_cluster2(:,2)),6,'r')
% plot(xdatas,ydatas,'k--','LineWidth',2)
% xlabel('log(HDL_c)')
% ylabel('log(LDL_c)')
% xlim([figAlim])
close 7


%%%%% Test Selection Variability




PSelect2=Pfinal;
w = std(PSelect2');
Wmat=repmat(w,numel(Pfinal(1,:)),1);
medP=repmat(median(PSelect2'),numel(Pfinal(1,:)),1);
Pnorm2=(PSelect2'-medP)./Wmat;

sumSelect=selection;
repeats=999;
P_centroid=zeros(22,repeats);
distP=zeros(1,repeats);
for i=1:repeats
    
    [gof(i),newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,normVPs,1);
    
    sumSelect=sumSelect+newselection;
    P_centroid(:,i)=mean(Pnorm2(newselection,:));
end

centroid_center=mean(P_centroid,2);

for i=1:repeats
    distP(i)= pdist([P_centroid(:,i) centroid_center]','chebychev');
end

figure_4_1 = sprintf('Results/figure4_M%d_S%d_%s.fig', method_id, N_size, yourname);
figure_4_2 = sprintf('Results/figure4_M%d_S%d_%s.png', method_id, N_size, yourname);
saveas(gcf, figure_4_1);
saveas(gcf, figure_4_2);
% figure
% hist(sumSelect(sumSelect>0)/(repeats+1),100)

figure
%% Get p_upper and p_lower
[tb,xb,names] = sbiosimulate(VPmodel);

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

%% Select virtual population
[~,OutPts] = SimulateVPs(VPmodel,pOut,'speed_test',0);

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0); % fetch the NHANES data
mu = m.mu;                    % log-normal distribution parameters
sigma = m.Sigma;              % log-normal distribution parameters

[selection, gofRun] = prevalence_select_func(mu,sigma,log(VPChar));

%% Make plots
NumVPs=numel(Pfinal(1,:));
PriorP=rand(NumVPs,1);

for i=1:22
    ind=num2str(i);
    Pname=strcat('k_{',ind,'}');
    ParameterNames{i}=Pname;
end

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);

P_scaled=(Pfinal-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
P_scaled1=P_scaled;
%ParameterNames=[ConstantParameters];
%ParameterNames{23}='k22';
subplot(2,2,1)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)
ylim([0 1])
alpha(0.3)
C1=corrcoef(P_scaled);

NumVPs=numel(Pfinal(1,selection));

PriorP=rand(NumVPs,1);

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);

P_scaled=(Pfinal(:,selection)-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
C2=corrcoef(P_scaled);
subplot(2,2,3)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)

ylim([0 1])
alpha(0.3)

Cmax=max(max(max(C1-eye(size(C1)))),max(max(C2-eye(size(C2)))));
Cmin=min(min(min(C1-eye(size(C1)))),min(min(C2-eye(size(C2)))));

subplot(2,2,2)
imagesc(C1,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square
subplot(2,2,4)
imagesc(C2,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square

figure_4_1 = sprintf('Results/figure4_M%d_S%d_%s.fig', method_id, N_size, yourname);
figure_4_2 = sprintf('Results/figure4_M%d_S%d_%s.png', method_id, N_size, yourname);
saveas(gcf, figure_4_1);
saveas(gcf, figure_4_2);

%%plot for comparison

end