function [cc] = whichcost(p_pps, c_stra, c_max, p_stra)
%% Determine which ellipsoid to optimize to by returning the corresponding boundary value.
% Suppose we want to generate Num_PPs plausible patients and we are now generating the k th pp, then p_pps should
% be equal to k / Num_PPs; c_stra is the n-1 vector returned by
% MCmultinormalci(); c_max is the prespecified largest boundary; p_stra is
% returned by findpi().

cum_p_stra = cumsum(p_stra);
c_stra = [ c_stra, c_max];
cc = c_stra(sum( p_pps > cum_p_stra) + 1 );
