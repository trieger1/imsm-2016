function [hist_score,selected,pOut] = ...
    OptimizeVPgeneration(sf_log10, p_include, VPs, runs)
% OptimizeVPgeneration(sf_log10, p_include, VPs, runs)
%   Selects virtual patients from the cohort of plausible patients
%   previously generated. And scores them for normality using a series of
%   K-S tests.
%
%   Inputs:
%       sf_log10 - 1x1 scalar, log10(scaling factor) for prob. of inclusion
%       p_include - nx1 vector, probability of inclusion for each plausible
%           patient (pre-scaling-factor).
%       VPs - nxm matrix, plausible patients for inclusion
%       runs - 1x1 scalar, number of times to attempt the fitting
%   
%   Where n is the number of plausible patients amd m is the number of
%   observations being matched to data.
%
%   Outputs:
%       hist_score  - 1x1 scalar, sum of all univariate K-S tests across
%           all runs.
%       selected    - nx1 boolean, 
%       pOut        - 1x1 scalar, p-value for K-S tests
%

%% 1. Setup:
sf = 10.^sf_log10(1); % scale factor input is log10
data_dim = numel(VPs(1,:));

% Pre-allocate output vectors:
pVal        = zeros(data_dim,1);
ksstat      = zeros(data_dim,1);
cv          = zeros(data_dim,1);
hist_score  = zeros(runs,1);
num_vps     = size(VPs,1);
%selectedsum=zeros(num_vps,1);

%% 2. Perform Selection and Univariate K-S Tests:
for j = 1:runs
    r = rand(num_vps,1);
    selected = r < p_include*sf;
    %selected=selectedtemp>threshold;
    num_final = sum(selected);
    if num_final > 1
        for i = 1:data_dim
            %Test if dist. is standard normal
            [~,pVal(i),ksstat(i),cv(i)] = kstest(VPs(selected,i));
        end
        hist_score(j) = sum(ksstat);
        pOut = pVal;
    else
        hist_score(j) = 1*data_dim;
    end
end
hist_score = sum(hist_score)/runs;

end % EoF