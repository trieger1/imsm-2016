addpath('Results');
addpath('Source');
addpath('Source/NHANES');
thdl = xptread('HDL_G.XPT');
ttc = xptread('TCHOL_G.XPT');
ttrig = xptread('TRIGLY_G.XPT');

% Merge based on the key SEQN:
c1 = join(thdl,ttc,'Keys','SEQN');
c1 = join(ttrig,c1,'Keys','SEQN');

load('Results/tempWorkspace');
% Log-transform the relevant columns:
lnHDL = log(c1.LBDHDD);
lnLDL = log(c1.LBDLDL);
lnTC = log(c1.LBXTC);


% real data


rdata=[lnHDL,lnLDL,lnTC];
scatter3(lnHDL,lnLDL,lnTC);
cc = zeros(3239, 1);
for i = 1:3239
    cc(i) = (rdata(i, :) - mu) / sigma * (rdata(i,:) - mu)';
end
[pca_coef, pca_score, pca_latent] = princomp(rdata);

%some lines in prevalence_select_func
root_inv_sigma = sqrtm(inv(sigma));
VPs = (VPs - mu1) * root_inv_sigma;

% K-S?
omegaaa = diag( 1 ./ sqrt(diag(sigma)) );
real_sigma =omegaaa * sigma *omegaaa;

kss = zeros(1, 1000);
for j = 1:1000
    xxx = mvnrnd([0 0 0], real_sigma, 500);
    ks = zeros(1,3);    
    for i = 1:3
       [~,~, ks(i),~] = kstest(xxx(:,i));
    end
    kss(j) = sum(ks);
end

kss_2 = zeros(1, 1000);
for j = 1:1000
    xxx_2 = mvnrnd([0 0 0], eye(3), 500);
    ks = zeros(1,3);    
    for i = 1:3
       [~,~, ks(i),~] = kstest(xxx_2(:,i));
    end
    kss_2(j) = sum(ks);
end

kss_nhans = zeros(1000, 3);
for j = 1:1000
    xx_nhans = mvnrnd(mu, sigma, 500);
    root_inv_sigma = sqrtm(inv(sigma));
    sigmaVPs    = repmat(diag(sigma)',500,1);
    mu1 = repmat(mu, 500, 1);
    xx_sdn = (xx_nhans - mu1)* root_inv_sigma;
    xx_nsdn = (xx_nhans - mu1)./sigmaVPs.^0.5;
    ks = zeros(2, 3);    
    for i = 1:3
       [~,~, ks(1,i),~] = kstest(xx_sdn(:,i)); %ks for standard transform
       [~,~, ks(2,i),~] =  kstest(xx_nsdn(:,i)); % ks for non-standard transform
    end
    kss_nhans(j, 1) = sum(ks(1,:));
    kss_nhans(j, 2) = sum(ks(2,:));
end

% PCA
load('Results/log_real_data.mat');
[V_nhan, D_nhan] = eig(sigma);
rot_data = (rdata - repmat(mu, 3239, 1) ) * V_nhan;


% Plots after generation

VirtualP = VPChar( selection,:);
scatter3(log(VPChar(:,1)), log(VPChar(:,2)), log(VPChar(:,3)));
hold on
scatter3(log(VirtualP(:,1)), log(VirtualP(:,2)), log(VirtualP(:,3)));
scatter3(rdata(:,1), rdata(:,2), rdata(:,3));

hold off

%Calculate the K-S Statistics for original data
stand_rdata = (rdata - repmat(mu, 3239, 1)) * sqrtm(inv(sigma));
ks_data = zeros(1,3);    
for i = 1:3
   [~,~, ks_data(i),~] = kstest(stand_rdata(:,i));
end
ks_data = sum(ks_data);
    
% Analysis for PCA method
rot_VPChar = (log(VPChar) - repmat(mu, 20000, 1)) * V_nhan;
scatter3(rot_data(:,1), rot_data(:,2), rot_data(:,3));
max(rot_VPChar)
hold on
scatter3(rot_VPChar(:,1), rot_VPChar(:,2), rot_VPChar(:,3));


%%Calculating p's and c's

NN=30000;
R=mvnrnd(mu,sigma,NN);
mu1=repmat(mu,NN,1);
distance=diag((R-mu1)*inv(sigma)*(R-mu1)');
distance2=sort(distance);
[quantile(distance2,0),quantile(distance2,0.2),quantile(distance2,0.4),quantile(distance2,0.6),quantile(distance2,0.8),quantile(distance2,1)]
c1 = sqrt(1.1);
c2 = sqrt(2);
c3 = sqrt(3);
c4 = sqrt(5.5);
c5 = sqrt(100);
p5 = 1 /5 * c5^3 /( c5^3 - c4^3);
p4 = 1/5 * c4^3 / ( c4^3 - c3^3) - p5 * c4^3 / c5^3;
p3 = 1/5 * c3^3 / ( c3^3 - c2^3) - p4 * c3^3 / c4^3 - p5 * c3^3 / c5^3;
p2 = 1/5 * c2^3 / ( c2^3 - c1^3) - p3 * c2^3 / c3^3 - p4 * c2^3 / c4^3 - p5 * c2^3 / c5^3;
p1 = 1/5  - p2 * c1^3 / c2^3 - p3 * c1^3 / c3^3 - p4 * c1^3 / c4^3 - p5 * c1^3 / c5^3;


%%Test how to use struct
struct1.time = 123;
struct1.pps = 100;
struct1(2).time = 321;
struct1(2).pps = 200;
mean(struct1) %this is not working.
%try this 
mean([struct1.time]);

%%generate from normal and do a selection
fake_PPs = mvnrnd(mu, sigma, 4000);
[fake_selection, fake_gof, ~, ~, fake_prob_include, fake_beta] = prevalence_select_func(mu,sigma,fake_PPs);


    fake_PPs  = log(VPChar);
    root_inv_sigma = sqrtm(inv(sigma));
    sigmaVPs    = repmat(diag(sigma)',10000,1);
    mu1 = repmat(mu, 10000, 1);
    fake_PPs_sdn = (fake_PPs - mu1)./sigmaVPs.^0.5;
    ks = zeros(1, 3);    
    for i = 1:3
       [~,~, ks(i),~] = kstest(fake_PPs_sdn(:,i)); %ks for standard transform
    end
    kss_fake = sum(ks);

