# Documentation to new or modified functions
## `RunMethod()`
The function `RunMethod()` will generate plausible patients of size 100, 500, 1000, 5000, 10000, and for size 100, 500, 1000, it will replicate 5 times and take the average of the result since a single run for these sizes seems unstable.

Input:

`method_id` (integer) The same interpretation as the `method` argument in `TestSpeed`.

`yourname`  (character string) Your name, for example, 'Yifei'

Output:
`sm_seq` (array of structure) will be saved in 'Result/SpeedTestPlot_M%d_%s.mat' file, where %d will be replaced by the `method_id`, and %s will be replaced with `yourname`. This file contains the numbers that will be useful for ploting.

Meanwhile, the '.mat' file that each individual `TestSpeed()` call produces will also be saved in the 'Result/SpeedTest_M%d_S%d_B%d_%s.mat' file, where the first %d is `methodid`, the second %d is the size of PPs, the third %d is the batch number of your replicates for size 100, 500, 1000, and %s is again `yourname`. Hence in total you will have 5*3 +1 +1 + 1 = 18 .mat files per each call of `RunMethod()`

I modified the code somewhere so it's not printing the optimization path when minimizing $\beta$, so don't be intimidated if you see nothing when the code is running.

Example:

```Matlab
RunMethod(1, 'Yifei')	% Yifei runs the original method
```

## Assignment for running:
If you have already run the original Pfizer method from Lukas' codes, you can skip method 1 and just run the new method.

| Names      | Method      |
| -----------|-------------|
| Yifei      | 1, 7        |
| Angie      | 1, 6        |
| Bekah      | 1, 5        |
| Yuzhou     | 1, 4        |

## `TestSpeed()`
The modified 'TestSpeed' function contains the following arguments:

`N_PPs` (integer) is the number of plausible patients you want to generate.

`method` (integer) refers to which method you want to use.
1 is the original method by Pfizer.
2 is the method that adds the penalty of low density to original cost function.
4 is the ellipsoid method + their original cost function.
5 is the ellipsoid method + (1,2,3,4,8) of their original cost function.
6 is the PCA method.
7 is the stratified method.

`lambda` (double) will be used in method 2, 4, 5, 7, which is the constant in front of the additional score function that we created, usually set to balance the original score and our created one. Suggested lambda for method 7 is 1, you can also try 0.1 .

`c2` (double) is the c^2 used in the ellipsoid methods(4, 5). If you use other method, just pass an arbitrary number.

`n_region` (integer) is the number of regions that we want to shoot to in method 7. This number grows larger as `N_PPs` grows larger.

`c_max` (double) is the c^2 of our largest ellpsoid for method 7. Recommended to be 100

`comment` (character) is a string that just in case you want to write down something about this running.

`result_name` (character) is a string to name the generated .mat file that contains the results, for example, 'SpeedTest'.

## `GenerateVPs`

`GenerateVPs` generates plausible patients ('VP' is a misnomer).

`method`, `lambda`, `c2` have the same usage as those in `TestSpeed()`.

`c_stra`, `c_max`, `p_stra` are used in the nested region method, with `c_stra` being the boundaries of the nested ellipsoids up to n_region -1, `c_max` being the boundary of the largest ellipsoid, and `p_stra` being the proportions of pps optimizing toward nested regions.

## `ScoreModelSS()`

`ScoreModelSS()` uses different cost function for different approaches.

`method`, `lambda`, `c2` have the same usage as those in `TestSpeed()`.

`c_stra`, `c_max`, `p_stra` have the same usage as those in `GenerateVPs()`.

`p_pps` is the fraction of plausible patients that have been generated (counting the current one). If you are currently generating the k th plausible patient, `p_pps` should be k / Num_PPs .

# Other modifications

This TestSpeed function will output the number of PPs that it has generated along its running, if you don't want this output, you can comment line 123-124 in 'GenerateVPs.m' file.

For method 7, time/pp is about 1.5 seconds, so adjust your size for PPs accordingly.

