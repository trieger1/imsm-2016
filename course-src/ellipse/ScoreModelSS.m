function [score] = ScoreModelSS(DataSS,ModelSS,RangeSS,SteadyStateFlag, method, lambda, c2, p_pps, c_stra, c_max, p_stra)
%% See ReadMe_Yifei.md for detailed description.

global mu omega V D;
flg=SteadyStateFlag;

MidRange=(RangeSS(:,1)'+RangeSS(:,2)')/2;


%% Redefine for convenience, grab only the data we are interested in
DataSS=DataSS(flg);
ModelSS=ModelSS(flg);
RangeSS(:,1)= RangeSS(flg, 1);
RangeSS(:,2)= RangeSS(flg, 2);
MidRange=MidRange(flg);
%RangeSS
%%%% SSE - SSE at points defined by range
score=(MidRange-ModelSS).^2 - (MidRange-RangeSS(:,1)').^2;
score=score./MidRange.^2;
ppSS = zeros(1, 3);
ppSS(1) = ModelSS(5) + ModelSS(6);  % HDL
ppSS(2) = ModelSS(9); % LDL
ppSS(3) = sum(ModelSS(5:7));  %TC

if sum(ppSS <= 0) == 0 % if nothing in ppSS is negative, save to do a log transformation
   ppSS = log( ppSS / 2.79 * 38.66) ;   %Unit conversion to match NHANES, and take a log
   ddSS = (ppSS - mu) * omega * (ppSS - mu)';
else 
    score = 1e256;
    return
end

if method == 2
% alter cost function by adding penalty of low density
    score=sum(score(score>=0)) + densipenalty(ddSS, lambda);
elseif method == 4
% ellipsoid method, adding to the original score.    
    score1 = sum(score(score>=0));
    score4 = lambda * max(0, ddSS - c2);
    score = score1+ score4;
elseif method == 5
    %ellipsoid method. Only use the original score of 1, 2, 3, 4 and 8
    score1 = score([1, 2, 3, 4, 8]);
    score1 = sum(score1(score1>=0));
    score5 = lambda * max(0, ddSS - c2);
    score = score1 + score5;
elseif method == 6
    %PCA method, basically recentering and rotating the data and come up
    %with new bounds.
    score1 = score([1, 2, 3, 4, 8]);
    %score1 = score;
    score1 = sum(score1(score1>=0));
    
    NewModelSS = ppSS(1:3);
    NewModelSS = (NewModelSS - mu) * V;
    %NewModelSS
    NewRangeSS = zeros(2, 3);
    NewRangeSS(1, :) = [ -0.15 -1.20 -1.4];
    NewRangeSS(2, :) = [ 0.8  1.2  2.3];
    %NewRangeSS
    NewMidRange = (NewRangeSS(1,:) + NewRangeSS(2,:))/2;
    %NewMidRange
    score6=(NewMidRange-NewModelSS).^2 - (NewMidRange-NewRangeSS(1,:)).^2;
    score6 = score6 ./ sqrt(diag(D))';

    score6 = sum(score6(score6>=0));
    score = score1 +  score6;
elseif method == 7
    %Stratified method. This is my favorite. Change different function as
    %the generation process goes on.
    
    score = sum(score(score>=0));
    %'hello from ScoreModelSS method 7'
    score7 = lambda * max(0, ddSS - whichcost(p_pps, c_stra, c_max, p_stra));
    score = score + score7;
elseif method == 1
    score = sum(score(score>=0));
end

end

