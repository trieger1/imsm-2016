function [selected,hist_score,hist_mu,hist_std,p_include,sf]= ...
    prevalence_select_func(mu,sigma,VPs)
% prevalence_select_func(mu,sigma,VPs)
%   Main selection function for converting plausible patients into virtual
%   patients. Requires inputs:
%       mu    - m x 1 vector, log-normal parameter of actual distribution
%       sigma - m x 1 vector, log-normal parameter of actual distribution
%       VPs   - n x m matrix, plausible patient values where
%               n is the number of plausible patients (1 row per patient)
%               m is the number of observations to be matched (same size as
%               mu).
%

%% 1. Setup:
num_vps = numel(VPs(:,1)); % number of plausible patients
dim     = numel(mu); % number of distributions to match (1d histograms)
mu1     = repmat(mu,num_vps,1); % for use in scaling VPs

%% 2. Generate Test Data:
% Based on mu and sigma, generate multi-variate PDF (assume Gaussian).
% For real data will need individual data, or infer distribution using known
% distr. and cross-correlations. Assumining multi-variate normal for now.

% Data input check:
%issymmetric(sigma);  %check symmetry
%[~,posdef] = chol(sigma); % check positive definite
sigmaVPs    = repmat(diag(sigma)',num_vps,1);
VPs         = (VPs-mu1)./sigmaVPs.^0.5; % Normalize VPs
data_pdf     = mvnpdf(VPs.*sigmaVPs.^0.5+mu1,mu,sigma); % data density

%root_inv_sigma = sqrtm(inv(sigma));
%VPs = (VPs - mu1) * root_inv_sigma;
%data_pdf = mvnpdf(VPs, zeros(1, 3), eye(3)); 

%% 3. Calculate the Probability of Inclusion for Each Plausible Patient:
% Probability density (PDF) at the VP's location divided by the density of
% plausible patients in the same region. The plausible patient density is
% created by taking the volume of the sphere to the num_pts nearest
% neighbors (default = 5).
num_pts     = 5; % Nearest neighbor points for density calculation
vp_tree     = KDTreeSearcher(VPs,'BucketSize',1);
[~,DVPs]    = knnsearch(vp_tree,VPs,'K',num_pts);  % Assumes non-identical VPs
BallVolume2 = nsphereVolume(dim,max(DVPs'));
vp_density = num_pts./BallVolume2; % VP density based on num_pts nearest neighbors
p_include = (data_pdf)./vp_density'; % Calculate the probability of inclusion

%% 4. Optimize the Scaling Factor for Selection of VPs:
runs    = 10; % Number of times to try the fitting
fopt    = @(p)OptimizeVPgeneration(p,p_include,VPs,runs);
sf_max  = 1/max(p_include); % Maximum scaling factor
sf_lower = 0; % lower bound for the scaling factor on probability of inclusion
sf_upper = log10(1000*sf_max); % upper bound for the scaling factor on the probability of inclusion

options_sa = saoptimset('TolFun',1e-15,'ReannealInterval',50000,...
    'InitialTemperature',0.5,'MaxIter',1000,'Display','off',...
    'TemperatureFcn',@temperatureboltz,'AnnealingFcn',@annealingboltz,...
    'AcceptanceFcn',@acceptancesa);
%   options=saoptimset('TolFun',1e-15,'ReannealInterval',50000,'InitialTemperature',1,'MaxIter',1000,'TemperatureFcn',@temperatureboltz,'AnnealingFcn', @annealingboltz,'AcceptanceFcn',@acceptancesa);
k   = simulannealbnd(fopt,log10(sf_max),sf_lower,sf_upper,options_sa); % optimize the log10(scaling factor) value
sf  = 10.^k(1); % scaling factor transformed back from log10

%% 5. With the Optimal Scaling Factor, [Re]select VPs:
% Iterate several times (default = 100) and select the best fit and mean
% fits.
num_tests = 100;
hist_score_temp = zeros(num_tests,1);
selected_temp   = zeros(num_vps,num_tests);
for i=1:num_tests
    [hist_score_temp(i),selected_temp(:,i)]= OptimizeVPgeneration(k,p_include,VPs,1);
end
[hist_score,I]  = min(hist_score_temp);
selected        = logical(selected_temp(:,I));
hist_mu         = nanmean(hist_score_temp);
hist_std        = nanstd(hist_score_temp);

%% Optional Code for Plotting Histograms:
%samplesize=sum(selected);

%r = mvnrnd(mu,sigma,samplesize); % sample distribution
%mu2=repmat(mu,samplesize,1);

%sigma2=repmat(diag(sigma)',samplesize,1);

%r=(r-mu2)./sigma2.^0.5; % normalize for consistency

%save('prevelance_selection_results')
% [n1,X1]=hist(r(:,1),20);
% [n2,X2]=hist(r(:,2),20);
% [n3,X3]=hist(r(:,2),20);

%{
figure

% Goal
subplot(3,5,1)
hist(r(:,1),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,2)
hist(r(:,2),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,3)
hist(r(:,3),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,4)
scatter(r(:,1),r(:,3))
set(gca,'xlim',[-3 3]);
set(gca,'ylim',[-4 4]);
subplot(3,5,5)
scatter(r(:,2),r(:,3))
set(gca,'xlim',[-3 3]);
set(gca,'ylim',[-4 4]);


% Initial
subplot(3,5,6)
hist(VPs(:,1),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,7)
hist(VPs(:,2),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,8)
hist(VPs(:,3),20);
set(gca,'xlim',[-5 5]);
subplot(3,5,9)
scatter(VPs(:,1),VPs(:,3))
set(gca,'xlim',[-3 3]);
subplot(3,5,10)
scatter(VPs(:,2),VPs(:,3))
set(gca,'xlim',[-5 5]);


% Final
subplot(3,5,11)
hist(VPs(selected,1),X1);
set(gca,'xlim',[-5 5]);
subplot(3,5,12)
hist(VPs(selected,2),X2);
set(gca,'xlim',[-5 5]);
subplot(3,5,13)
hist(VPs(selected,3),X3);
set(gca,'xlim',[-5 5]);
subplot(3,5,14)
scatter(VPs(selected,1),VPs(selected,3))
set(gca,'xlim',[-3 3]);
set(gca,'ylim',[-4 4]);
subplot(3,5,15)
scatter(VPs(selected,2),VPs(selected,3))
set(gca,'xlim',[-3 3]);
set(gca,'ylim',[-4 4]);
figure(gcf);

%}

end % EoF
