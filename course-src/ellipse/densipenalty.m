function [penal] = densipenalty(distance2, lambda)
d = distance2;
if d <= 1.1
    pp = 0;
elseif d <= 2
    pp = 1.1;
elseif d <= 3
    pp = 2;
elseif d <= 5.5
    pp = 3;
else pp = 5.5;
end

penal = pp * lambda;
end