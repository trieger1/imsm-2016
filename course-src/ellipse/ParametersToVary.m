function [ConstantParameters,ConstantParameterValues] = ParametersToVary(m1)

k=1;

%%%% get a list of the repeated assignments, to exclude. Fix Dep also
%%%% reorders the model to be compatible with 2010b

[~,~,rule_names,~,m1] = FixDep(m1);

%Rulenames=m1.rules;
%%%% load a list of parameter names that I don't want to vary

%load('Exclude.mat')

%%% Add these to the Repeated Assignment which it makes no sense to vary
%Rulenames=[Rulenames ; Exclude_from_Fit_list];

all_parameters = sbioselect(m1.parameters);

for i=1:numel(all_parameters)
    
    check(i)=0;
    
    tmp_name = all_parameters(i).Name;
    
    %%%%%%%%%
    %%% Check if rule exist, or is on the no fly list
    
    for j=1:numel(rule_names)
        Out=strcmp(rule_names(j), tmp_name);
        if Out==1
            check(i)=check(i)+1;
        end
    end
    
    if check(i)==0
        ConstantParameters{k}=tmp_name;
        ConstantParameterValues(k)=all_parameters(i).Value;
        k=k+1;
    end
    
end

%%%tidy up

ConstantParameters=ConstantParameters';
ConstantParameterValues=ConstantParameterValues';

%%%remove irrelevant parameters
zeroPs=ConstantParameterValues==0;
ConstantParameters=ConstantParameters(~zeroPs);
ConstantParameterValues=ConstantParameterValues(~zeroPs);

end

