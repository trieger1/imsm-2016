function sm = TestSpeed(N_PPs, method, lambda, c2, n_region, c_max, comment, result_name)
% TestSpeed.m (Modified by Yifei Liu)
%   Evaluates the performance of 'GenerateVPs', and prevelance_select_func and
%   generates a few key metrics as a output struct including:
%   
%   sm.gof - goodness-of-fit to the histograms
%   sm.selection_efficiency - how efficient was plausible patients --> VPs
%   sm.time_per_pp - how much time did it take to generate all of the PPs
%   sm.time_per_vp - time_per_pp * selection_efficiency
%   sm.cputime_pp - total cputime for generating pp
%   sm.cputime_total - total cputime for calling TestSpeed()
%   sm.n_vps - number of vps generated
%   sm.n_pps - number of pps generated, just for convenience
%
%% Explanation of arguments
% See ReadMe_Yifei.md for detailed explanation.

cpu_time_start = cputime;

%% Matlab startup
% addpath('Results');
% addpath('Source');
% addpath('Source/NHANES');

% mu is the estimated mean of NHANES data, sigma is the estimated
% covariance matrix (may use a different name since there is a function in
% matlab named sigma(), it happened once that the predence order messed
% up), omega is the inverse of sigma, V and D are the eigenvector (column
% of V) matrix and matrix with eigenvalue as diagonal. Declared as global
% variable to pass to ScoreModelSS() function.

global mu sigma omega V D; 

if nargin == 0
    N_PPs = 500; % Set the number of plausible patients to generate
end

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0); % fetch the NHANES data
mu = m.mu;                    % log-normal distribution parameters
sigma = m.Sigma;              % log-normal distribution parameters

omega = inv(sigma);           % inverse of sigma
[V, D] = eig(sigma);

%% Get the boundaries (ci's) for nested region method and the proportions (pi's);
if method == 7 %if we use nested region method
    c_stra = MCmultinormalci(mu,sigma, n_region); %Calculate boundaries
    p_stra = findpi(c_stra,c_max); % Use boundaries to calculate proportions
else
    c_stra = 1; %It doesn't really matter what number is here. Simply assign a value to pass to GenerateVPs()
    p_stra = 1;
end

%% Step 1. Generate plausible patient cohort:
%%% Start the timing clock
PPs_start_time1 = cputime;
StartTime1=tic;
GenerateVPs('SpeedTest',N_PPs, method, lambda, c2, c_stra, c_max, p_stra);
run_time1=toc(StartTime1); % Save finishing time 1 (create a run_time2 to compare against the base case)
PPs_time = cputime - PPs_start_time1;
load('Results/SpeedTest.mat'); % Loads the results of the GenerateVPs step

%% Step 2. Simulate the new plausible patients to steady state:
[~,OutPts] = SimulateVPs(VPmodel,pOut,'speed_test',0);

% Create NHANES outputs:
VPChar(:,1)=OutPts(:,5)+OutPts(:,6); %HDL
VPChar(:,2)=OutPts(:,9); % LDL
VPChar(:,3)=OutPts(:,7)+OutPts(:,5)+OutPts(:,6); %TC

% Unit conversion to match NHANES
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;

%% Step 3. Select the virtual population based on the NHANES data distribution
[selection, gof] = prevalence_select_func(mu,sigma,log(VPChar));

%% Step 4. Get key metrics on performance
total_cpu_time = cputime - cpu_time_start;
%How well does your histograms match the data? (lower the better)
sm.gof = gof;
%How many plausible patients do you need to make 1 VP? (lower the better)
sm.selection_efficiency = numel(selection)/sum(selection);
%How long to make 1 plausible patient (shorter the better)
sm.time_per_pp = (PPs_time)/N_PPs;
%How long to make 1 VP? (shorter the better)
sm.time_per_vp = sm.time_per_pp*sm.selection_efficiency;
%How long is the cpu time for generating pps?
sm.cputime_pp = PPs_time;
%How long is the cpu time for all this procedure?
sm.cputime_total = total_cpu_time;
%How many virtual patients you generated in the end?
sm.n_vps = sum(selection);
%Record the number of PPs generated.
sm.n_pps = N_PPs;
%% Clean-up
save(['../Results/', result_name]); % Save the output before exiting

end