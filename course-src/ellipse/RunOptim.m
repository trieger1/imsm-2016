function [ ScoreFinal,t1,x1] = RunOptim(p,ConstantParameters,VPmodel,t_baseline,x_baseline,C,FitIndices,RangeSS,SteadyStateFlag, method, lambda,  c2, p_pps, c_stra, c_max, p_stra)
% RunOptim(p,ConstantParameters,VPmodel,t_baseline,x_baseline,C,FitIndices,RangeSS,SteadyStateFlag)
%   Takes in an initial parameter guess for a plausible patient, adds those
%   guesses to the SimBiology model as variants and simulates to steady
%   state.
% 
% Added arguments that need to be passed to ScoreModelSS

%% Initial Setup:
ScoreFinal = 0; % Initialize the final score
p=10.^p;        % Parameters are passed as log10(p)

% Sets an option for dynamic fitting, not used for steady state fitting
% [usual case]
% Acceptable Range for dynamic fitting- this defines
% how close the virtual patients need to be to the mean for each species
% variable
Range_fold=[1 1 1 1 1 1 1 1 1];

%% 1. Create New Variant
% This step adds a "variant" to the simbiology model, that is an
% alternative parameterization of the model, which is simulated below.
NewP2 = addvariant(VPmodel,'NewP2');
% Writes the proposed parameters to the new variant:
for i=1:numel(p)
    addcontent(NewP2,{'parameter',ConstantParameters{i},'Value',p(i)})
end

%% 2. Simulate and score results
% Use try, but with no catch - may fail due to integration issues. 
% If integration fails, assume something bad has happened and we don't
% want that VP (add a very high score)
try
    [t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state

    % This code should be inactive, but leaving it here just-in-case:
    for i = 1:numel(FitIndices)
        if SteadyStateFlag==0
            Temp = ScoreModel(t_baseline(:),x_baseline(:,FitIndices(i)),t1(:),x1(:,FitIndices(i)),Range_fold(i));
            ScoreFinal=ScoreFinal+Temp;
        end
    end
    
    % Score results (steady state [usual] case):
    if sum(SteadyStateFlag>0)
        Temp = ScoreModelSS(x_baseline(end,:),x1(end,:),RangeSS,SteadyStateFlag, method, lambda,c2, p_pps, c_stra, c_max, p_stra);
        ScoreFinal = ScoreFinal + Temp;
    end 
catch
    ScoreFinal = 1e256; % If the simulation terminated unsuccessfully, just assign a very large value
end

%% 3. Clean-up (delete variants)
% Delete the last variant that was added to the model - need to do this
% to keep things organized
delete(VPmodel.variant(numel(VPmodel.variant)))

end

