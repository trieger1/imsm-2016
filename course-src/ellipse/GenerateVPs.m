function GenerateVPs(My_model_save, NumVPs, method, lambda, c2, c_stra, c_max, p_stra)
%% 
%% 1. Load model into matlab workspace
dirSave=strcat('../Results/',My_model_save);
load('My_Model.mat')

% Rename the model something sensible
VPmodel=van_de_pas_mod1;

% Reseed the random number generator:
rng('shuffle')

% Accelerate the model - need to setup mex compliler to do this:
sbioaccelerate(VPmodel);

% Simulate to generate baseline VP, grab names:
[tb,xb,names] = sbiosimulate(VPmodel);

%% 2. Load free parameters
% Do not include 'parameters' redefined by SimBiology in repeated
% assignments, or parameters which are known with high accuracy.

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

%% 3. The Integration Configuration

C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);

%%%% 3. Identify the outputs of the model which you are going to constrain
% These should match up with the oder of SS_lower and SS_upper.

%%%%%%%%%%%%%%%%%% Find Indices to fit %%%%%%%%%%%%%

A=strcmp(names,'intestine.free_cholesterol');
FitIndices(1)=find(A==1);

A=strcmp(names,'intestine.cholesterol_ester');
FitIndices(2)=find(A==1);

A=strcmp(names,'liver.free_cholesterol');
FitIndices(3)=find(A==1);

A=strcmp(names,'liver.cholesterol_ester');
FitIndices(4)=find(A==1);

A=strcmp(names,'plasma.HDL_free_cholesterol');
FitIndices(5)=find(A==1);

A=strcmp(names,'plasma.HDL_cholesterol_ester');
FitIndices(6)=find(A==1);

A=strcmp(names,'plasma.non_HDL_cholesterol');
FitIndices(7)=find(A==1);

A=strcmp(names,'periphery.cholesterol');
FitIndices(8)=find(A==1);

%A=strcmp(names,'LDL_estimate_NHANES');
%FitIndices(9)=find(A==1);

%%%%%%%% Flag the states here that are being compared at steady-state

SteadyStateFlag=true(numel(names),1);
%SteadyStateFlag(9)=false; % uncomment to match the LDL later

RangeSS=-1*ones(numel(names),2);

RangeSS(:,1)=SS_lower';
RangeSS(:,2)=SS_upper';

RangeSS(~SteadyStateFlag,:)=nan;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Add variant of initial guess
NewP=addvariant(VPmodel,'NewP');

for i=1:numel(ConstantParameters)
    addcontent(NewP,{'parameter',ConstantParameters{i},'Value',ConstantParameterValues(i)})
end

%% 4. Prepare scoring function 'RunOptim' -
% Tell MATLAB I'm interested in 'p', tb and xb are the mean VP I'm
% fitting around.

%f=@(p)RunOptim(p,ConstantParameters,VPmodel,tb,xb,C,FitIndices,RangeSS,SteadyStateFlag, method, lambda, c2);

% Options for optimization - note, problem specific! Could probably use
% local optimization to accomplish this same thing:
options2=saoptimset('ObjectiveLimit',0.005,'TolFun',1e-5,'Display','off',...
    'ReannealInterval',500,'InitialTemperature',0.5,'MaxIter',400,'TemperatureFcn',...
    @temperatureboltz,'AnnealingFcn', @annealingboltz,'AcceptanceFcn',@acceptancesa);

% Predefine number of VPs you want and allocate storage arrays:
pOut=zeros(numel(ConstantParameters),NumVPs);
Score_V= zeros(NumVPs,1);
%'hello'
%% 5. Vary parameters to form initial guess and perform optimization:
k=1;
while k<=NumVPs
    % New Initial Range
    Initial=p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
    p_upperSA=log10(p_upper);
    p_lowerSA=log10(p_lower);
    InitialSA=log10(Initial);
    %'morehello'
    % Tell MATLAB I'm interested in 'p', tb and xb are the mean VP I'm
    % fitting around.

    % Moved objective function here since for nested region method, the
    % objective function will be changed for different batches of plausible
    % patients. Also pass arguments that are needed by ScoreModelSS()
    % function
    
    f=@(p)RunOptim(p,ConstantParameters,VPmodel,tb,xb,C,FitIndices,RangeSS,SteadyStateFlag, method, lambda, c2, k/NumVPs, c_stra, c_max, p_stra);
    % Pass initial guess, scoring function, bounds, to optimization routine.
    try
        [p,score]= simulannealbnd(f,InitialSA,p_lowerSA,p_upperSA,options2);
        if score<2
            pOut(:,k)=10.^p;
            Score_V(k)=score;
            k=k+1;
        end
    end
end

%% 6. Save outputs
%overwrite any changes to the model
load('My_Model.mat')

save(dirSave,'pOut','VPmodel','Score_V')
end
