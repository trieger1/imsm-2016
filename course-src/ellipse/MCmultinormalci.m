function [vector]=MCmultinormalci(mu,sigma,n)
%% Function to calculate the boundaries (ci's) of nested region method using Monti
%% Carlo simulations.
% Mu is the estimated mean of NHANES data, and sigma is
% the estimated covariance matrix, n is the number of regions. A vector of
% length n-1 will be returned, being the first n-1 boundaries, while the
% largest boundary needs to be specified by user.

NN=1000000; % Number of data points to sample from N(mu, sigma)

R=mvnrnd(mu,sigma,NN);
mu1=repmat(mu,NN,1);
temp = (R-mu1)*inv(sigma); 

distance2=zeros(NN,1);
for i=1:NN
   distance2(i)=temp(i,:)*(R(i,:)-mu1(i,:))';
end % Calculate c^2 = (r - mu)^T (sigma)^-1 (r - mu) for each r

q=(1:(n-1))/n;
vector=quantile(distance2,q); %return the quantile.
