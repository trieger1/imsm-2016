function sm_seq = grab_summary_data(method_id, yourname)
%%Recover the correct summary from original data
%%It's useless now.

N_PPs_seq = [100, 500, 1000, 5000, 10000];
N_size =  numel(N_PPs_seq);
N_rep = 5;

for i = 1:N_size
    if i <= 3
       for j = 1:N_rep
	   result_name = sprintf('Results/SpeedTest_M%d_S%d_B%d_%s.mat', method_id, N_PPs_seq(i), j, yourname);
       load(result_name);
       sm_temp(j) = sm;
       end
       sm_seq(i).gof = mean([sm_temp.gof]);
       sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
       sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
       sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
       sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
       sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
       sm_seq(i).n_vps = mean([sm_temp.n_vps]);
       sm_seq(i).n_pps = mean([sm_temp.n_pps]);
    else
    result_name = sprintf('Results/SpeedTest_M%d_S%d_%s.mat', method_id, N_PPs_seq(i), yourname);
    load(result_name);
    sm_seq(i) = sm;
    end
end
result_recover_name = sprintf('Results/SpeedTestPlotRecover_M%d_%s.mat', method_id, yourname);
save(result_recover_name, 'sm_seq');
end

