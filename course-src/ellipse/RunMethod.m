function sm_seq = RunMethod(method_id, yourname)
%% RunMethod() funtion generates plausible patients with default sizes 100, 500, 1000, 5000, 10000
%% using one of the following methods:
%
% method 1(Original)
% method 2(Adding penalty for low density)
% method 4(ellipsoid method without changing the original one)
% method 5(ellipsoid method. Only use the original score of 1, 2, 3, 4 and 8)
% method 6(PCA, basically recentering and rotating the data and coming up with new bounds.)
% method 7(Nested region. Use different cost functions as the generation process goes on.)
% 
% 'method' should be an integer, and 'yourname' is the charater string of
% your name,like, 'Yifei'

% For stability, run 5 times for sizes 100, 500, 1000 and take the average.
% The function calls TestSpeed() and save all the .mat files that
% TestSpeed() generates with name 'SpeedTest_M%d_S%d_B%d_%s.mat' in the
% 'Results/' directory, where (%d, %d, %d, %s) corresponds to (method_id,
% size, batch number[1 to 5 for sizes 100, 500, 1000], yourname). An array
% of structure will be returned with each element being the return value of
% TestSpeed

addpath('../Source/NHANES/');
addpath('../Results/');

%%1. Run TestSpeed for different method

%N_PPs_seq = [3]; %This is for testing whether the code run through.
N_PPs_seq = [100]; %change here for different sizes of pps
N_size = numel(N_PPs_seq);
N_rep = 5;

switch method_id
    case 1
        lambda = 1e256; c2 = 1e256; n_region = 1e256; c_max = 1e256; %Useless parameters for method 1
        comment = 'Original method';
        
        for i = 1:N_size %Run for these 5 sizes of PPs
            sprintf('Generating size %d for the original method, no hurry', N_PPs_seq(i))
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time
                for j = 1:N_rep %Repeat 5 times for size 100, 500, 1000. 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);
            else
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);            
            end
        end
    case 2
        c2 = 1e256; n_region = 1e256; c_max = 1e256; %Useless parameters for method 2
        lambda = 0.001;  comment = 'Adding penalty for low density method'; % Useful parameters.

        for i = 1:N_size %Run for these 5 sizes of PPs
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time
                for j = 1:N_rep 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);
            else
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
            end
        end
    case 4
        n_region = 1e256; c_max = 1e256; %Useless parameters for method 4
        lambda = 1;  c2 = 100; comment = 'Ellipsoid method 1'; % Useful parameters.

        for i = 1:N_size %Run for these 5 sizes of PPs
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time
                for j = 1:N_rep 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);
            else
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
            end
        end
    case 5
        n_region = 1e256; c_max = 1e256; %Useless parameters for method 5
        lambda = 1;  c2 = 100; comment = 'Ellipsoid method 2'; % Useful parameters.

        for i = 1:N_size %Run for these 5 sizes of PPs
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time
                for j = 1:N_rep 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);                
            else           
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
            end
        end
    case 6
        lambda = 1e256; c2 = 1e256; n_region = 1e256; c_max = 1e256; %Useless parameters for method 6
        comment = 'PCA method';
        for i = 1:N_size %Run for these 5 sizes of PPs
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time
                for j = 1:N_rep 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);                
            else
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region, c_max, comment, result_name);
            end
        end
    case 7        
        c2 = 1e256; %Useless parameters
        lambda = 1; n_region_seq = [5, 5, 10, 15, 20];c_max = 100; comment= 'Stratified method';
        for i = 1:N_size %Run for these 5 sizes of PPs
            sprintf('Generating size %d for the stratify method, no hurry', N_PPs_seq(i))
            if i <= 3 %If it's in the first 3 sizes, repeat 5 time                
                for j = 1:N_rep 
                    result_name = sprintf('SpeedTest_M%d_S%d_B%d_%s', method_id, N_PPs_seq(i), j, yourname);
                    sm_temp(j) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region_seq(i), c_max, comment, result_name)
                end
                sm_seq(i).gof = mean([sm_temp.gof]);
                sm_seq(i).selection_efficiency = sum([sm_temp.n_pps]) / sum([sm_temp.n_vps]);
                sm_seq(i).time_per_pp = mean([sm_temp.time_per_pp]);
                sm_seq(i).time_per_vp = sum([sm_temp.cputime_pp]) / sum([sm_temp.n_vps]);
                sm_seq(i).cputime_pp = mean([sm_temp.cputime_pp]);
                sm_seq(i).cputime_total = mean([sm_temp.cputime_total]);
                sm_seq(i).n_vps = mean([sm_temp.n_vps]);
                sm_seq(i).n_pps = mean([sm_temp.n_pps]);                
            else
            result_name = sprintf('SpeedTest_M%d_S%d_%s', method_id, N_PPs_seq(i), yourname);
            sm_seq(i) = TestSpeed(N_PPs_seq(i), method_id, lambda, c2, n_region_seq(i), c_max, comment, result_name);
            end
        end    
end
result_plot_name = sprintf('../Results/SpeedTestPlot_M%d_%s',method_id, yourname);
save(result_plot_name, 'sm_seq');
end




        
        
        
        
                
        