%% RunScript_ModularGeneratePPs.m
% This shows part 1 of the re-scripting into a more modular format.
% Once the kinks have been worked out, I'll append the patient selection
% process and goodness-of-fit work, with augmentations like added KL
% divergence etc.

% Right now, the biggest "kink" happens in secion 9 of
% GeneratePPs_ModularOptAlg_ModularObjFunc in which the plausible patients
% that are located by SA produce an error when you try to re-run the
% biosimulate steady state calculations to retrieve the vector of the 9
% Steady State values.

%% I've written "Flag!!!" wherever there seems to be a problem - Any help/suggestions would be appreciated

%% Add Paths

% For brevity, I prefer to specify the root directory
Root_Directory = 'Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Modular Code' ;

addpath([ Root_Directory '\Results']);
addpath([ Root_Directory '\Source']);
addpath([ Root_Directory '\Source\NHANES']);
addpath([ Root_Directory '\Source\Modular Objective Functions']);
addpath([ Root_Directory '\Source\Modular Optimization Algorithms']);
addpath([ Root_Directory '\Source\Modular ObjFunc OptAlg Connection']);
addpath([ Root_Directory '\Source\Support Funcs - Generate PPs']) ;

%% Set Up the (two) Inputs
File_Save_String = 'ModularDemo' ; % This string isn't actually used yet bc the script now outputs instead of saves
NumPPs = 50 ; % Number of desired plausible patients

%% Run the file
% Outputs: Plaus Pat Params    9-Steady-State-Vals    ObjectiveFunct Score
[ Output_PPs_FinalParameters ,  Output_PPs_ModelSS ,  Output_PPs_ObjFunVals ] = GeneratePPs_ModularOptAlg_ModularObjFunc( File_Save_String , NumPPs ) ;
