
The goal of this folder is to revise function, such as GnerateVPs.m function, 
inorder to enhance clarity, functionality, and flexibity.

General ideas include:

A) Increasing the clarity as to which items are for the which task in the 
pipeline of generating a virtual population.
 ex. differentiating the choice in objective function and the choice in 
optimization/search algorithm


B) Sorting .m files according to which larger function they are supporting.
ex. Modular Code\Source\Support Funcs - Generate PPs

C) Adhere to the "one function, one task" principle
ex. RunOptim.m has been divided into its multitude of constituent tasks.

D) Minimize the deep heirrachy of the code schematic. (Related to C)
ex. Instead of GenerateVPs.m calling RunOptim, which calls sbiosimulate 
and ScoreModelSS, keep as many calls to the "root" script as possible.

E) Instead of passing a large number of variables through various functions, 
cache the key variables and call them locally.


To Ted, Richard, et al: I've placed a large amount of commenting in. Part of 
this is to provide land markers of where things were previously. Where 
possible, I have always opted to comment out code instead of removing it 
completely. I do this because I suspect it is easier to recall the items that 
are there, but commented out, rather than remember items that have been removed 
and therfore aren't seen. I'd encourage you to remove those things as clutter, 
as soon as you're happy that you understand the reason I made a given change. 



A Note on the directory:

The root directory for my coding is:
"Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Modular Code",
Which is secure system maintained by the Oxford Institute of Biomedical 
Engineering (IBME).




