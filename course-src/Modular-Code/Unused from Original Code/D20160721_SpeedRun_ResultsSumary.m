% D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , PlausPat_Selection_Case)
N_PPs_List =  [ 10 25 50 75 100 150 200 250 300 400 500 750 1000 2500 5000 7500];
SpeedTest_ResultsCells = cell( 3 , length(N_PPs_List) ) ;


for N_PPs =  N_PPs_List ;
    
    save('SpeedTest_CurrentN' , 'N_PPs') ;
    disp(datestr(now))
    
    clear( 'SpeedTest_Results_1' , 'SpeedTest_Results_2' , 'SpeedTest_Results_3' ) ;
    [SpeedTest_Results_1] = D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , 1 ) ;
    SpeedTest_ResultsCells{1, find(N_PPs ==  N_PPs_List) } = SpeedTest_Results_1;
    save('SpeedTest_ResultsCells' , 'SpeedTest_ResultsCells') ;
    disp(datestr(now))
    
    clear( 'SpeedTest_Results_1' , 'SpeedTest_Results_2' , 'SpeedTest_Results_3' ) ;
    [SpeedTest_Results_2] = D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , 2 ) ;
    SpeedTest_ResultsCells{2, find(N_PPs ==  N_PPs_List) } = SpeedTest_Results_2;
    save('SpeedTest_ResultsCells' , 'SpeedTest_ResultsCells') ;
    disp(datestr(now))
    
    clear( 'SpeedTest_Results_1' , 'SpeedTest_Results_2' , 'SpeedTest_Results_3' ) ;
    [SpeedTest_Results_3] = D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , 3 ) ;
    SpeedTest_ResultsCells{3, find(N_PPs ==  N_PPs_List) } = SpeedTest_Results_3;
    save('SpeedTest_ResultsCells' , 'SpeedTest_ResultsCells') ;
    disp(datestr(now))
    
end




%% Plot the Results
Method = 1 ;

SpeedTest_ResultsCells{ 1 , Method }.gof
SpeedTest_ResultsCells{ 1 , Method }.selection_efficiency
SpeedTest_ResultsCells{ 1 , Method }.time_per_pp
SpeedTest_ResultsCells{ 1 , Method }.time_per_vp

GOF    = nan(3, length(N_PPs_List) ) ;
SelEff = nan(3, length(N_PPs_List) ) ;
TimePP = nan(3, length(N_PPs_List) ) ;
TimeVP = nan(3, length(N_PPs_List) ) ;

for     Method = 1:3 ;
    for Time = 1:length(N_PPs_List)
        
        GOF(Method,Time) = SpeedTest_ResultsCells{ Method , Time }.gof ;
        SelEff(Method,Time) = SpeedTest_ResultsCells{ Method , Time }.selection_efficiency ;
        TimePP(Method,Time) = SpeedTest_ResultsCells{ Method , Time }.time_per_pp ;
        TimeVP(Method,Time) = SpeedTest_ResultsCells{ Method , Time }.time_per_vp ;
                
    end   
end

X_Limits = [1000 N_PPs_List(end) ] ;

figure
subplot(4,1,1)
hold on;
plot( N_PPs_List , GOF(1,:) , 'k')
plot( N_PPs_List , GOF(2,:) , 'r')
plot( N_PPs_List , GOF(3,:) , 'b')
ylabel('Goodness of Fit')
xlabel('# Plausible Patients Acquired')
legend('Orig SA' , 'Random Method' , 'Revised SA')
xlim(X_Limits)

subplot(4,1,2)
hold on;
plot( N_PPs_List , SelEff(1,:) , 'k')
plot( N_PPs_List , SelEff(2,:) , 'r')
plot( N_PPs_List , SelEff(3,:) , 'b')
ylabel('Selection Efficiency')
xlabel('# Plausible Patients Acquired')
legend('Orig SA' , 'Random Method' , 'Revised SA')
xlim(X_Limits)

subplot(4,1,3)
hold on;
plot( N_PPs_List , TimePP(1,:) , 'k')
plot( N_PPs_List , TimePP(2,:) , 'r')
plot( N_PPs_List , TimePP(3,:) , 'b')
ylabel('Time per PP')
xlabel('# Plausible Patients Acquired')
legend('Orig SA' , 'Random Method' , 'Revised SA')
xlim(X_Limits)

subplot(4,1,4)
hold on;
plot( N_PPs_List , TimeVP(1,:) , 'k')
plot( N_PPs_List , TimeVP(2,:) , 'r')
plot( N_PPs_List , TimeVP(3,:) , 'b')
ylabel('Time per VP')
xlabel('# Plausible Patients Acquired')
legend('Orig SA' , 'Random Method' , 'Revised SA')
xlim(X_Limits)




