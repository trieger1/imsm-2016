%% RunScript.m - generates figures from published paper
%   Generates a plausible patient cohort from the van de Pas Model, fits
%   the cohort against the NHANES data (with some transformation of model
%   outputs) and outputs key figures from the paper.
%
%   Step 1.  Generate Plausible Patients
%   Call GenerateVPs.m function which executes the generation of
%   'Plausible Patients'
%
%   Step 2. Analyze Plausible Patients
%
%   Extract the information to constrain against NHANES data set
%
%   Step 3. Select the virtual population based on the NHANES data distribution for LDLC, HDLC and TC
%   Note, the file will display iterations indicating convergence of the
%   method. The f(x) value printed is the average K-S test score between
%   univariate comparison with data. Anything around 0.1 is typically a
%   very good fit.
%
%   Increase these for anything other than a test run. Total number of
%   plausible patients will be simsperbatch*numofbatches. Aim for at least >
%   10,000 for this problem.

%% Setup
clear all; %#ok<CLSCR>
% addpath('Results');
% addpath('Source');
% addpath('Source/NHANES');

% Alternative - added by GWC
addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Results')
addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source')
addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source\NHANES')


% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0);
mu = m.mu;
sigma = m.Sigma;

% Set the number of plausible patients to generate:
simsperbatch = 10;
numofbatches = 1;

PlausPat_Selection_Case = 2 ;




%% Step 1. Generate Plausible Patients - These Get stored in "Results" of the current directory
% By default, uses parallel toolbox
Step1_Start = tic ;

% p = parpool;
% parfor i=1:numofbatches
rng(1)
for i=1:numofbatches
    batchname=strcat('My_Model_VPs',num2str(i));
    
    % Run Appropriate Method - (Added by GWC 2016/07/22)
    switch PlausPat_Selection_Case
        case 1
    GenerateVPs(batchname,simsperbatch);            
        case 2
    GeneratePPs_Random_D201607221305(batchname,simsperbatch);   
        case 3
    GeneratePPs_SA_Eps_D201607221930(batchname,simsperbatch);        
    end;

end

% delete(p);
toc(Step1_Start) ;


%% Step 2. Analyze Plausible Patients - This gets store in Results_All within the current directoy
% Model-specific steps:
% Simulate plausible patients to steady state:
Step2_Start = tic ;

[DYDT,FinalPts,Pfinal,Jobs]=get_and_distribute_VPs(numofbatches);
% Gather model outputs into outputs amenable for comparison to NHANES:
VPChar(:,1)=FinalPts(:,5)+FinalPts(:,6); %HDL
VPChar(:,2)=FinalPts(:,9); % LDL
VPChar(:,3)=FinalPts(:,7)+FinalPts(:,5)+FinalPts(:,6); %TC

% Unit conversions to match NHANES:
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;

toc(Step2_Start) ;

size( DYDT , 1)
%% Step 3. Select the virtual population based on the NHANES data distribution
[selection,~,~,~,ProbInclude,betaS]= prevalence_select_func(mu,sigma,log(VPChar));

% Example: generate new selection, and evaluate how good this selection is
% [gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,log(VPChar),1); % Previous
[gof,newselection]= OptimizeVPgeneration(  log10(betaS), ...                    % log10(betaS) added by Yifan
                                                                 ProbInclude, ...
                                                                 NormalizeVPs(log(VPChar),mu,sigma) , ...
                                                                 1 ) ;        % Line 64


% Save key variables:
matfile = fullfile('Results', 'tempWorkspace.mat');
save(matfile,'DYDT','FinalPts','Pfinal','Jobs',...
    'VPChar','mu','sigma','selection','ProbInclude','betaS',...
    'gof','newselection');

%% Step 4. plot Paper Data
% Note: clears work space and closes figures. If you want to plot the 
% latest data generated edit getVPsPlotFit.m file to load 'tempWorkspace'.
% Otherwise load paper data 'Results_All_Paper.mat'
getVPsPlotFits;
