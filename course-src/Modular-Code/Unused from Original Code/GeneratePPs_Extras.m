%% GeneratePPs_Extras



%% From Run Script
%% Setup
clear all; %#ok<CLSCR>
addpath('Results');
addpath('Source');
addpath('Source/NHANES');

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0);
mu = m.mu;
sigma = m.Sigma;

% Set the number of plausible patients to generate:
simsperbatch = 100;
numofbatches = 4;


i = 1
    batchname=strcat('My_Model_VPs',num2str(i));
 % My_Model_VPs1 ... My_Model_VPs8: Located in Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code\Results

  
%  Generic function: function GenerateVPs(My_model_save,NumVPs)   
%  From RunSrcipt:   GenerateVPs(batchname,simsperbatch);
% So use...
My_model_save = batchname  
NumVPs = simsperbatch



