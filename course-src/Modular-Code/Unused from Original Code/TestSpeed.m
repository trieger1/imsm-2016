function sm = TestSpeed(N_PPs)
% TestSpeed.m
%   Evaluates the performance of 'GenerateVPs', and prevelance_select_func and
%   generates a few key metrics as a output struct including:
%   
%   sm.gof - goodness-of-fit to the histograms
%   sm.selection_efficiency - how efficient was plausible patients --> VPs
%   sm.time_per_pp - how much time did it take to generate all of the PPs
%   sm.time_per_vp - time_per_pp * selection_efficiency
%

%% Matlab startup
addpath('Results');
addpath('Source');
addpath('Source/NHANES');

if nargin == 0
    N_PPs = 500; % Set the number of plausible patients to generate
end

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0); % fetch the NHANES data
mu = m.mu;                    % log-normal distribution parameters
sigma = m.Sigma;              % log-normal distribution parameters

%% Step 1. Generate plausible patient cohort:
%%% Start the timing clock
StartTime1=tic;
GenerateVPs('SpeedTest',N_PPs);
run_time1=toc(StartTime1); % Save finishing time 1 (create a run_time2 to compare against the base case)
load('Results/SpeedTest.mat'); % Loads the results of the GenerateVPs step

%% Step 2. Simulate the new plausible patients to steady state:
[~,OutPts] = SimulateVPs(VPmodel,pOut,'speed_test',0);

% Create NHANES outputs:
VPChar(:,1)=OutPts(:,5)+OutPts(:,6); %HDL
VPChar(:,2)=OutPts(:,9); % LDL
VPChar(:,3)=OutPts(:,7)+OutPts(:,5)+OutPts(:,6); %TC

% Unit conversion to match NHANES
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;

%% Step 3. Select the virtual population based on the NHANES data distribution
[selection, gof] = prevalence_select_func(mu,sigma,log(VPChar));

%% Step 4. Get key metrics on performance
%How well does your histograms match the data? (lower the better)
sm.gof = gof;
%How many plausible patients do you need to make 1 VP? (lower the better)
sm.selection_efficiency = numel(selection)/sum(selection);
%How long to make 1 plausible patient (shorter the better)
sm.time_per_pp = (run_time1)/N_PPs;
%How long to make 1 VP? (shorter the better)
sm.time_per_vp = sm.time_per_pp*sm.selection_efficiency;

%% Clean-up
save('Results/SpeedTest'); % Save the output before exiting

end