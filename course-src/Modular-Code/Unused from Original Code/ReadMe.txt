The file RunScript.m recapitulates the main results of the paper. The functions RunScript.m calls MATLAB implementations of the algorithm present in the paper and can be easily modified to work with different models. 

As it stands, this code assumes a SimBiology model, but it could easily be modified to run with other MATLAB code.

To run the algorithm 'as is' run RunScript.m. This generates a fixed number of VPs (simsperbatch*numofbatch) in parallel (assuming that the requisite toolboxes are available). 
GenerateVPs.m, which is called from 'RunScript', generates the 'plausible patients', and prevalence_select_func selects a virtual population that fits the prescribed data as well as possible.  

By default, only the parameter values of the generated patients are stored. The code get_and_distribute_VPs uses these parameter sets to calculate model outputs of interest (in this case steady states). 

There are some manipulations to these results which are then passed to prevalence_select_func, along with the multivariate normal distribution parameters for the empirical data (mu and sigma). 
This function returns the selection function as a logical index (1 if patient is in the virtual population, 0 otherwise).

Note that this is choice is made based on the calculated probabilities (See text), so repeated runs will lead to a slightly different selection).