function [ OptAlg_Options ] = SAOriginal_Options( ~ )

%% Options for the Original Optimization Alogrithm - This should be relocated ASAP 
% Options for optimization - note, problem specific! Could probably use
% local optimization to accomplish this same thing:
options2=saoptimset('ObjectiveLimit',0.005,'TolFun',1e-5,'Display','off',...
    'ReannealInterval',500,'InitialTemperature',0.5,'MaxIter',400,'TemperatureFcn',...
    @temperatureboltz,'AnnealingFcn', @annealingboltz,'AcceptanceFcn',@acceptancesa);

OptAlg_Options = options2 ;

end

