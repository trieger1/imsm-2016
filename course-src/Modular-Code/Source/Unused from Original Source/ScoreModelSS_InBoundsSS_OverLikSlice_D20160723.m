function [score] = ScoreModelSS_InBoundsSS_OverLikSlice_D20160723(DataSS,ModelSS,RangeSS,SteadyStateFlag, Min_MVN_Likelihood)

flg=SteadyStateFlag;

MidRange=(RangeSS(:,1)'+RangeSS(:,2)')/2;

%% Redefine for convenience, grab only the data we are interested in
DataSS=DataSS(flg);
ModelSS=ModelSS(flg);
RangeSS=RangeSS(flg);
MidRange=MidRange(flg);



%% With Respect to NHANES log-MVN
% Note: NHANES_SIGMA_PInv == NHANES_SIGMA_Inv @ [true,true,false;true,false,false;false,false,true]
NHANES_MU           = [3.94418598402665,4.64235150199378,5.19760658095589] ;
NHANES_SIGMA        = [0.0674056902674009,0.00657727631074845,0.0159582170284738;0.00657727631074845,0.113343416142512,0.0685203081287785;0.0159582170284738,0.0685203081287785,0.0495362723034005] ;
% NHANES_SIGMA_Inv    = [20.2723231272322,16.9229988007973,-29.9392577492654;16.9229988007973,67.9955395253635,-99.5055941453592;-29.9392577492654,-99.5055941453592,167.471849585296] ;
% NHANES_SIGMA_PInv   = [20.2723231272322,16.9229988007973,-29.9392577492653;16.9229988007973,67.9955395253633,-99.5055941453590;-29.9392577492653,-99.5055941453589,167.471849585296] ;

% HDL Part
HDL_LDL_TC = nan(1,3) ;
HDL_LDL_TC(1) = ModelSS(5)+ ModelSS(6);

% LDL Part
HDL_LDL_TC(2) = ModelSS(9);

% TC
HDL_LDL_TC(3) = ModelSS(7)+ ModelSS(5)+ ModelSS(6);

% Convert to NHANES Units
HDL_LDL_TC = HDL_LDL_TC/2.79  ;
HDL_LDL_TC = HDL_LDL_TC*38.66 ;

log_HDL_LDL_TC = log(HDL_LDL_TC) ;

NLML = mvnpdf( log_HDL_LDL_TC , NHANES_MU , NHANES_SIGMA );



%%%% SSE - SSE at points defined by range
score=(MidRange-ModelSS).^2 - (MidRange-RangeSS(:,1)').^2;
score=score./MidRange.^2;
score=sum(score(score>=0));

%% Add Likelihood below threshold
score = score + (-1)*min([ (NLML - Min_MVN_Likelihood)   , 0  ]) ;


end


LML_Thresholds = [ 1 2 3 5 7 9];
OffSets =  -2:.01:2 ;

figure
for Min_MVN_Likelihood = LML_Thresholds

    % Create Points
    Holder = nan( 2 , length(OffSets) ) ;    
    
    for OffSet = OffSets
    NLML = mvnpdf( NHANES_MU + OffSet , NHANES_MU , NHANES_SIGMA ) ;
    Holder( 1 , find(OffSet == OffSets) ) = NLML ;
    Holder( 2 , find(OffSet == OffSets) ) = (-1)*min([ (NLML - Min_MVN_Likelihood) 0]);
    end   ;

    subplot(2,3, find(Min_MVN_Likelihood == LML_Thresholds))

   hold on;
   plot( [-2 2] , [Min_MVN_Likelihood  Min_MVN_Likelihood] , '-r' , 'linewidth' , 3)
   plot( OffSets , Holder(1,:) , '-b' , 'linewidth' , 3)
   plot( OffSets , Holder(2,:) , '-k' , 'linewidth' , 3)
   legend( 'LML Bound' , 'p(Current PP)' , 'Objective Fun')
   
   title(['Slice at LML of ' num2str(Min_MVN_Likelihood) ])
   
    
end
