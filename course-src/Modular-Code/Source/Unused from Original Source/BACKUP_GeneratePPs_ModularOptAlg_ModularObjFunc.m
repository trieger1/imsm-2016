function GeneratePPs_ModularOptAlg_ModularObjFunc(My_model_save,NumVPs)

%% Created 2016/07/24 by GWC
% This function is a revision of the GnerateVPs.m function, with the goal to enhance clarity, functionality, and flexibity
% by reducing the number of calls to functions that subsequently make call to other functions 
% e.g. RunOptim, which subquently makes calls to:
% (1)	[t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state
% (2)	Temp = ScoreModelSS(x_baseline(end,:),x1(end,:),RangeSS,SteadyStateFlag);     % Score results (steady state [usual] case):
% which, respectively, 
% (1) evaluate the Steady State values, given parameter set p and a simbiology model, and 
% (2) calculate the Objective function to be optimized.
% 
% At a minimum, the desired functionality should feature direct computation of 
% (1) Steady state values of the simbiology give a parameter set, and
% (2) The objective function, given an desired pateint characteristics (e.g. parameters p, SS_values, and functions thereof.)
% 
% Since a critical disitrinction between candidate search algorithms is whether the Object function is altered between iterations,
% this will serve as something of a template, with suggested placement of critical elements depending on functionality.
% 
% Furthermore, since it is impossible to interpret the exact relationship between queries to the objective function and the optimization
% algorithm, upon initiation, the objective function and the optimization algorithm will not be included as input.
% Solutions to this shortcoming are more than welcome.
% Until then, descriptive addendums to the function are probably the way to go.
% 
% This template uses the original SA formulation.
% 
% Two folders were added to the Source directory:
% 'Modular Objective Functions'
% 'Modular Optimization Algorithms'

%% A Note on addpaths
% All necessary paths should be added in 
% 'TestSpeed.m'  which looks like: sm = TestSpeed(N_PPs)
% or its variants, e.g. 
% 'D20160722_TestSpeed_CompareVPSelectionMethods.m', which looks like: sm = D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , PlausPat_Selection_Case)
% To debug within function, use the commands:

% addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Results');
% addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source');
% addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source\NHANES');
% addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source\Modular Objective Functions');
% addpath('Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Code - D20160722\Source\Modular Optimization Algorithms');
% My_model_save = 'ModularDemo'
% NumVPs = 10

%% Sections 1-3 are unaltered from Generat_VPs.m, except for additional commenting

%% 1. Load model into matlab workspace
dirSave=strcat('Results/',My_model_save);
load('Source/My_Model.mat')

% Rename the model something sensible
VPmodel=van_de_pas_mod1;

% % Reseed the random number generator  % Removed for Reproducibility GWC
% rng('shuffle')

% Accelerate the model - need to setup mex compliler to do this:
sbioaccelerate(VPmodel);

% Simulate to generate baseline VP, grab names:
[tb,xb,names] = sbiosimulate(VPmodel);
% tb: 8x1 double w/ values 3.3 to 864k (for VPmodel=van_de_pas_mod1)
% xb: 8x9 double w/ each row of 9 elements identical (for VPmodel=van_de_pas_mod1)
% names 9x1 cell of biomodel state names, ex. intestine.free_cholesterol
% function sbiosimulate is not used elsewhere

%% 2. Load free parameters
% Do not include 'parameters' redefined by SimBiology in repeated
% assignments, or parameters which are known with high accuracy.

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);
% ConstantParameters: 22x1 cell of strings: k01,..,k21, LDL_nonHDL_ratio
% ConstantParameterValues: 22x1 w/ values .03-19

search_mag=5; % Search Magnitude for unknown/unspecificed parameters
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);
% The crucial 'hyperparameters' affecting plausible parameters and SteadyState Values
% SS_lower and SS_upper: 9x1 Bounds on Plausible Steady State Values, see print 'names' for list
% p_lower and p_upper: 22x1 Bounds on Plausible Parameter Values, see print 'ConstantParameters' for list

%% 3. The Integration Configuration
% For the biological ODE model
C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);

%%%% 3. Identify the outputs of the model which you are going to constrain
% These should match up with the order of SS_lower and SS_upper.

%%%%%%%%%%%%%%%%%% Find Indices to fit %%%%%%%%%%%%%
% The 9 Steady State Entities (one is commented out)
A=strcmp(names,'intestine.free_cholesterol');
FitIndices(1)=find(A==1);

A=strcmp(names,'intestine.cholesterol_ester');
FitIndices(2)=find(A==1);

A=strcmp(names,'liver.free_cholesterol');
FitIndices(3)=find(A==1);

A=strcmp(names,'liver.cholesterol_ester');
FitIndices(4)=find(A==1);

A=strcmp(names,'plasma.HDL_free_cholesterol');
FitIndices(5)=find(A==1);

A=strcmp(names,'plasma.HDL_cholesterol_ester');
FitIndices(6)=find(A==1);

A=strcmp(names,'plasma.non_HDL_cholesterol');
FitIndices(7)=find(A==1);

A=strcmp(names,'periphery.cholesterol');
FitIndices(8)=find(A==1);

%A=strcmp(names,'LDL_estimate_NHANES');
%FitIndices(9)=find(A==1);

%%%%%%%% Flag the states here that are being compared at steady-state

SteadyStateFlag=true(numel(names),1); % A 9x1 column of Logical 1's
%SteadyStateFlag(9)=false; % uncomment to match the LDL later

% Create a 9x2 double of plausible steady-state ranges, Looks like: 
% [0,3.072;0,0.5632;12.24,16.56;6.3 ,12.78 ;0.090675,2.81894625;0.272093023255814,8.45895348837209;1.662561,28.4022;22.032,112.752;0.64728,23.91309]
RangeSS=-1*ones(numel(names),2);

RangeSS(:,1)=SS_lower';
RangeSS(:,2)=SS_upper';

RangeSS(~SteadyStateFlag,:)=nan; % All are SS flags, so no change

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Add variant of initial guess
% This creates a tables of params: Index, names, value
NewP=addvariant(VPmodel,'NewP');

for i=1:numel(ConstantParameters) % same as i=1:22
    addcontent(NewP,{'parameter',ConstantParameters{i},'Value',ConstantParameterValues(i)})
end


%% Begining of substantial alterations
% This section was previously: 
% "%% 4. Prepare scoring function 'RunOptim' -
% Tell MATLAB I'm interested in 'p', tb and xb are the mean VP I'm
% fitting around."

%% 4. Prepare scoring function 'RunOptim' -
% Tell MATLAB I'm interested in 'p', tb and xb are the mean VP I'm
% fitting around.

f=@(p)RunOptim(p,ConstantParameters,VPmodel,tb,xb,C,FitIndices,RangeSS,SteadyStateFlag);

% Options for optimization - note, problem specific! Could probably use
% local optimization to accomplish this same thing:
options2=saoptimset('ObjectiveLimit',0.005,'TolFun',1e-5,'Display','off',...
    'ReannealInterval',500,'InitialTemperature',0.5,'MaxIter',400,'TemperatureFcn',...
    @temperatureboltz,'AnnealingFcn', @annealingboltz,'AcceptanceFcn',@acceptancesa);

% Predefine number of VPs you want and allocate storage arrays:
pOut=nan(numel(ConstantParameters),NumVPs);
Score_V= nan(NumVPs,1);

%% 5. Vary parameters to form initial guess and perform optimization:
k=1;
while k<=NumVPs
    % New Initial Range
    Initial=p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
    p_upperSA=log10(p_upper);
    p_lowerSA=log10(p_lower);
    InitialSA=log10(Initial);
    
    % Pass initial guess, scoring function, bounds, to optimization routine.
    try
        [p,score]= simulannealbnd(f,InitialSA,p_lowerSA,p_upperSA,options2);
        if score<eps
            pOut(:,k)=10.^p;
            Score_V(k)=score;
            k=k+1;
        end
    end
end

% sum( Score_V > 0 )

%% 6. Save outputs
%overwrite any changes to the model
load('Source/My_Model.mat')

save(dirSave,'pOut','VPmodel','Score_V')
end
