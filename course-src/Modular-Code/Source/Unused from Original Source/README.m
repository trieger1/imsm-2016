% The purspose of this folder is to avoid the clutter of extra scripts that
% get passed along, but never called.

% It is not advisable to add a path to this sub-directory. Instead, if one
% of these functions needs to be called, cut and paste the file into
% 'Modular Code/Source'.