%This script loads the data generated for the paper, or that generated from
%RunScript.m and makes figures similar to the main results of the paper.

% For calculating 95% error surfaces from 3D data, this script calls
% error_ellipse.m by AJ Johnson (2004). Available from the MATLAB File
% Exchange http://www.mathworks.com/matlabcentral/fileexchange/4705-error-ellipse
% see error_license.txt for the license for this file.

close all
clear all

load('Results/tempWorkspace.mat')
% this step pre-saved
%%%[DYDT,FinalPts,Pfinal,Jobs]=get_and_distribute_VPs();
%close all
VPChar(:,1)=FinalPts(:,5)+FinalPts(:,6); %HDL_c
VPChar(:,2)=FinalPts(:,9); % LDL_c
VPChar(:,3)=FinalPts(:,7)+FinalPts(:,5)+FinalPts(:,6); %TC
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;
fidelity=30;
BinN=85;
%[selection,~,~,~,ProbInclude,sf]= prevalence_select_func(mu,sigma,log(VPChar(:,:)));
%sf
NumVps=numel(VPChar(:,1));

figure

ProbInclude2=ProbInclude*betaS;
bins=-300:0.1:0.5;
bins2=-300:0.1:0.5;
temp=ProbInclude2>1;
ProbInclude2(temp)=1;

[n,x]=hist(log10(ProbInclude2),bins);
%set(gca,'XScale','log');
bar1=bar(x,n,'b','LineWidth',1,'BarWidth',1);
set(bar1,'FaceColor','none')
xlim([-20  2])
xlabel('log(Probabilty VP in Vpop)')
ylabel('frequency')
hold on
[n,x]=hist(log10(ProbInclude2(selection)),bins2);
bar2=bar(x,n,'r','LineWidth',1,'BarWidth',1);
set(bar2,'FaceColor','r','EdgeColor','k')

axes('parent',gcf,'position',[0.2 0.5 0.4 0.4]);
bar3=bar(x,n,'r','LineWidth',1,'BarWidth',1,'FaceColor','none');
set(bar3,'FaceColor','r','EdgeColor','k')
xlim([-3  1])




[normVPs] = NormalizeVPs(log(VPChar),mu,sigma);
gofbest=1;
for i=1:100
    
    [gof,newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,normVPs,1);
    
    if gof<gofbest
        gofbest=gof;
        selection=newselection;
    end
end

%set(gca,'XScale','log');

PSelect=Pfinal(:,selection);
w = std(PSelect');
Wmat=repmat(w,sum(selection),1);
medP=repmat(median(PSelect'),sum(selection),1);
Pnorm=(PSelect'-medP)./Wmat;
idx=kmeans(Pnorm,2,'Replicates',100);


figure
%silhouette(Pnorm,idx)
VP_final_all=VPChar(selection,:);
VP_final_cluster1=VP_final_all(idx==1,:);
VP_final_cluster2=VP_final_all(idx==2,:);






figure(7)
[hellipse]=error_ellipse(sigma,mu,'conf' ,0.95);

figure
subplot(2,3,1)
%HDL_c
OverlayBarPDF(log(VPChar(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')
figAlim=xlim;
subplot(2,3,2)
%LDL_c
OverlayBarPDF(log(VPChar(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')
figBlim=xlim;
subplot(2,3,3)
%TC
OverlayBarPDF(log(VPChar(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')
figClim=xlim;


xdatas = get(hellipse(3), 'XData');
ydatas = get(hellipse(3), 'YData');
zdatas = get(hellipse(3), 'ZData');
subplot(2,3,6)
%TC vs HDL_c
hold on

scatter(log(VPChar(1:fidelity:NumVps,3)),log(VPChar(1:fidelity:NumVps,1)),2,'r')
xlabel('log(TC)')
ylabel('log(HDL_c)')

plot(zdatas,xdatas,'k--','LineWidth',2)
xlim([figClim])
xdatas = get(hellipse(2), 'XData');
ydatas = get(hellipse(2), 'YData');
zdatas = get(hellipse(2), 'ZData');
subplot(2,3,5)
hold on
%TC vs LDL_c

scatter(log(VPChar(1:fidelity:NumVps,2)),log(VPChar(1:fidelity:NumVps,3)),2,'r')
xlabel('log(LDL_c)')
ylabel('log(TC)')
plot(ydatas,zdatas,'k--','LineWidth',2)
xlim([figBlim])
xdatas = get(hellipse(1), 'XData');
ydatas = get(hellipse(1), 'YData');
zdatas = get(hellipse(1), 'ZData');
subplot(2,3,4)
hold on
%HDL_c vs LDL_c

scatter(log(VPChar(1:fidelity:NumVps,1)),log(VPChar(1:fidelity:NumVps,2)),2,'r')

xlabel('log(HDL_c)')
ylabel('log(LDL_c)')

plot(xdatas,ydatas,'k--','LineWidth',2)
xlim([figAlim])



figure
subplot(2,3,1)
%HDL_c
OverlayBarPDF(log(VP_final_all(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')
figAlim=xlim;
subplot(2,3,2)
%LDL_c
OverlayBarPDF(log(VP_final_all(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')
figBlim=xlim;
subplot(2,3,3)
%TC
OverlayBarPDF(log(VP_final_all(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')
figClim=xlim;


xdatas = get(hellipse(3), 'XData');
ydatas = get(hellipse(3), 'YData');
zdatas = get(hellipse(3), 'ZData');
subplot(2,3,6)
%TC vs HDL_c
hold on

scatter(log(VP_final_all(:,3)),log(VP_final_all(:,1)),6,'r')
plot(zdatas,xdatas,'k--','LineWidth',2)
xlim([figClim])
xlabel('log(TC)')
ylabel('log(HDL_c)')


xdatas = get(hellipse(2), 'XData');
ydatas = get(hellipse(2), 'YData');
zdatas = get(hellipse(2), 'ZData');
subplot(2,3,5)
hold on
%TC vs LDL_c

scatter(log(VP_final_all(:,2)),log(VP_final_all(:,3)),6,'r')
plot(ydatas,zdatas,'k--','LineWidth',2)
xlim([figBlim])
xlabel('log(LDL_c)')
ylabel('log(TC)')

xdatas = get(hellipse(1), 'XData');
ydatas = get(hellipse(1), 'YData');
zdatas = get(hellipse(1), 'ZData');
subplot(2,3,4)
hold on
%HDL_c vs LDL_c

scatter(log(VP_final_all(:,1)),log(VP_final_all(:,2)),6,'r')
plot(xdatas,ydatas,'k--','LineWidth',2)
xlim([figAlim])
xlabel('log(HDL_c)')
ylabel('log(LDL_c)')

figure
subplot(2,3,1)
Overlay2BarPDF(log(VP_final_cluster1(:,1)),log(VP_final_cluster2(:,1)),mu(1),sigma(1,1)^0.5,BinN,'log(HDL_c)')

subplot(2,3,2)
Overlay2BarPDF(log(VP_final_cluster1(:,2)),log(VP_final_cluster2(:,2)),mu(2),sigma(2,2)^0.5,BinN,'log(LDL_c)')

subplot(2,3,3)
Overlay2BarPDF(log(VP_final_cluster1(:,3)),log(VP_final_cluster2(:,3)),mu(3),sigma(3,3)^0.5,BinN,'log(TC)')


xdatas = get(hellipse(3), 'XData');
ydatas = get(hellipse(3), 'YData');
zdatas = get(hellipse(3), 'ZData');
subplot(2,3,6)
%TC vs HDL_c
hold on

scatter(log(VP_final_cluster1(:,3)),log(VP_final_cluster1(:,1)),6,'b')
scatter(log(VP_final_cluster2(:,3)),log(VP_final_cluster2(:,1)),6,'r')
plot(zdatas,xdatas,'k--','LineWidth',2)
xlabel('log(TC)')
ylabel('log(HDL_c)')
xlim([figClim])

xdatas = get(hellipse(2), 'XData');
ydatas = get(hellipse(2), 'YData');
zdatas = get(hellipse(2), 'ZData');
subplot(2,3,5)
hold on
%TC vs LDL_c

scatter(log(VP_final_cluster1(:,2)),log(VP_final_cluster1(:,3)),6,'b')
scatter(log(VP_final_cluster2(:,2)),log(VP_final_cluster2(:,3)),6,'r')
plot(ydatas,zdatas,'k--','LineWidth',2)
xlabel('log(LDL_c)')
ylabel('log(TC)')
xlim([figBlim])

xdatas = get(hellipse(1), 'XData');
ydatas = get(hellipse(1), 'YData');
zdatas = get(hellipse(1), 'ZData');
subplot(2,3,4)
hold on
%HDL_c vs LDL_c

scatter(log(VP_final_cluster1(:,1)),log(VP_final_cluster1(:,2)),6,'b')
scatter(log(VP_final_cluster2(:,1)),log(VP_final_cluster2(:,2)),6,'r')
plot(xdatas,ydatas,'k--','LineWidth',2)
xlabel('log(HDL_c)')
ylabel('log(LDL_c)')
xlim([figAlim])
close 7


%%%%% Test Selection Variability




PSelect2=Pfinal;
w = std(PSelect2');
Wmat=repmat(w,numel(Pfinal(1,:)),1);
medP=repmat(median(PSelect2'),numel(Pfinal(1,:)),1);
Pnorm2=(PSelect2'-medP)./Wmat;

sumSelect=selection;
repeats=999;
P_centroid=zeros(22,repeats);
distP=zeros(1,repeats);
for i=1:repeats
    
    [gof(i),newselection]= OptimizeVPgeneration(log10(betaS),ProbInclude,normVPs,1);
    
    sumSelect=sumSelect+newselection;
    P_centroid(:,i)=mean(Pnorm2(newselection,:));
end

centroid_center=mean(P_centroid,2);

for i=1:repeats
    distP(i)= pdist([P_centroid(:,i) centroid_center]','chebychev');
end

%figure
%hist(sumSelect(sumSelect>0)/(repeats+1),100)