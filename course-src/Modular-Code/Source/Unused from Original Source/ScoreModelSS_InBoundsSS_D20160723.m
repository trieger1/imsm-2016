function [score] = ScoreModelSS_InBoundsSS_D20160723(DataSS,ModelSS,RangeSS,SteadyStateFlag)

flg=SteadyStateFlag;

MidRange=(RangeSS(:,1)'+RangeSS(:,2)')/2;

%% Redefine for convenience, grab only the data we are interested in
DataSS=DataSS(flg);
ModelSS=ModelSS(flg);
RangeSS=RangeSS(flg);
MidRange=MidRange(flg);

%%%% SSE - SSE at points defined by range
score=(MidRange-ModelSS).^2 - (MidRange-RangeSS(:,1)').^2;
score=score./MidRange.^2;
score=sum(score(score>=0));

end

