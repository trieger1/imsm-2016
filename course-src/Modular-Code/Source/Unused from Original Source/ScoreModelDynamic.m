function [score] = ScoreModelDynamic(DataTime,Data,ModelTime,Model,Range)

DataLength=numel(DataTime);

%%%% Where to compare the data (percentage total length)
Meas_points_per=[2 5 10 30 50 100];

%%%% Weight Measurement points
weights=[2 1 1 0.5 0.2 0.01];

weights=weights';

%%%% Index of where to sample simulation
SampleIndices=round(DataLength*Meas_points_per/100);

%%%% Interplate data at sample points
Model_sample=interp1(ModelTime,Model,DataTime(SampleIndices));

Data_sample=Data(SampleIndices);

%%%% SSE - SSE at points defined by range
score=(Data(SampleIndices)-Model_sample).^2 - (Data(SampleIndices).*(1-(1+Range))).^2;
score=score.*weights./Data_sample.^2;
score=sum(score(score>=0));


end

