function [ Output_PPs_FinalParameters ,  Output_PPs_ModelSS ,  Output_PPs_ObjFunVals ] = GeneratePPs_ModularOptAlg_ModularObjFunc(File_Save_String , NumPPs )

%% Created 2016/07/24 by GWC
% This function is a revision of the GnerateVPs.m function, with the goal to enhance clarity, functionality, and flexibity
% by reducing the number of calls to functions that subsequently make call to other functions 
% e.g. RunOptim, which subquently makes calls to:
% (1)	[t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state
% (2)	Temp = ScoreModelSS(x_baseline(end,:),x1(end,:),RangeSS,SteadyStateFlag);     % Score results (steady state [usual] case):
% which, respectively, 
% (1) evaluate the Steady State values, given parameter set p and a simbiology model, and 
% (2) calculate the Objective function to be optimized.
% 
% At a minimum, the desired functionality should feature direct computation of 
% (1) Steady state values of the simbiology give a parameter set, and
% (2) The objective function, given an desired pateint characteristics (e.g. parameters p, SS_values, and functions thereof.)
% 
% Since a critical disitrinction between candidate search algorithms is whether the Object function is altered between iterations,
% this will serve as something of a template, with suggested placement of critical elements depending on functionality.
% 
% Furthermore, since it is impossible to interpret the exact relationship between queries to the objective function and the optimization
% algorithm, upon initiation, the objective function and the optimization algorithm will not be included as input.
% Solutions to this shortcoming are more than welcome. Possible solution: a
% 'Combination_Script' that takes the OptAlg and ObjFunc as arguments.

% Until then, descriptive addendums to the function are probably the way to go.
% 
% This template uses the original SA formulation.
% 
% Two folders were added to the Source directory:
% 'Modular Objective Functions'
% 'Modular Optimization Algorithms'

%% Quasi-Modular
% It's a bit tough to do a complete abstract using 
%  Connection_File , Connection_Options , CostFun_File , CostFun_Options , OptAlg_File , OptAlg_Options
% all from the start as sexahcnageable parts. I will attempt to implment a
% quasimodular approach, in that it's just *very clear* how things are
% fitting together.

%% Inputs
% File_Save_String : What to call it. DOES NOT actually specify a model
% NumPPs : The Number of PPS to acquire
% Connection_File  : Instructions of how to connect the Opt Alg to the Cosst Function
% Connection_Options  % Any variable parameter for the connection file. May be [];
% CostFun_File  : The cost Function.   Takes inputs [ p , SS_Vals , Extras , CostFun_Options]
% CostFun_Options  : The options for CostFun_File
% OptAlg_File  : The opt algorithm,   Takes inputs [ p , SS_Vals , Extras , OptAlg_Options] or maybe just the cost function and options
% OptAlg_Options  : The options for OptAlg_File. Ex. The usual ones for the optimization tool box

%% Outputs
% Output_PPs_FinalParameters : A matrix (NumPPs x NumParams, e.g. 22) 
% Output_PPs_ModelSS: The steady state model values derived from each
% patient, with param values Output_PPs_FinalParameters.
%  that is, Model: f(Output_PPs_FinalParameters) ---> Output_PPs_ModelSS
% Output_PPs_ObjFunVals: The objective function, ostensibly a function of 
% {Output_PPs_FinalParameters Output_PPs_ModelSS}

%% A Note on addpaths
% All necessary paths should be added in 
% 'TestSpeed.m'  which looks like: sm = TestSpeed(N_PPs)
% or its variants, e.g. 
% 'D20160722_TestSpeed_CompareVPSelectionMethods.m', which looks like: sm = D20160722_TestSpeed_CompareVPSelectionMethods(N_PPs , PlausPat_Selection_Case)
% To debug within function, use the following commands:
% 
% Root_Directory = 'Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Modular Code' ;
% 
% addpath([ Root_Directory '\Results']);
% addpath([ Root_Directory '\Source']);
% addpath([ Root_Directory '\Source\NHANES']);
% addpath([ Root_Directory '\Source\Modular Objective Functions']);
% addpath([ Root_Directory '\Source\Modular Optimization Algorithms']);
% addpath([ Root_Directory '\Source\Modular ObjFunc OptAlg Connection']);
% addpath([ Root_Directory '\Source\Support Funcs - Generate PPs']) ;
% 
% File_Save_String = 'ModularDemo'
% NumPPs = 10

%% Sections 1-3 
% Largely unaltered from Generate_VPs, but have been moved into a function
% called "Generate_Parameters_BiosimulateModel( Biosim_Model_Name )"
% which (1) contains these design choices, and (2) caches the key
% parameters for easy local retrieval for the myriad of involved functions.

 % This is the name of a .mat file, in "Source", which is ammenable to the BioSim/ODE work
BioSimModelName_String = 'My_Model.mat' ;

% Here are the parameters concerning this model. For full documentation of
% what they mean, see 'Generate_Parameters_BiosimulateModel'
% The function also saves them, so that they can be called locally in other
% functions.
[ VPmodel , tb , xb , names , ConstantParameters , ConstantParameterValues , search_mag , p_lower , p_upper , ...
            SS_lower , SS_upper , RangeSS_MidPoints , C , FitIndices , SteadyStateFlag , RangeSS , NewP ] = ...
            Generate_Parameters_BiosimulateModel( BioSimModelName_String ) ; 

%% ***Current Code Problem (Ted, Richard, any ideas?)***
% The function 'Generate_Parameters_BiosimulateModel' also saves all of the
% variables it outputs. The goal is to cache with so it can be
% inexpensively called locally in many functions. That is, I would like:
% [ VPmodel , tb , xb , names , ConstantParameters , ConstantParameterValues , search_mag , p_lower , p_upper , ...
%            SS_lower , SS_upper , RangeSS_MidPoints , C , FitIndices , SteadyStateFlag , RangeSS , NewP ]
% , from the lines above, to be save as
% [ BioSimModelName_String '_KEYPARAMS.mat' ]
% so that for functions like 'ScoreModelSS.m', I only need to use
% 'load('KEYPARAMS.mat') and have all of that information locally
% available.
% This will save on the use of the cluttered varaible inputs.
% Unfortunately, for functions like 'Compute_SteadyState', this creates an error when
% 'sbiosimulate' is used. ( Compute_SteadyState still works when the variables are direcly passed to it.
% For now, I'll just store these all within a signle entity, and pass it to
% functions. This will look cleaner, at least, than passing many functions.
            
%  Key_Param_Cache = load('My_Model_KEYPARAMS.mat') ;  % This should be equivalent to the code below     
              
 Key_Param_Cache = [];    % Until the Load '.mat' cache works, put these into a structure and pass around   
        
  Key_Param_Cache.VPmodel                   = VPmodel ;
  Key_Param_Cache.tb                        = tb ;
  Key_Param_Cache.xb                        = xb ;
  Key_Param_Cache.names                     = names ;
  Key_Param_Cache.ConstantParameters        = ConstantParameters ;
  Key_Param_Cache.ConstantParameterValues   = ConstantParameterValues ;
  Key_Param_Cache.search_mag                = search_mag ;
  Key_Param_Cache.p_lower                   = p_lower ;
  Key_Param_Cache.p_upper                   = p_upper ;
  Key_Param_Cache.SS_lower                  = SS_lower ;
  Key_Param_Cache.SS_upper                  = SS_upper ;
  Key_Param_Cache.RangeSS_MidPoints         = RangeSS_MidPoints ;
  Key_Param_Cache.C                         = C ;
  Key_Param_Cache.FitIndices                = FitIndices ;
  Key_Param_Cache.SteadyStateFlag           = SteadyStateFlag ;
  Key_Param_Cache.RangeSS                   = RangeSS ;
  Key_Param_Cache.NewP                      = NewP ;
 
  % Making two extras to see if it gets altered without my seeing it in Section 9
  Extra_Key_Param_Cache_1 = Key_Param_Cache ;
  Extra_Key_Param_Cache_2 = Key_Param_Cache ;
  
%% Begining of substantial alterations
% This section was previously: 
% "%% 4. Prepare scoring function 'RunOptim' -
% Tell MATLAB I'm interested in 'p', tb and xb are the mean VP I'm
% fitting around."

%% 4. Prepare Steady State sbiosimulate function
% Anon Function Input: p  AND (ConstantParameters,VPmodel,C)) are pre-set.
% Anon Function Output: The Steady State Values
% How: Compute_SteadyState contains the 'sbiosimulate' function and other
% preparatory parameters.
% Previously: % f=@(p)RunOptim(p,ConstantParameters,VPmodel,tb,xb,C,FitIndices,RangeSS,SteadyStateFlag); 

f_SteadyStateFromP = @(p)Compute_SteadyState(p,Key_Param_Cache);

%% 4.1 Example: Uncomment 1 time and run.
%% FLAG!!!! Ted and Richard: Values in 'ModelSS_2' are negative. Does this mean the model has been run incorrectly?
% rng(12) ; Example_P = p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
% [t_SSOutput , x_SSOutput] = f_SteadyStateFromP(Example_P) ; % This is [t1,x1] = sbiosimulate(VPmodel,C,NewP2);  ...the values of 'p' are in 'NewP2'
% % The constituent inputs of ScoreModelSS:
% % ScoreModelSS(DataSS,ModelSS,RangeSS,SteadyStateFlag)
% DataSS_2 = xb(end,:) ; % Data SS isn't determined by any results in Compute_SteadyState
% ModelSS_2 = Compute_ModelSS_from_SteadyState(t_SSOutput , x_SSOutput) % The SS values were're actually interested in.
% % Already have: [ RangeSS, SteadyStateFlag ]
% % This should output values instead of NaNs (which indicates that biosimulate failed)

%% 5. Specify the Objective Function (If using a non-adaptive objective function)

% 'InsideSSBounds_ObjFunc' is the "Modular" Objective function file
% Create an Anon Function where only 'p' is varied
Objective_Funct = @(p) InsideSSBounds_ObjFunc( exp(p) , Key_Param_Cache) ; % Put in as Expon. bc of SA


%% 6. Specify the Optimization Algorithm

% Include Options in separate file
OptimAlg_Options = SAOriginal_Options() ;

% This is the function: [x, fval, exitflag, output] = SAOriginal_OptAlg(FUN, x0, lb, ub, options)
%% Flag!!! (Lower Priority) Ted and Richard:
% For some reason, the following works in Section 9:
% OptimAlg_Function =  @( FUN , Start_Pt ) simulannealbnd( FUN , Start_Pt, ...
%                                                             log(Key_Param_Cache.p_lower), log(Key_Param_Cache.p_upper), OptimAlg_Options) ;
% But this does *not* work in Section 9
% OptimAlg_Function =  @( FUN , Start_Pt ) SAOriginal_OptAlg( FUN , Start_Pt, ...
%                                                             log(Key_Param_Cache.p_lower), log(Key_Param_Cache.p_upper), OptimAlg_Options) ;
% Any ideas?

OptimAlg_Function =  @( FUN , Start_Pt ) simulannealbnd( FUN , Start_Pt, ...
                                                            log(Key_Param_Cache.p_lower), log(Key_Param_Cache.p_upper), OptimAlg_Options) ;

%% 7. Specify the connecting Instructions
%% A modular aspect for later
% Ostensibly this will be given function handles as inputs, and return as outputs:
% [ Output_PPs_FinalParameters , Output_PPs_ModelSS , Output_PPs_ObjFunVals ]
% It would then be run in Section 9...or something like that....

%% 8. Define Outputs of Interest - These will be saved
% Predefine number of VPs you want and allocate storage arrays:
Output_PPs_FinalParameters= ( nan(numel(ConstantParameters),NumPPs) )'; % One row per patient, one column per parameter (this is transfposed from the previous version.)
Output_PPs_ModelSS = ( zeros(numel(SteadyStateFlag),NumPPs) )'; % One row per patient, one column per Steady State value in ODE
Output_PPs_ObjFunVals = nan(NumPPs,1);

%% 9. Vary parameters to form initial guess and perform optimization:
Count=1;
Error_Flag = 0; % In case something goes wrong, don't need to guess where it happened

while Count<=NumPPs
    % New Initial Range
    Initial_Param =p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
    Log_Initial_Param = log(Initial_Param);
%     p_upperSA=log10(p_upper); % Removed since it's now a param in OptimAlg_Function
%     p_lowerSA=log10(p_lower); % Removed since it's now a param in OptimAlg_Function
    
    % Pass initial guess, scoring function, bounds, to optimization routine.
    try       
        Error_Flag = 1;
            [p,score]= OptimAlg_Function( Objective_Funct , Log_Initial_Param ) ;
        if score<eps
            
            % Store Plausible Patient Params
        Error_Flag = 2;
            PP_Params = exp(p) ; % Back into the original units
            Output_PPs_FinalParameters(Count,:)= PP_Params ;
            
            % Store Plausible Patient Steady State Values
            %% Flag!!! Ted and Richard: Sometimes this does not work
            % I can't tell if it's because 'Key_Param_Cache' or something
            % gets alters thought me knowing by biosimulate. I've tested
            % this by saving two spare copies and refreshing them each
            % iteration. Any ideas?
            % Note that 'Compute_SteadyState' outputs 'NaN's if biosimulate
            % fails
        Error_Flag = 3;
%             [ t_PP , x_PP] = Compute_SteadyState( PP_Params ,Key_Param_Cache) ;
            [ t_PP , x_PP] = Compute_SteadyState( PP_Params ,Extra_Key_Param_Cache_1) ;
            ModelSS_PP      = Compute_ModelSS_from_SteadyState(t_PP , x_PP) ;% The SS values were're actually interested in.                       
            Output_PPs_ModelSS(Count,:)= ModelSS_PP ; 
            
            % Store Plausible Patient Score
        Error_Flag = 4;
            Output_PPs_ObjFunVals(Count) =  score ;
            
            % Reset the cache in case that is being changed btwn iterations
            Extra_Key_Param_Cache_1 = Extra_Key_Param_Cache_2 ;
            
            Count=Count+1;
        end
        
    catch
        disp(['Count' 'Error_Flag'])
        disp([Count Error_Flag])
    end
end


%% 6. Save outputs
% % There's too much saving stuff going on. Leaving this in, in case it's
% % helpful for running in parallel.

% %overwrite any changes to the model
% load('Source/My_Model.mat')
% 
% % save(File_Save_String,'Output_PPs_FinalParameters', 'Output_PPs_ModelSS' ,'Output_PPs_ObjFunVals' , 'VPmodel')

end