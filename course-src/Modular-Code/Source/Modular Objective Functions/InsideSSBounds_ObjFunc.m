function [ Objective_Value ] = InsideSSBounds_ObjFunc(p , Key_Param_Cache)

%% This is the original objective function used with the original SA search for plausible patients.
% The cost function returns "Objective_Value = 0" when the Model_SS is within the
% Steady State Bounds and "Objective_Value > 0" otherwise.

%% Set-up
% This is where I would like to load the biosim variables locally, instead
% of passing them as inputs. It's less clutter.

% Key_Param_Cache = load('My_Model_KEYPARAMS.mat') ; % I'd like to just load it like this
% or this
% Key_Param_Cache = load('Example_Key_Param_Cache.mat' ) ;

%% Part 1: List Parameters of Interest (for clarity)
RangeSS     =   Key_Param_Cache.RangeSS ;

%% Part 2: Take input p, and return Steady State values

    [t1_SSOutput , x1_SSOutput] = Compute_SteadyState(p,Key_Param_Cache) ;  % Two steady state outputs, only 'x1_SSOutput' matters
    ModelSS = Compute_ModelSS_from_SteadyState(t1_SSOutput , x1_SSOutput);  % The SS values were're actually interested in.

%% Part 3: Calculate the Objective Function
% The idea here is that, if the researchers wants to do nothing with the
% model (and learn nothing about the model), except try some new objective 
% function, the only thing they would need to do is edit the lines here.

% This should look VERY familiar....

MidRange=(RangeSS(:,1)'+RangeSS(:,2)')/2;

%%%% SSE - SSE at points defined by range
Distance_from_Bounds    = (MidRange-ModelSS).^2 - (MidRange-RangeSS(:,1)').^2 ;
Distance_from_Bounds    = Distance_from_Bounds./MidRange.^2 ;

Objective_Value                   = sum(Distance_from_Bounds(Distance_from_Bounds>=0)) ;



%% Examples: For Experimentation Inside Function
% % Example Non-Plausible Patient
% p_lower = Key_Param_Cache.p_lower ;
% p_upper = Key_Param_Cache.p_upper ;
% 
% rng(12) ; Example_P_NonPlausible = p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
%     [t1_SSOutput_NonPlausible , x1_SSOutput_NonPlausible] = Compute_SteadyState(Example_P_NonPlausible,Key_Param_Cache) ;  % Two steady state outputs, only 'x1_SSOutput' matters
%     ModelSS_NonPlausible = Compute_ModelSS_from_SteadyState(t1_SSOutput_NonPlausible , x1_SSOutput_NonPlausible);  % The SS values were're actually interested in.

end

