function [ Model_SS ] = Compute_ModelSS_from_SteadyState( t1,x1)

%% For clarity:
%  This function takes the output values (verbatim) from
%  Compute_SteadyState and returns the Model_SS, which is generally what we
%  think of as the Steady State values, given teh input paramters.

% In this case, Model_SS is just the bottom line of row of x1.
% t1 is unused, but included as an input to avoid confusion about which one
% to use. Admittedly, t1 is an unlikely candidate for confusion, since it
% is a column vector.

Model_SS = x1(end,:) ;

end

