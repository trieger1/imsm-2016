function [ VPmodel , tb , xb , names , ConstantParameters , ConstantParameterValues , search_mag , p_lower , p_upper , ...
            SS_lower , SS_upper , RangeSS_MidPoints , C , FitIndices , SteadyStateFlag , RangeSS , NewP ] = Generate_Parameters_BiosimulateModel( Biosim_Model_Name )

%% What this function is
% The purpose of this file is to provide the commands that generate key
% parameters of a given model. The generation of these key parameters is a
% **design choice** that may change with insight into the biological
% system. This is a function that engineers, lab scientists, clinicians,
% etc. can sit around and have a chat about...decide which bits are
% reasonable and which aren't. It is the quantitative-to-subject-matter
% interface.

%% What this function is not
% It is (probably) not a good cache for the derived parameters once the
% design choices has been made. Those should be saved and stored separately
% for inexpensive access.
% It may also be that this is a poor format to handle key parameters from
% across models. In which case, clarity might be best served by creating
% multiple, aptly-named functions.

%% Why?
% There are crucial parameters, e.g. the free parameters and the steady
% state parameters which are crucial to our reasoning about the model and
% many of our decisions about the objective function and the
% search/optimization algorithms of interest. At the same time, it would be
% better to just pull them out as/when necessary because:
% 1) It creates clutter in the scripts to generate PPs/VPs
% 2) The derivation of these parameters is a separate design decision, and
% one that may not interest to those fiddling with the PP/VP search
% 3) If the paramters of the "end result" can be grabbed directly, then we 
% don't need to pass them all as inputs whenever we wish to use them in
% other functions....so it unclutters the input to these functions, so the
% variable of interest is clear.

% Potential downsides: It takes a few seconds to generate these values from
% scratch....so probably shouldn't use this as a cache if calls to this
% function are being timed for computational efficiency.
% Slow bits: sbioaccelerate(VPmodel); % This takes a few seconds to execute.
% 

%% 1. Load model into matlab workspace
% dirSave=strcat('Results/',File_Save_String); % No longer used
load( Biosim_Model_Name ) ; % This is the input var. File is in Source.

% Rename the model something sensible. 'van_de_pas_mod1' came from load('')
VPmodel=van_de_pas_mod1;

% % Reseed the random number generator  % Removed for Reproducibility GWC
% % If needed, should probably be placed elsewhere.
% rng('shuffle')

% Accelerate the model - need to setup mex compliler to do this:
sbioaccelerate(VPmodel); % This takes a few seconds to execute.

% Simulate to generate baseline VP, grab names:
[tb,xb,names] = sbiosimulate(VPmodel);
% Short for t_baseline and x_baseline (in Compute_SteadyState.m and RunOptim.m)
% tb: 8x1 double w/ values 3.3 to 864k (for VPmodel=van_de_pas_mod1)
% xb: 8x9 double w/ each row of 9 elements identical (for VPmodel=van_de_pas_mod1)
% names 9x1 cell of biomodel state names, ex. intestine.free_cholesterol
% function sbiosimulate is not used elsewhere

%% 2. Load free parameters
% Do not include 'parameters' redefined by SimBiology in repeated
% assignments, or parameters which are known with high accuracy.

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);
% ConstantParameters: 22x1 cell of strings: k01,..,k21, LDL_nonHDL_ratio
% ConstantParameterValues: 22x1 w/ values .03-19

search_mag=5; % Search Magnitude for unknown/unspecificed parameters
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);
% The crucial 'hyperparameters' affecting plausible parameters and SteadyState Values
% SS_lower and SS_upper: 9x1 Bounds on Plausible Steady State Values, see print 'names' for list
% p_lower and p_upper: 22x1 Bounds on Plausible Parameter Values, see print 'ConstantParameters' for list

%% 3. The Integration Configuration
% For the biological ODE model
C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);

%%%% 3. Identify the outputs of the model which you are going to constrain
% These should match up with the order of SS_lower and SS_upper.

%%%%%%%%%%%%%%%%%% Find Indices to fit %%%%%%%%%%%%%
% The 9 Steady State Entities (one is commented out)
A=strcmp(names,'intestine.free_cholesterol');
FitIndices(1)=find(A==1);

A=strcmp(names,'intestine.cholesterol_ester');
FitIndices(2)=find(A==1);

A=strcmp(names,'liver.free_cholesterol');
FitIndices(3)=find(A==1);

A=strcmp(names,'liver.cholesterol_ester');
FitIndices(4)=find(A==1);

A=strcmp(names,'plasma.HDL_free_cholesterol');
FitIndices(5)=find(A==1);

A=strcmp(names,'plasma.HDL_cholesterol_ester');
FitIndices(6)=find(A==1);

A=strcmp(names,'plasma.non_HDL_cholesterol');
FitIndices(7)=find(A==1);

A=strcmp(names,'periphery.cholesterol');
FitIndices(8)=find(A==1);

%A=strcmp(names,'LDL_estimate_NHANES');
%FitIndices(9)=find(A==1);

%%%%%%%% Flag the states here that are being compared at steady-state

SteadyStateFlag=true(numel(names),1); % A 9x1 column of Logical 1's
%SteadyStateFlag(9)=false; % uncomment to match the LDL later

% Create a 9x2 double of plausible steady-state ranges, Looks like: 
% [0,3.072;0,0.5632;12.24,16.56;6.3 ,12.78 ;0.090675,2.81894625;0.272093023255814,8.45895348837209;1.662561,28.4022;22.032,112.752;0.64728,23.91309]
RangeSS=-1*ones(numel(names),2);

RangeSS(:,1)=SS_lower';
RangeSS(:,2)=SS_upper';

RangeSS(~SteadyStateFlag,:)=nan; % All are SS flags, so no change

RangeSS_MidPoints = (RangeSS(:,1)'+RangeSS(:,2)')/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Add variant of initial guess
% This creates a tables of params: Index, names, value
NewP=addvariant(VPmodel,'NewP');

for i=1:numel(ConstantParameters) % same as i=1:22
    addcontent(NewP,{'parameter',ConstantParameters{i},'Value',ConstantParameterValues(i)})
end

save(   [ 'Z:\netshares\bspprojects13\GWC_GPs\Other Stuff\VP\Modular Code\Source\' Biosim_Model_Name(1:(end-4)) '_KEYPARAMS' ] , ...
         'VPmodel' , 'tb' , 'xb' , 'names' , 'ConstantParameters' , 'ConstantParameterValues' , 'search_mag' , 'p_lower' , 'p_upper' , ...
            'SS_lower' , 'SS_upper' , 'RangeSS_MidPoints' , 'C' , 'FitIndices' , 'SteadyStateFlag' , 'RangeSS' , 'NewP' ) ;


        %% there some discrepency btwn what's being saved (incorrect) and what's being outputted (correct)
% [ VPmodel , tb , xb , names , ConstantParameters , ConstantParameterValues , search_mag , p_lower , p_upper , ...
%             SS_lower , SS_upper , RangeSS_MidPoints , C , FitIndices , SteadyStateFlag , RangeSS , NewP ] 
        
        
          
        
        
end

 
        
