function [DepMin,DepMout,names,ord,m1] = FixDep(m1)
%Reorder dependencies in model for backward compatibility with MATLAB
%versions prior to 2013
A=m1.Rules;

N=numel(A);

DepM=zeros(N);


clear k
for i=1:N
    B=A(i);
    C{i,1}=B.rule;
    
    Vrb{i,1}=strtok(B.rule,' = ');
    
end




for i=1:N
    
    temp = strfind(C, Vrb{i,1});
    
    Dupe=strcat(Vrb{i,1},'_');
    
    temp2= strfind(C, Dupe);
    
    %if temp ==temp2
    %k(:,i)=[];
    %else
    %k(:,i)=temp;
    %end
    
    k(:,i)=temp;
end
for i=1:N
    
    k{i,i}=[];
    
end

for i=1:N
    for j=1:N
        
        if isempty(k{i,j})==1
            DepM(i,j)=0;
        else
            DepM(i,j)=1;
        end
        
    end
end
%order=graphtopoorder(sparse(DepM'));

DepMin=DepM';



DepMout=DepMin;

ord=graphtopoorder(sparse(DepMout));
DepMout=DepMin(ord,ord);


names=Vrb(ord);

reorder(m1, m1.Rules(ord));
end