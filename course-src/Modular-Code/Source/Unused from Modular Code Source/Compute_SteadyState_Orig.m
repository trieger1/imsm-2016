function [ t1,x1] = Compute_SteadyState(p,ConstantParameters,VPmodel,C)
% Altered verion of 'RunOptim.m' to extricate the calculation of Steady
% State values from the computation of a Score/Objective Function.
% Inputs:
% p : parameters, **p WAS passed as log10(p)** - This seems unnecessary now that the the CostFunctionEval is separate. 
% ConstantParameters : A list of the parameter names. Same as ConstantParameters in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc', ex. the 22x1 cell of name strings. 
% VPmodel : The sbiosimulate model. Same as VPmodel in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc', ex. van_de_Pas_Baseline_LDL_scalar_estimate
% t_baseline : This is 'tb' in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc', ex. The 8x1 double 
% x_baseline : This is 'xb' in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc', ex. The 8x9 double 
% C : The intergration *C*onfiguration settings.  Same as C in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc'
% FitIndices : An indication of the Steady State indies to fit.  Same as FitIndices in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc'. Ex: 1:8 in van_de_Pas
% RangeSS : Upper and lower bounds of plausible steady state values. Same as RangeSS in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc'. Ex: 9x2 double
% SteadyStateFlag : A Logical column of Steady State indices. Same as SteadyStateFlag in 'GenerateVPs' and 'GeneratePPs_ModularOptAlg_ModularObjFunc'. Ex: 9x1 logical of all 1's

% Previous documentation:
% RunOptim(p,ConstantParameters,VPmodel,t_baseline,x_baseline,C,FitIndices,RangeSS,SteadyStateFlag)
%   Takes in an initial parameter guess for a plausible patient, adds those
%   guesses to the SimBiology model as variants and simulates to steady
%   state.
%  The origianl function Inputs/Outputs were:
% [ ScoreFinal,t1,x1] = RunOptim(p,ConstantParameters,VPmodel,t_baseline,x_baseline,C,FitIndices,RangeSS,SteadyStateFlag)
% Inputs removed : t_baseline,x_baseline, FitIndices,RangeSS,SteadyStateFlag
%% Initial Setup:
% ScoreFinal = 0; % Initialize the final score        % Removed by GWC
% p=10.^p;        % Parameters are passed as log10(p) % Removed by GWC

% Sets an option for dynamic fitting, not used for steady state fitting
% [usual case]
% Acceptable Range for dynamic fitting- this defines
% how close the virtual patients need to be to the mean for each species
% variable
Range_fold=[1 1 1 1 1 1 1 1 1];

%% 1. Create New Variant
% This step adds a "variant" to the simbiology model, that is an
% alternative parameterization of the model, which is simulated below.
NewP2 = addvariant(VPmodel,'NewP2');
% Writes the proposed parameters to the new variant:
for i=1:numel(p)
    addcontent(NewP2,{'parameter',ConstantParameters{i},'Value',p(i)})
end

%% 2. Simulate and score results
% Use try, but with no catch - may fail due to integration issues. 
% If integration fails, assume something bad has happened and we don't
% want that VP (add a very high score)
try
    [t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state   
%      Removed Section 1 was here   
catch
%      Removed Section 2 was here

    % New Catch Statement: Return NaNs if  sbiosimulate fails
    t1 = nan(1,1) ;
    x1 = nan(1,1) ;
end

%% 3. Clean-up (delete variants)
% Delete the last variant that was added to the model - need to do this
% to keep things organized
delete(VPmodel.variant(numel(VPmodel.variant)))

%% Removed Sections for Reference

%% Removed Section 1
% %     Removing Scoring Evaluation Element: (The ObjFun here only calculates in SS_Values fall in the plausible range
%     % This code should be inactive, but leaving it here just-in-case:
%     for i = 1:numel(FitIndices)
%         if SteadyStateFlag==0
%             Temp = ScoreModel(t_baseline(:),x_baseline(:,FitIndices(i)),t1(:),x1(:,FitIndices(i)),Range_fold(i));
%             ScoreFinal=ScoreFinal+Temp;
%         end
%     end
%     
%     % Score results (steady state [usual] case):
%     if sum(SteadyStateFlag>0)
%         Temp = ScoreModelSS(x_baseline(end,:),x1(end,:),RangeSS,SteadyStateFlag);
%         ScoreFinal = ScoreFinal + Temp;
%     end 

%% Removed Section 2
%     ScoreFinal = 1e256; % If the simulation terminated unsuccessfully, just assign a very large value


end

