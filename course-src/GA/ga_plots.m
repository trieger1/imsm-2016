
close all;
clear all;

%% generation plots

load('Results/SpeedTest.mat');

nPP = n_plausible(end);
PP = zeros(nPP,3);
FinalPts = OutPts;

PP(:,1) = FinalPts(:,5)+FinalPts(:,6); %HDL_c
PP(:,2)=FinalPts(:,9); % LDL_c
PP(:,3)=FinalPts(:,7)+FinalPts(:,5)+FinalPts(:,6); %TC
PP=PP/2.79;
PP=PP*38.66;

figure(7)
[hellipse]=error_ellipse(sigma,mu,'conf' ,0.95);

figure;
gif_name = 'generations.gif';

n_start = 1;
for i = 1:length(n_plausible)-1
    n_end = n_plausible(i);
    
    % TC vs HDL_c
    xdatas = get(hellipse(3), 'XData');
    ydatas = get(hellipse(3), 'YData');
    zdatas = get(hellipse(3), 'ZData');
    
    subplot(1,3,1);
    
    hold on
    scatter(log(PP(n_start:n_end,3)),log(PP(n_start:n_end,1)),3);
    
    plot(zdatas,xdatas,'k--','LineWidth',2)
    
    xlabel('log(TC)')
    ylabel('log(HDL_c)')
    
    %axis equal
    xlim([log(min(PP(:,3))),log(max(PP(:,3)))]);
    ylim([log(min(PP(:,1))),log(max(PP(:,1)))]);
    
    
    
    %LDL_c vs TC
    xdatas = get(hellipse(2), 'XData');
    ydatas = get(hellipse(2), 'YData');
    zdatas = get(hellipse(2), 'ZData');
    
    subplot(1,3,2);
    hold on
    scatter(log(PP(n_start:n_end,2)),log(PP(n_start:n_end,3)),3);
    plot(ydatas,zdatas,'k--','LineWidth',2)
    
    xlabel('log(LDL_c)')
    ylabel('log(TC)')
    
    %axis equal
    xlim([log(min(PP(:,2))),log(max(PP(:,2)))]);
    ylim([log(min(PP(:,3))),log(max(PP(:,3)))]);   
    
    %HDL_c vs LDL_c
    
    xdatas = get(hellipse(1), 'XData');
    ydatas = get(hellipse(1), 'YData');
    zdatas = get(hellipse(1), 'ZData');
    
    subplot(1,3,3);
    hold on
    scatter(log(PP(n_start:n_end,1)),log(PP(n_start:n_end,2)),3);
    plot(xdatas,ydatas,'k--','LineWidth',2)    
    
    xlabel('log(HDL_c)')
    ylabel('log(LDL_c)')
    
    %axis equal
    xlim([log(min(PP(:,1))),log(max(PP(:,1)))]);
    ylim([log(min(PP(:,2))),log(max(PP(:,2)))]);
    
   
    
    delete(findall(gcf,'Tag','Title'))
    
    annotation('textbox', [0 0.9 1 0.1],  'String',...
        ['Generation : ', num2str(i-1), ', plausible patients : ', num2str(n_plausible(i))], ...
        'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'Tag', 'Title')
    
    drawnow;
    frame = getframe(1);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    
    imwrite(imind,cm,['generation', num2str(i),'.png'],'png');
    
%     if i==1
%         imwrite(imind,cm,gif_name,'gif', 'Loopcount',inf, 'DelayTime', 1);
%     else
%         imwrite(imind,cm,gif_name,'gif','WriteMode','append', 'DelayTime', 1);
%     end
    
    n_start = n_end+1;
end

%% histogram plots
% load('Results/GA_OUTPUT.mat');
% figure();
% for i = 1:9
%     subplot(3,3,i);
%     hist(all_population(:,i),20);
%
%     set(get(gca,'child'),'FaceColor','r','EdgeColor','r');
%
%     hold on
%     hist(pOut(:,i),20);
%
% end
%
% figure();
% for i = 1:9
%     subplot(3,3,i)
%     hist(all_population(:,9+i),20);
%
%     set(get(gca,'child'),'FaceColor','r','EdgeColor','r');
%
%     hold on
%     hist(pOut(:,i),20);
% end
%
% figure()
% for i = 1:4
%     subplot(2,2,i);
%     hist(all_population(:,18+i),20);
%
%     set(get(gca,'child'),'FaceColor','r','EdgeColor','r');
%
%     hold on
%     hist(pOut(:,i),20);
% end
