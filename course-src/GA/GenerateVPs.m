function GenerateVPs(My_model_save,NumPPs)

%% 1. Load model into matlab workspace
dirSave=strcat('../Results/GA_',My_model_save);
load('../Source/My_Model.mat')

delete(['../Results/',My_model_save,'.mat']);
% Rename the model something sensible
VPmodel=van_de_pas_mod1;

% Reseed the random number generator:
rng('shuffle')

% Accelerate the model - need to setup mex compliler to do this:
sbioaccelerate(VPmodel);

% Simulate to generate baseline VP, grab names:
[tb,xb,names] = sbiosimulate(VPmodel);

%% 2. Load free parameters
% Do not include 'parameters' redefined by SimBiology in repeated
% assignments, or parameters which are known with high accuracy.

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

%% 3. The Integration Configuration

C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);

%%%% 3. Identify the outputs of the model which you are going to constrain
% These should match up with the oder of SS_lower and SS_upper.

%%%%%%%%%%%%%%%%%% Find Indices to fit %%%%%%%%%%%%%

A=strcmp(names,'intestine.free_cholesterol');
FitIndices(1)=find(A==1);

A=strcmp(names,'intestine.cholesterol_ester');
FitIndices(2)=find(A==1);

A=strcmp(names,'liver.free_cholesterol');
FitIndices(3)=find(A==1);

A=strcmp(names,'liver.cholesterol_ester');
FitIndices(4)=find(A==1);

A=strcmp(names,'plasma.HDL_free_cholesterol');
FitIndices(5)=find(A==1);

A=strcmp(names,'plasma.HDL_cholesterol_ester');
FitIndices(6)=find(A==1);

A=strcmp(names,'plasma.non_HDL_cholesterol');
FitIndices(7)=find(A==1);

A=strcmp(names,'periphery.cholesterol');
FitIndices(8)=find(A==1);

%A=strcmp(names,'LDL_estimate_NHANES');
%FitIndices(9)=find(A==1);

%%%%%%%% Flag the states here that are being compared at steady-state

SteadyStateFlag=true(numel(names),1);
%SteadyStateFlag(9)=false; % uncomment to match the LDL later

RangeSS=-1*ones(numel(names),2);

RangeSS(:,1)=SS_lower';
RangeSS(:,2)=SS_upper';

RangeSS(~SteadyStateFlag,:)=nan;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Add variant of initial guess
NewP=addvariant(VPmodel,'NewP');

for i=1:numel(ConstantParameters)
    addcontent(NewP,{'parameter',ConstantParameters{i},'Value',ConstantParameterValues(i)})
end

%% 5. Vary parameters to form initial guess and perform optimization:
mvmodel = correlate_nhanes_chol(false);
pdf_fnc = @(x) mvnpdf(x, mvmodel.mu, mvmodel.Sigma);

pop_size = 10000;

%latin hypercube initial conditions
hn=lhsdesign(pop_size,numel(p_lower));
initial_p=bsxfun(@plus,p_lower',bsxfun(@times,hn,(p_upper'-p_lower')));

%uniform random initial conditions
% initial_p = transpose(repmat(p_lower+(p_upper-p_lower),1,pop_size).*...
%     rand(numel(p_upper),pop_size));
p_lower = log10(p_lower);
p_upper = log10(p_upper);

initial_p = log10(initial_p);

opt = gaoptimset('Display', 'iter', 'Generations', 10, ...
        'InitialPopulation', initial_p, 'PopulationSize', pop_size,...
        'OutputFcn', @ga_output);

save('../Results/GA_SAVE_OPTIONS.mat', 'My_model_save', 'NumPPs');

f=@(p)RunOptimGA(p,ConstantParameters,VPmodel,tb,xb,C,FitIndices,...
        RangeSS,SteadyStateFlag,pdf_fnc);
ga(f,22,[],[],[],[],p_lower,p_upper, [],[],opt); %output saved in Results/GA_OUTPUT.mat
	
disp('FINISHED GENERATING PLAUSIBLE PATIENTS');
end

