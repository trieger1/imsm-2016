function [state, options, optchanged] = ga_output(options, state, flag)
%% ga_output.m : This is function that can be passed as an option to 
% Matlab's genetic algorthim routine, inputs and outputs are defined at
% http://www.mathworks.com/help/gads/genetic-algorithm-options.html#f17837

%% load in VPmodel, My_model_save and NPPs
load('../Source/My_Model.mat');
load('../Results/GA_SAVE_OPTIONS.mat');

VPmodel =  van_de_pas_mod1;
optchanged = false;

%% find plausible patients, i.e. those with a score < 0
plausible_indices = find(state.Score < 0);

population_plausible = state.Population(plausible_indices,:);
scores_plausible = state.Score(plausible_indices);

%% append to existing list of plausible patients if possible
if exist(['../Results/',My_model_save,'.mat'], 'file')==2
    load(['../Results/',My_model_save,'.mat']);
    
    pOut = [pOut; 10.^population_plausible];
    Score_v = [Score_v; scores_plausible];
    
    plausible_concat = [pOut, Score_v];
    
    plausible_concat = uniquetol(plausible_concat, 'ByRows', true);
    
    pOut = plausible_concat(:,1:end-1);
    Score_v = plausible_concat(:,end);
    n_plausible = [n_plausible, length(Score_v)];
    
else   %create new list of plausible patients
    pOut = 10.^population_plausible;
    Score_v = scores_plausible;
    n_plausible = length(plausible_indices);        
end

%% save results
save(['../Results/',My_model_save,'.mat'], 'pOut', 'Score_v', 'VPmodel', 'n_plausible');
disp(['Number of plausible patients found: ', num2str(n_plausible(end))]);

if (n_plausible(end) >= NumPPs)
    state.StopFlag = 'y';   
end
