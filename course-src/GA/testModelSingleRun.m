%% Run a single patient and plot results, this code follows GenerateVPs.m
load('My_Model.mat');
% Simulate to generate baseline VP, grab names:
VPmodel = van_de_pas_mod1;
[tb,xb,names] = sbiosimulate(VPmodel);

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);
search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

C=getconfigset(VPmodel);
%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);

%%%%%%%% Flag the states here that are being compared at steady-state

SteadyStateFlag=true(numel(names),1);
%SteadyStateFlag(9)=false; % uncomment to match the LDL later

RangeSS=-1*ones(numel(names),2);

RangeSS(:,1)=SS_lower';
RangeSS(:,2)=SS_upper';

RangeSS(~SteadyStateFlag,:)=nan;

for k = 1:100 
    %generate random parameter set
    p=p_lower+(p_upper-p_lower).*rand(numel(p_upper),1);
    
    NewP2 = addvariant(VPmodel,'NewP2');
    % Writes the proposed parameters to the new variant:
    for i=1:numel(p)
        addcontent(NewP2,{'parameter',ConstantParameters{i},'Value',p(i)})
    end
   
    try
 
    [t1,x1] = sbiosimulate(VPmodel,C,NewP2); % Simulated the new variant, usually to steady state
    
    %test if patient is plausible
    score = ScoreModelSS(xb(end,:),x1(end,:),RangeSS,SteadyStateFlag);
    
    if (score > 0)
        %save('../Results/BadPatient', 'p', 't1', 'x1');
    else
        %save('../Results/PlausiblePatient', 'p', 't1', 'x1');
        break;
    end
    
    catch
        disp('model failed');
    end

    delete(VPmodel.variant(numel(VPmodel.variant)));
    
end

%A = [t1 x1];
%header = 't,int-fc,int-ce,liv-fc,liv-ce,plas-HDL-fc,plas-HDL-ce,plas-non-HDL-c,perif-c,LDL';
%dlmwrite('../Tex/data/bad.csv', header,'delimiter', '');
%dlmwrite('../Tex/data/bad.csv', A,'delimiter', ',', '-append', 'precision', '%5.6e');
