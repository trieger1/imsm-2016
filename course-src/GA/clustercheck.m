
[gof,newselection1]= OptimizeVPgeneration(log10(sf),ProbInclude,log(VPChar),1);
[gof,newselection2]= OptimizeVPgeneration(log10(sf),ProbInclude,log(VPChar),1);

PSelect=Pfinal(:,selection);
w = std(PSelect');
Wmat=repmat(w,sum(selection),1);
medP=repmat(median(PSelect'),sum(selection),1);
Pnorm=(PSelect'-medP)./Wmat;
idx=kmeans(Pnorm,2,'Replicates',100);


for i=1:22
    Pcluster1(i)=median(PSelect(i,idx==1))/median(PSelect(i,:));
    Pcluster2(i)=median(PSelect(i,idx==2))/median(PSelect(i,:));
end

scatter(Pcluster1,Pcluster2);

P1=PSelect(:,idx==1);
P2=PSelect(:,idx==2);
scatter3(P1(19,:),P1(5,:),P1(6,:))
hold on
scatter3(P2(19,:),P2(5,:),P2(6,:))

A=[sum(newselection1),sum(newselection2)];
I=sum(newselection1.*newselection2);
venn(A,I)



%%%%% Parameters in unison

Unionselection=newselection1.*newselection2;

nonUnionselection= newselection1+newselection2-2*newselection1.*newselection2;

CombinedSelection=newselection1+newselection2-newselection1.*newselection2;

PSelect=Pfinal(:,logical(CombinedSelection));
w=std(PSelect');
Wmat=repmat(w,sum(CombinedSelection),1);
medP=repmat(median(PSelect'),sum(CombinedSelection),1);
Pnorm=(PSelect'-medP)./Wmat;
idx=kmeans(Pnorm,2,'Replicates',100);

