function [score] = ScoreModelSSGA(ModelSS,RangeSS,pdf_fnc,SteadyStateFlag)

flg=SteadyStateFlag;

MidRange=(RangeSS(:,1)'+RangeSS(:,2)')/2;

%% Redefine for convenience, grab only the data we are interested in
ModelSS=ModelSS(flg);
RangeSS=RangeSS(flg);
MidRange=MidRange(flg);

%%%% SSE - SSE at points defined by range
score=(MidRange-ModelSS).^2 - (MidRange-RangeSS(:,1)').^2;
score=score./MidRange.^2;
score=sum(score(score>=0));

ModelSS = ModelSS*(38.66/2.79); %convert units to match NHANES
lnHDL = log((ModelSS(5) + ModelSS(6)));
lnTC =  log((ModelSS(7) + ModelSS(5) + ModelSS(6)));
lnLDL = log((ModelSS(9)));

if abs(score < eps)
%	pdf_fnc([lnHDL, lnLDL, lnTC])
	%pause
	score = score - pdf_fnc([lnHDL, lnLDL, lnTC]);
end


%score = score - pdf_fnc([lnHDL, lnLDL, lnTC]);

end

