function [OutDYDT,OutPts] = SimulateVPs(VPmodel,parametersets,tag_string,saveflag)

savename=strcat('Results',tag_string);

NumVPs=numel(parametersets(:,1));
[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);
C=getconfigset(VPmodel);


%set(C, 'StopTime', 200000);
set(C, 'SolverType', 'ode15s');

set(C.SolverOptions, 'RelativeTolerance', 1.0e-4);
set(C.SolverOptions, 'AbsoluteTolerance', 1.0e-6);


OutDYDT=zeros(NumVPs,1);

[t_currentVP,x_currentVP] = sbiosimulate(VPmodel);

NumofStates=numel(x_currentVP(1,:));

OutPts=zeros(NumVPs,NumofStates);


for i=1:NumVPs
 
    p = parametersets(i,:);
    
    CurrentVP=addvariant(VPmodel,'CurrentVP');
    
    %%%% II. Add passed parameters to variant
    for j=1:numel(p)
        addcontent(CurrentVP,{'parameter',ConstantParameters{j},'Value',p(j)});
    end
    
    try
        [t_currentVP,x_currentVP] = sbiosimulate(VPmodel,C,CurrentVP);
    catch
        x_currentVP = 1e30*ones(NumofStates,1);
        t_currentVP=[1 2];
    end
    
    OutPts(i,:)=x_currentVP(end,:);
    OutDYDT(i)=max(x_currentVP(end,:)-x_currentVP(end-1,:))/(t_currentVP(end)-t_currentVP(end-1));
    
    
    delete(VPmodel.variant(numel(VPmodel.variant)))
end

if saveflag==1
    save(savename,'OutPts','OutDYDT');
end

end

