close all
clear all

addpath('distributionPlot');
figure

%% Makes violin plots and correlation matrices of parameters

load('../Results/baselinePP500.mat'); %% load in your file here%%
% Pfinal= plausible patients
% load in .mat file with the following fields:
% VPmodel
% pOut

Pfinal = pOut';

%% Get p_upper and p_lower
[tb,xb,names] = sbiosimulate(VPmodel);

[ConstantParameters,ConstantParameterValues] = ParametersToVary(VPmodel);

search_mag=5;
[p_lower,p_upper,SS_lower,SS_upper]= Input_Ranges(VPmodel,names,ConstantParameterValues,search_mag);

%% Select virtual population
[~,OutPts] = SimulateVPs(VPmodel,pOut','speed_test',0);

% Create NHANES outputs:
VPChar(:,1)=OutPts(:,5)+OutPts(:,6); %HDL
VPChar(:,2)=OutPts(:,9); % LDL
VPChar(:,3)=OutPts(:,7)+OutPts(:,5)+OutPts(:,6); %TC

% Unit conversion to match NHANES
VPChar=VPChar/2.79;
VPChar=VPChar*38.66;

% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
m = correlate_nhanes_chol(0); % fetch the NHANES data
mu = m.mu;                    % log-normal distribution parameters
sigma = m.Sigma;              % log-normal distribution parameters

[selection, gofRun] = prevalence_select_func(mu,sigma,log(VPChar));

%% Make plots
NumVPs=numel(Pfinal(1,:));
PriorP=rand(NumVPs,1);

for i=1:22
    ind=num2str(i);
    Pname=strcat('k_{',ind,'}');
    ParameterNames{i}=Pname;
end

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);

P_scaled=(Pfinal-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
P_scaled1=P_scaled;
%ParameterNames=[ConstantParameters];
%ParameterNames{23}='k22';
subplot(2,2,1)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)
ylim([0 1])
alpha(0.3)
C1=corrcoef(P_scaled);

NumVPs=numel(Pfinal(1,selection));

PriorP=rand(NumVPs,1);

p_upper_mat=repmat(p_upper,1,NumVPs);
p_lower_mat=repmat(p_lower,1,NumVPs);

P_scaled=(Pfinal(:,selection)-p_lower_mat)./(p_upper_mat-p_lower_mat);
P_scaled=P_scaled';
C2=corrcoef(P_scaled);
subplot(2,2,3)
distributionPlot(P_scaled,'showMM',6,'xNames',ParameterNames,'histOpt',1)

ylim([0 1])
alpha(0.3)

Cmax=max(max(max(C1-eye(size(C1)))),max(max(C2-eye(size(C2)))));
Cmin=min(min(min(C1-eye(size(C1)))),min(min(C2-eye(size(C2)))));

subplot(2,2,2)
imagesc(C1,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square
subplot(2,2,4)
imagesc(C2,[Cmin Cmax])
set(gca,'xtick',[],'ytick',[])
colorbar
axis square

