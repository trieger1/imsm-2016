function [j_merge,met_plot_merge] = append_data_structs(old_data_file,new_data_file,save_file)
%% function append_data_structs
%   Helper function, takes in two previous runs and merges the two most important outputs:
%   the 'j' structure and 'met_plot' structure.
%

load(old_data_file,'j','met_plot');
j_old = j;
met_plot_old = met_plot;

met_old = categorical({met_plot_old.method}');

load(new_data_file,'j','met_plot');
j_new = j;
met_plot_new = met_plot;

met_new = categorical({met_plot_new.method}');

u = union(met_old,met_new);
for i = 1:numel(u)
    r1 = met_new == u(i);
    r2 = met_old == u(i);
    
    if sum(r1) == 0 && sum(r2) == 1
        met_plot_merge(i) = met_plot_old(r2);
    elseif sum(r2) == 0 && sum(r1) == 1
        met_plot_merge(i) = met_plot_new(r1);
    elseif sum(r1) == 1 && sum(r2) == 1
        % Usual case, check for which one is largest:
        if met_plot_old(r2).num_pps > met_plot_new(r1).num_pps
            met_plot_merge(i) = met_plot_old(i);
        else
            met_plot_merge(i) = met_plot_new(i);
        end
    else
        error('something is wrong here, how did we get here?');
    end
end

j_merge = j_old;
j_merge((end+1):(end+numel(j_new))) = j_new;

j = j_merge; %#ok<*NASGU>
met_plot = met_plot_merge;

save(save_file,'j','met_plot');

end % function append_data_structs
