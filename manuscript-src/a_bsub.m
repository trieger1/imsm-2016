%% Temporary wrapper for test_speed function:
clear;clc;close all;rng('shuffle');
addpath('./NHANES');
addpath('./model');
addpath('./allcomb');

if exist('results','dir') ~= 7
    mkdir('results');
end

if exist('txtout','dir') ~= 7
    mkdir('txtout');
end

%% Save files:
resume_run = 1;
txt_name = 'txtout/hpc_long_'; % running text files for extremely long runs

%% Find VPs de novo, or load last results:

if ~resume_run
    %% Key inputs:
    num_pps     = [100;500;1000;5000;10000];  % Number of plausible patients to attempt to create
    num_iters   = 5;
    num_regions = 5;                        % Ellipse-method only input, un-needed otherwise
    methods      = {'SA';'NSA';'MH';'GA'};  % Method to use, must be a precise input (e.g., 'NSA','SA','GA')
    mdl_mat     = 'My_Model.mat';           % Location of the exported SimBiology model
    i_start = 1;
    
    %% NHANES Log-Normal Fit - see correlate_NHANES_chol.m
    m       = correlate_nhanes_chol(0); % fetch the NHANES data
    mu      = m.mu;                     % log-normal distribution parameters
    sigma   = m.Sigma;                  % log-normal distribution parameters
    
    %% Load the model
    load(mdl_mat);
    num_pps = sort(num_pps); % just in case
    atmp = allcomb(1:numel(methods),1:num_iters,1:numel(num_pps)); % define all the cases to be run
    i_end = size(atmp,1);
    save(strcat(txt_name,'_run_setup.mat'));
else
    load(strcat(txt_name,'_run_setup.mat'));
    % Figure out i_start:
    list = dir(strcat(txt_name,'*.csv'));
    i_start = numel(list)+1;
end

%% Main loop, go through for each case (method/#pps), generate results
for i_all_iter = i_start:i_end
    fprintf('%d of %d: %s, %d\n',i_all_iter,size(atmp,1),...
        methods{atmp(i_all_iter,1)},num_pps(atmp(i_all_iter,3)));
    % Call test_speed the master function:
    j(i_all_iter).sm = test_speed(num_pps(atmp(i_all_iter,3)), ...
        methods{atmp(i_all_iter,1)}, van_de_pas_mod1, mu, sigma, num_regions);
    j(i_all_iter).num_pps = num_pps(atmp(i_all_iter,3)); % nominal number of pps
    j(i_all_iter).method = methods{atmp(i_all_iter,1)};
    write_j_to_file(txt_name,j(i_all_iter),i_all_iter);
end

%% EoS
