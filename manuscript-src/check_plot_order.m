function [m,j] = check_plot_order(m_in,j_in,method_list)
%% function check_plot_order
%   Reorders the input structure by method and number of PPs, for ease of
%   plotting.
%

% Legacy datasets, fix the names of the methods:
% for i = 1:numel(j_in)
%     switch j_in(i).method
%         case 'simanneal'
%             j_in(i).method = 'SA';
%         case 'ellipse'
%             j_in(i).method = 'NSA';
%         case 'mh'
%             j_in(i).method = 'MH';
%         case 'ga'
%             j_in(i).method = 'GA';
%         case 'SA'
%             j_in(i).method = 'SA';
%         case 'NSA'
%             j_in(i).method = 'NSA';
%         case 'MH'
%             j_in(i).method = 'MH';
%         case 'GA'
%             j_in(i).method = 'GA';
%         otherwise
%             fprintf('Unrecoginized method: %s\n',j_in(i).method);
%             error('Abort in check_plot_order, unknown method');
%     end
% end
% 
% for i = 1:numel(m_in)
%     switch m_in(i).method
%         case 'simanneal'
%             j_in(i).method = 'SA';
%         case 'ellipse'
%             j_in(i).method = 'NSA';
%         case 'mh'
%             j_in(i).method = 'MH';
%         case 'ga'
%             j_in(i).method = 'GA';
%         case 'SA'
%             j_in(i).method = 'SA';
%         case 'NSA'
%             j_in(i).method = 'NSA';
%         case 'MH'
%             j_in(i).method = 'MH';
%         case 'GA'
%             j_in(i).method = 'GA';
%         otherwise
%             fprintf('Unrecoginized method: %s\n',m_in(i).method);
%             error('Abort in check_plot_order, unknown method');
%     end
% end

method_list_cat = categorical(method_list);
m_method = categorical({m_in.method}');
j_method = categorical({j_in.method}');

cm = 1;
cj = 1;

for i = 1:numel(method_list_cat)
    rm = m_method == method_list_cat(i);
    rj = j_method == method_list_cat(i);
    
    if sum(rm) > 0
        m_sub = m_in(rm);
        % Get the NPPs:
        npp_tmp = zeros(numel(m_sub),1); % reinitialize
        for k = 1:numel(m_sub)
            npp_tmp(k) = m_sub(k).num_pps;
        end
        [~,is] = sort(npp_tmp);
        for k = 1:numel(m_sub)
            m(cm) = m_sub(is(k));
            cm = cm + 1;
        end
    end
    
    if sum(rj) > 0
        j_sub = j_in(rj);
        % Get the NPPs:
        npp_tmp = zeros(numel(j_sub),1); % reinitialize
        for k = 1:numel(j_sub)
            npp_tmp(k) = j_sub(k).num_pps;
        end
        [~,is] = sort(npp_tmp);
        for k = 1:numel(j_sub)
            j(cj) = j_sub(is(k));
            cj = cj + 1;
        end
    end
end

end % function check_plot_order
