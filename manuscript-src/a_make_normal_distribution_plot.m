function a_make_normal_distribution_plot

%% Create nested histograms for illustration of NSA method:
mu = 6;
sigma2 = 2;
normdenfun = @(x) 1/sqrt(2*sigma2*pi)*exp(-(x-mu).^2/(2*sigma2));
x = 0:.001:12;
y = normdenfun(x);

%Determine L1
alpha1 = 5.391;
beta1 = 6.609;
chk1=abs(alpha1-mu);
chk2=abs(beta1-mu);
%if chk1~=chk2
%    error('|alpha1-mu| does not equal |beta1-mu|') %make sure centered around mu
%end
int1 = integral(normdenfun,alpha1,beta1); %find alpha1, beta1 so that int1 = 1/3
l1=beta1-alpha1;
L1ind1 = find(x==alpha1); %index: x(5392) = 5.391
L1ind2 = find(x==beta1); %index: x(6610) = 6.609

%Determine L2
alpha2 = 4.632;
beta2 = 7.368;
chk3=abs(alpha2-mu);
chk4=abs(beta2-mu);
%if chk3~=chk4
%    error('|alpha2-mu| does not equal |beta2-mu|')
%end
int2 = integral(normdenfun,alpha2,beta2); %find alpha2, beta2 so that int2 = 2/3
l2=beta2-alpha2;
L2ind1 = find(x==alpha2); %index: x(4633) = 4.632
L2ind2 = find(x==beta2); %index: x(7369) = 7.368

%Determine L3
alpha3 = 1;
beta3 = 11;
chk5=abs(alpha3-mu);
chk6=abs(beta3-mu);
%if chk5~=chk6
%    error('|alpha3-mu| does not equal |beta3-mu|')
%end
int3 = integral(normdenfun,alpha3,beta3); %find alpha3, beta3 so that int3 = 1
l3=beta3-alpha3;
L3ind1 = find(x==alpha3); %index: x(1001) = 1.000
L3ind2 = find(x==beta3); %index: x(11001) = 11.000

%solve for q: number of patients optimzied in each round
q3 = 1/3*(l3/(l3-l2)); %.4589  - with n = 100 patients, round 3 patients: 46
q2 = 1/3*(l2/(l2-l1))-q3*(l2/l3); %.4752 - round 2 patients: 47
q1 = 1/3 - q2*(l1/l2) - q3*(l1/l3); %.0659 - round 1 patients: 7

%create random points for patients in each round
rng('default') %get same random set each time we run the code
rng(1)
%rand(num_points,1) = uniform distributed random points between 0 and 1
%if want random points between 10 and 15, then
%5*rand(num_points,1) = uniform distributed points between 0 and 5
%5*rand(num_points,1) + 10 = uniform distributed points between 10 and 15
patientsR1 = (beta1-alpha1).*rand(6,1)+alpha1; %Round 1
%if min(patientsR1)<alpha1 || max(patientsR1)>beta1
%    error('patient is created outside L1')
%end

rng(2)
patientsR2 = (beta2-alpha2).*rand(48,1)+alpha2; %Round 2
%if min(patientsR2)<alpha2 || max(patientsR2)>beta2
%    error('patient is created outside L2')
%end

rng(3)
patientsR3 = (beta3-alpha3).*rand(46,1)+alpha3; %Round 3
%if min(patientsR3)<alpha3 || max(patientsR3)>beta3
%    error('patient is created outside L3')
%end


%determine number and type of patient in each Lk
allpatients = [patientsR1; patientsR2; patientsR3]; %100x1 vector

%L1 population
L1popfromR1 = patientsR1(patientsR1 <= beta1 & patientsR1 >= alpha1); %6 patients
L1popfromR2 = patientsR2(patientsR2 <= beta1 & patientsR2 >= alpha1); %27 patients
L1popfromR3 = patientsR3(patientsR3 <= beta1 & patientsR3 >= alpha1); %10 patients
L1total = allpatients(allpatients <= beta1 & allpatients >= alpha1); %43 patients
%if length(L1total)~=length(L1popfromR1)+length(L1popfromR2)+length(L1popfromR3)
%    error('L1 total population is incorrect')
%end


%L2\L1 left population
L2leftpopfromR2 = patientsR2(patientsR2 < alpha1 & patientsR2 >= alpha2); %14 patients
L2leftpopfromR3 = patientsR3(patientsR3 < alpha1 & patientsR3 >= alpha2); %4 patients
L2lefttotal = allpatients(allpatients < alpha1 & allpatients >= alpha2); %18 patients
%if length(L2lefttotal)~=length(L2leftpopfromR2)+length(L2leftpopfromR3)
%    error('L2 left total population is incorrect')
%end
%L2\L1 right population
L2rightpopfromR2 = patientsR2(patientsR2 <=beta2 & patientsR2 > beta1); %7 patients
L2rightpopfromR3 = patientsR3(patientsR3 <=beta2 & patientsR3 > beta1); %1 patients
L2righttotal = allpatients(allpatients <=beta2 & allpatients > beta1); % 8 patients
%if length(L2righttotal)~=length(L2rightpopfromR2)+length(L2rightpopfromR3)
%    error('L2 right total population is incorrect')
%end
%total L2\L1 population
L2total = [L2lefttotal;L2righttotal]; %26 patients

%L3\L2 left population
L3leftpopfromR3 = patientsR3(patientsR3 < alpha2 & patientsR3 >= alpha3); %17 patients
L3lefttotal = allpatients(allpatients < alpha2 & allpatients >= alpha3); %17 patients
%if length(L3leftpopfromR3) ~= length(L3lefttotal)
%    error('L3 left total population is incorrect')
%end
%L3\L2 right population
L3rightpopfromR3 = patientsR3(patientsR3 <= beta3 & patientsR3 > beta2); %14 patients
L3righttotal = allpatients(allpatients <= beta3 & allpatients > beta2); %14 patients
%if length(L3rightpopfromR3) ~= length(L3righttotal)
%    error('L3 right total population is incorrect')
%end
%total L3\L2 popualtion
L3total = [L3lefttotal; L3righttotal]; %31 patients

%check that all the addition is correct - 100 total patients
%if length(allpatients) ~= length(L1total) + length(L2lefttotal) + length(L2righttotal) + ...
%                           length(L3lefttotal) + length(L3righttotal)
%    error('total population is inccorect')
%end

%Determine proportion of patients in each Lk from each round
%This will be the area of the "distribution" (the rectangles) - add up to 1
propL1fromR1 = length(L1popfromR1)/100; %.0600
propL1fromR2 = length(L1popfromR2)/100; %.2700
propL1fromR3 = length(L1popfromR3)/100; %.1000
propL1 = length(L1total)/100; %.4300 %this shoudl equal the addition of the above

propL2leftfromR2 = length(L2leftpopfromR2)/100; %.1400
propL2leftfromR3 = length(L2leftpopfromR3)/100; %.0400
propL2left = length(L2lefttotal)/100; %.1800

propL2rightfromR2 = length(L2rightpopfromR2)/100; %.0700
propL2rightfromR3 = length(L2rightpopfromR3)/100; %.0100
propL2right = length(L2righttotal)/100; %.0800

propL3leftfromR3 = length(L3leftpopfromR3)/100; %.1700
propL3left = length(L3lefttotal)/100; %.1700 %these should be equal

propL3rightfromR3 = length(L3rightpopfromR3)/100; %.1400
propL3right = length(L3righttotal)/100; %.1400 %these should be equal

%check all the proportions add to 1
%if 1 ~= propL1 + propL2left + propL2right + propL3left + propL3right
%    error('proportions do not add to 1')
%end


%want to create rectangles that approximate normal distribution
%so area of rectangles should add to 1
%A = l*h, so h = A/l where A = proportion of patients in that group
hL1fromR1 = propL1fromR1/l1; %.0493
hL1fromR2 = propL1fromR2/l1; %.2217
hL1fromR3 = propL1fromR3/l1; %.0821
hL1 = propL1/l1; %.3530

hL2leftfromR2 = propL2leftfromR2/(alpha1-alpha2); %.1845
hL2leftfromR3 = propL2leftfromR3/(alpha1-alpha2); %.0527
hL2left = propL2left/(alpha1-alpha2); %.2372

hL2rightfromR2 = propL2rightfromR2/(beta2-beta1); %.0922
hL2rightfromR3 = propL2rightfromR3/(beta2-beta1); %.0132
hL2right = propL2right/(beta2-beta1); %.1054

hL3left = propL3left/(alpha2-alpha3); %.0468
hL3right = propL3right/(beta3-beta2); %.0385

%check that areas of rectangles add to 1
%hL1*(l1)+hL2left*(alpha1-alpha2)+hL2right*(beta2-beta1)+...
%    hL3left*(alpha2-alpha3)+hL3right*(beta3-beta2)

%% Plotting options
newblue = [0 .4470 .7410];
newgreen = [.4660 .6740 .1880];
newyellow = [.9290 .6940 .1250];
neworange = [.8500 .3250 .0980];
newred = [ 0.6350    0.0780    0.1840];
newpurple=[   0.4940    0.1840    0.5560];
oldred = [1 0 0];

R1color = newred;
R2color = newyellow;
R3color = newblue;

%% Figure (A) Patients
fig1 = figure('units','inches','position',[1 1 20 10],'Name','NSA','visible','on');

subplot(1,2,2);

% Note: in order to stack shaded rectangles on top of each other, I added the
% height of the bottom rectangle to the height of the top rectangle
% (all the rectangles start at 0, but they are plotted on top of each other)

% Plot normal distribution
Y = normpdf(x,mu,sqrt(sigma2));
plot(x,y,'-k','linewidth',3)
hold on;

%plot L1 Column from [alpha1, beta1]
L1R3A = area(x([L1ind1,L1ind2]),(hL1fromR1+hL1fromR2+hL1fromR3)*ones(1,2));
L1R3A.FaceColor = R3color;
L1R2A = area(x([L1ind1,L1ind2]),(hL1fromR1+hL1fromR2)*ones(1,2));
L1R2A.FaceColor = R2color;
L1R1A = area(x([L1ind1,L1ind2]),hL1fromR1*ones(1,2));
L1R1A.FaceColor= R1color;

%plot Left L2\L1 Column from [alpha2, alpha1]
L2leftR3A = area(x([L2ind1,L1ind1]),(hL2leftfromR2+hL2leftfromR3)*ones(1,2));
L2leftR3A.FaceColor = R3color;
L2leftR2A = area(x([L2ind1,L1ind1]),(hL2leftfromR2)*ones(1,2));
L2leftR2A.FaceColor = R2color;

%plot Right L2\L1 Column from [beta1, beta2]
L2rightR3A = area(x([L1ind2,L2ind2]),(hL2rightfromR2+hL2rightfromR3)*ones(1,2));
L2rightR3A.FaceColor = R3color;
L2rightR2A = area(x([L1ind2,L2ind2]),(hL2rightfromR2)*ones(1,2));
L2rightR2A.FaceColor = R2color;

%plot Left L3\L2 Column from [alpha3, alpha2]
L3leftR3A = area(x([L3ind1,L2ind1]),(hL3left)*ones(1,2));
L3leftR3A.FaceColor = R3color;

%plot Right L3\L2 Column from [beta2, beta3]
L3rightR3A = area(x([L2ind2,L3ind2]),(hL3right)*ones(1,2));
L3rightR3A.FaceColor = R3color;

%Plot normal distribution
Y = normpdf(x,mu,sqrt(sigma2));
plot(x,Y,'-k','linewidth',2)

%Plot patients as circles on x axis
p3=plot(patientsR3,0.38,'.','markersize',40,'color',R3color); %'MarkerEdgeColor','k','MarkerFaceColor',R3color);
p2=plot(patientsR2,0.38,'.','markersize',30,'color',R2color);%'MarkerEdgeColor','k','MarkerFaceColor',R2color);
p1=plot(patientsR1,0.38,'.','markersize',20,'color',R1color);%'MarkerEdgeColor','k','MarkerFaceColor',R1color);

xlabel('Patient Measurement')
ylabel('Density')
%title({'Distribution of Patients',' '})
legend('Normal PDF','Round 3','Round 2','Round 1','Location','West');
legend boxoff;
ylim([0 0.4]);
set(gca,'LineWidth',2,'FontSize',24);

%% Figure of normal distribution
subplot(1,2,1);

% Normal Distribution with rectangle shading coloring:
plot(x,y,'-k','linewidth',3)
hold on;

L3area = area(x(L3ind1:1:L3ind2),y(L3ind1:1:L3ind2));
L3area.FaceColor = newyellow;

L2area = area(x(L2ind1:1:L2ind2),y(L2ind1:1:L2ind2));
L2area.FaceColor = newgreen;

L1area = area(x(L1ind1:1:L1ind2),y(L1ind1:1:L1ind2));
L1area.FaceColor = newblue;

% Add annotation lines:
yax = [0 0.40];
plot([alpha1 alpha1],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);
plot([beta1 beta1],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);
plot([alpha2 alpha2],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);
plot([beta2 beta2],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);
plot([alpha3 alpha3],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);
plot([beta3 beta3],[yax(1), yax(2)],'--','color',[0.4 0.4 0.4],'LineWidth',1);

% Add more annotation lines:
xpos = [0.28	0.314722
0.26049	0.336115;
0.15841	0.43591];

ypos = [0.77817;0.8329;0.8877];

annotation('doublearrow',xpos(1,:),[ypos(1) ypos(1)],'HeadStyle','plain',...
    'LineWidth',2,'Color',[0.4 0.4 0.4],'LineStyle','--','LineWidth',2);
annotation('doublearrow',xpos(2,:),[ypos(2) ypos(2)],'HeadStyle','plain',...
    'LineWidth',2,'Color',[0.4 0.4 0.4],'LineStyle','--','LineWidth',2);
annotation('doublearrow',xpos(3,:),[ypos(3) ypos(3)],'HeadStyle','plain',...
    'LineWidth',2,'Color',[0.4 0.4 0.4],'LineStyle','--','LineWidth',2);
text(beta1+0.1,0.325,'L_1','fontsize',24,...
    'HorizontalAlignment','left','Color',[0.4 0.4 0.4]);
text(beta2+0.1,0.352,'L_2','fontsize',24,...
    'HorizontalAlignment','left','Color',[0.4 0.4 0.4]);
text(beta3+0.1,0.38,'L_3','fontsize',24,...
    'HorizontalAlignment','left','Color',[0.4 0.4 0.4]);

% Finish plot:
xlabel('Patient Measurement')
ylabel('Density')
legend('Normal PDF','L_3','L_2','L_1','location','west');
legend boxoff;
ylim(yax);
set(gca,'FontSize',24,'LineWidth',2);

annotation('textbox',[0.05,0.896 0.05 0.05],'String','A',...
    'FontSize',28,'LineStyle','none');
annotation('textbox',[0.5,0.896 0.05 0.05],'String','B',...
    'FontSize',28,'LineStyle','none');


end 

%% Old stuffs

%set(h,'LineStyle','none');
% % Create textbox
% annotation('textbox',...
%     [0.605938271604942 0.783847980997627 0.0371593714927038 0.0831353919239907],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String','L_2',...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0 0.447058826684952 0.74117648601532]);
% 
% % Create doublearrow
% annotation('doublearrow',[0.19304152637486 0.839506172839506],...
%     [0.887361045130643 0.888361045130643],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
% 
% % Create doublearrow
% annotation('doublearrow',[0.429854096520764 0.606060606060606],...
%     [0.843230403800477 0.843230403800476],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
% 
% % Create doublearrow
% annotation('doublearrow',[0.475869809203143 0.554433221099888],...
%     [0.79809976247031 0.79809976247031],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
% 
% % Create textbox
% annotation('textbox',...
%     [0.556555555555559 0.7624703087886 0.0304253647586977 0.0522565320665083],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String',{'L_1'},...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0.501960813999176 0.501960813999176 0.501960813999176]);
% 
% % Create textbox
% annotation('textbox',...
%     [0.839383838383846 0.859857482185275 0.0304253647586982 0.0522565320665083],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String','L_3',...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0 0.447058826684952 0.74117648601532]);



%% Old stuff, keep just in case
% have letters above
% % Create textbox
% annotation(figure1,'textbox',...
%     [0.499316498316503 0.864608076009502 0.0304253647586982 0.0522565320665083],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String','L_3',...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0 0.447058826684952 0.74117648601532]);
%
% % Create doublearrow
% annotation(figure1,'doublearrow',[0.194163860830528 0.840628507295174],...
%     [0.85648218527316 0.85748218527316],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
%
% % Create textbox
% annotation(figure1,'textbox',...
%     [0.500438832772168 0.760095011876486 0.0371593714927039 0.0831353919239907],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String','L_2',...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0 0.447058826684952 0.74117648601532]);
%
% % Create doublearrow
% annotation(figure1,'doublearrow',[0.429854096520764 0.606060606060606],...
%     [0.781472684085512 0.781472684085511],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
%
% % Create textbox
% annotation(figure1,'textbox',...
%     [0.499316498316501 0.712589073634205 0.0304253647586978 0.0522565320665083],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176],...
%     'String',{'L_1'},...
%     'LineStyle','none',...
%     'FontSize',16,...
%     'FitBoxToText','off',...
%     'EdgeColor',[0.501960813999176 0.501960813999176 0.501960813999176]);
%
% % Create doublearrow
% annotation(figure1,'doublearrow',[0.478114478114479 0.556677890011224],...
%     [0.703087885985749 0.703087885985749],...
%     'Color',[0.501960813999176 0.501960813999176 0.501960813999176]);
