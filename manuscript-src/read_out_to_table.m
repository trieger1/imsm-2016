function t = read_out_to_table(outdir)

list = dir(strcat(outdir,'*.csv'));

tcount = 0;
for i = 1:numel(list)
   if ~list(i).isdir && ~strcmp(list(i).name,'dummy.txt')
       fprintf('Adding: %s\n',list(i).name);
       if tcount == 0
           t = readtable(strcat(outdir,list(i).name),'ReadVariableNames',true);
           tcount = tcount + 1;
           fprintf('%s\t%d\t%d\n',t.method{1},t.iter(1),t.num_pps(1));
       else
           ttmp = readtable(strcat(outdir,list(i).name),'ReadVariableNames',true);
           fprintf('%s\t%d\t%d\n',ttmp.method{1},ttmp.iter(1),ttmp.num_pps(1));
           t = [t;ttmp];
           tcount = tcount + 1;
       end
   end 
end

% Filter raw table into something more useful:
t = condense_table(t,'mu');
t = condense_table(t,'sigma');
t = condense_table(t,'p_lower');
t = condense_table(t,'p_upper');
t = condense_table(t,'Pfinal');
t = condense_table(t,'VPChar');

% Scale the parameters:
t.PScaled = (t.Pfinal - t.p_lower)./(t.p_upper - t.p_lower);

if (max(max(t.PScaled)) > 1 || min(min(t.PScaled)) < 0)
    error('problem with parameter search, parameters out of bounds');
end

t.meth_cat = categorical(t.method); % categorical version comes in handy

end

function tout = condense_table(tin,str_to_find)

tout = tin;
r = contains(tin.Properties.VariableNames,str_to_find);
tout.(str_to_find) = [tin{:,r}];
tout(:,r) = [];

end